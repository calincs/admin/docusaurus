---
id: structured-data
title: Données structurées
definition: Données sous forme de feuilles de calcul, de bases de données relationnelles, de fichiers JSON, de fichiers RDF et de fichiers XML.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les données structurées sont des données qui se présentent sous la forme de feuilles de calcul (par exemple CSV, TSV, XSL, XSLX), :Term[les bases de données relationnelles]{#relational-database}, JSON dossiers, :Term[RDF]{#resource-description-framework} dossiers, et :Term[XML]{#xml} dossiers.

Au LINCS, nous considérons que les données sont structurées si les :Term[entités]{#entity} sont toutes marquées individuellement (par exemple, une entité par cellule de tableur ou par élément XML) et si les entités sont connectées, soit de manière hiérarchique (par exemple, éléments XML imbriqués), soit avec des relations entre les entités exprimées selon un schéma et une structure de données clairement définis (par exemple, des en-têtes de tableur reliant des colonnes d'entités entre elles).

## Examples

- Les [Canadian Centre for Ethnomusicology](https://www.ualberta.ca/museums/museum-collections/canadian-centre-for-ethnomusicology.html) données ont commencé sous la forme de plusieurs feuilles de calcul avec une ligne pour chaque artefact

| ID            | Titre               | placeMade | placeMadeID                      | matériel | matérielID                               |
| ------------- | ------------------- | --------- | -------------------------------- | -------- | ---------------------------------------- |
| CCEA-L1995.63 | Flûte en bambou     | Edmonton  | https://sws.geonames.org/5946768 | bambou   | http://www.wikidata.org/entity/Q27891820 |
| CCEA1995.21   | Paire de tambours Taiko | Shinano   | https://sws.geonames.org/1852136 | peau     | http://www.wikidata.org/entity/Q3291230  |

- Les données de la [Collection d'art de l'Université de la Saskatchewan](https://saskcollections.org/) ont commencé sous la forme d'un fichier XML avec un élément parent pour chaque objet d'art.

```xml
<?xml version="1.0" ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <rdf:Description>
        <ObjectIdentifier>1910.001.001</ObjectIdentifier>
        <AcquistionDate>1910</AcquistionDate>
        <ObjectTitle>Portrait of Thomas Copland</ObjectTitle>
        <ArtistName url="http://www.wikidata.org/entity/Q100921439">Victor Albert Long</ArtistName>
        <Medium url="http://vocab.getty.edu/aat/300015050">oil paint</Medium>
        <Category url="http://vocab.getty.edu/aat/300033618">painting</Category>
    </rdf:Description>
    <rdf:Description>
        <ObjectIdentifier>2018.026.001</ObjectIdentifier>
        <AcquistionDate>2018</AcquistionDate>
        <ObjectTitle>Grace</ObjectTitle>
        <ArtistName url="http://www.wikidata.org/entity/Q19609740">Lori Blondeau</ArtistName>
        <Medium url="http://vocab.getty.edu/aat/300265621">inkjet print</Medium>
        <Category url="http://vocab.getty.edu/aat/300046300">Photograph</Category>
    </rdf:Description>
</rdf:RDF>
```
