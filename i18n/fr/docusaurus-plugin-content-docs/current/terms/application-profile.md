---
id: application-profile
title: Profil d’application
definition: Un schéma composé d’éléments de métadonnées tirés d’un ou plusieurs espaces de noms, ainsi que des politiques et des directives liées à leur utilisation, préparé pour une application particulière.
---

Un profil d’application est un schéma composé d’éléments de métadonnées tirés d’un ou plusieurs :Term[espaces de noms]{#namespace}, ainsi que les politiques et directives relatives à leur utilisation, préparées pour une application particulière. Il reflète un ensemble de décisions enregistrées concernant une cible de données partagées pour une communauté donnée.

Pour :Term[Données ouvertes et liés (LOD)]{#linked-open-data} projets, un profil d’application doit fournir des détails sur le :Term[ontologie]{#ontology} (classes et :Term[propriétés]{#property}), :Term[vocabulaires]{#vocabulary} et formats de données. Ils doivent documenter la façon dont les données sont modélisées, déclarer quoi :Term[entités]{#entity} et :Term[properties]{#property} seront utilisés et déclareront les types de données pour les valeurs de chaîne, ainsi que des notes d’application pour une utilisation cohérente des champs et des propriétés. Un profil d’application doit clarifier les attentes concernant les données ingérées, traitées et gérées, documenter les modèles et les normes, et noter où la mise en œuvre s’écarte des normes communautaires ([UCLA Library](https://guides.library.ucla.edu/semantic-web/bestpractices)).

## Exemples

- [Cooper Hewitt Smithsonian Design Museum API](https://collection.cooperhewitt.org/api/)
- [Europeana APIs](https://pro.europeana.eu/page/apis)
- [Library of Congress API](https://libraryofcongress.github.io/data-exploration/)
- [Rijksmuseum API](https://www.rijksmuseum.nl/en/api/-rijksmuseum-oai-api-instructions-for-use)
- [WCLC APIs](https://www.oclc.org/developer/develop/web-services/worldcat-search-api.en.html)

## Autres ressources

- [API (Wikipedia)](https://en.wikipedia.org/wiki/Application_programming_interface)
- Wrubel (2018) [“Unboxing the Library: Introduction to APIs”](https://labs.loc.gov/static/labs/meta/Wrubel-UnboxingtheLibrary-IntroductiontoAPIs.pdf) [PowerPoint]
- Wuyts (2018) _[Cultivating APIs in the Cultural Heritage Sector](http://jolanwuyts.eu/files/Cultivating_APIs_in_the_cultural_heritage_sector_Jolan_Wuyts_2018.pdf)_
