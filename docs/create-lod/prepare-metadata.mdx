---
sidebar_position: 4
title: "Prepare Metadata"
description: "Name your dataset and provide keywords"
---

Every project requires a dataset title to refer to a project’s dataset as it appears in LINCS and keywords to help users explore the data. Keywords appear in places such as ResearchSpace, Borealis Dataverse, and on the LINCS website.

## Title Requirements

Dataset title should be consistent across LINCS (e.g., in our notes, documentation, [ResearchSpace](/docs/tools/researchspace)). To facilitate this, titles should be chosen as early in the process as possible.

Each dataset must have three titles: long title, short title, and dataset identifier. Projects are asked to provide their own long and short titles. LINCS will recommend a dataset identifier, which the project will be invited to approve.

All forms of a dataset title must be:

* Unique to the project
    * No two projects in LINCS can use the same dataset title
    * One project can use identical forms for more than one title (e.g., long title and short title can be the same)
    * If a project has more than one dataset, each dataset must have its own unique title; where possible, these titles should share a common element (e.g., project-people, project-places, etc.)
* Easy to type, spell, and pronounce
    * No random strings
    * No ambiguous combinations like 1l or 0O
    * No offensive content
    * Canadian spellings preferred

## Long Title

The long title is used where a dataset is referred to in full.

**Examples:** the project page on the portal, at first mention in publications, in citations 

The long title must be:

* Maximum 54 characters
* Punctuation, accents, and other special characters are allowed 

A long title can be provided in either or both of English and French. Long title capitalization will be governed by the context in which the title is used (e.g., on the Portal, titles will adhere to the portal style guidelines) and by language (e.g., French capitalization rules will always take precedence for French titles).

:::note

LINCS offers tools and resources in both English and French and is able to support bilingual content. We encourage projects to provide bilingual material, but we are unable to provide translation services. 

:::

## Short Title

The short title is used where space is at a premium.

**Examples:** in drop-down menus and tab headings on the Portal, on entity cards, on second and subsequent mentions in publications, on slides, in labels within the linked data form of your data 

The short title must be:

* Easy to pronounce (e.g., AdArchive) _or_ a long-established project acronym (e.g., MoEML)
* Maximum 20 characters
* Must be clear that the name is specific to the project, and not a more general class of LINCS data:

<table>
  <tr>
    <td>**Project**</td>
    <td>**Acceptable Short Title**</td>
    <td>**Unacceptable Short Title**</td>
  </tr>
  <tr>
    <td>Orlando Project</td>
    <td>Orlando</td>
    <td>Women Writers</td>
  </tr>
  <tr>
    <td>AdArchive: Tracing Pre-Digital Networked Feminisms</td>
    <td>AdArchive</td>
    <td>Advertisements</td>
  </tr>
  <tr>
    <td>University of Saskatchewan Art Collection</td>
    <td>USask Art</td>
    <td>Art Gallery</td>
  </tr>
</table>


A short title can be provided in both English and French. Short title capitalization will be governed by the context in which the title is used (e.g., on the portal, titles will adhere to the portal style guidelines) and by language (e.g., French capitalization rules will always take precedence for French titles).

## Dataset Identifier

The dataset identifier is the shortest version of the dataset title and is used where space is very limited, such as for file names. LINCS creates dataset identifiers and invites projects to approve them before they are implemented.

:::note

The dataset identifier is used in the linked data creation process. It cannot be changed after LINCS publishes the data without breaking the applications that query the data.

:::

**Examples:** in named graphs, in URLs, in file names

The dataset identifier must be:

* Maximum 20 characters
* No spaces or underscores
* No capital letters (lowercase only)
* No special characters or accents 

## Keywords Requirements

As a linked data project, we require keywords that are associated with URIs.

We want keywords to converge, where possible, as this increases their usefulness. We also want researchers to be able to choose keywords from a list that best aligns with their principles and their research domains. To facilitate convergence while respecting domain-specific priorities, we recommend researchers either (a) choose keywords from one of our recommended keywords lists or (b) propose a list to add to our recommendations to promote their use by other researchers.

To ensure a meaningful but manageable number of keywords for each dataset, we ask researchers to provide 3–5 (up to a maximum of 10) keywords.

## Providing Keywords for Your Dataset

* Generate a list of 3–5 keywords (and up to a maximum of 10) for your dataset
* Use the recommended keywords lists to find URIs for your keywords
    * If there is another source for keywords that is better suited to your domain, you are encouraged to use that list instead or as well as the recommended lists
    * You are welcome to mix and match keywords from more than one list, if needed, but we ask that you do not provide URIs for the same keyword from more than one source
    * When choosing among keywords and URIs, we encourage you to consult the LINCS keywords page [coming soon] and to make choices, where possible, that converge on the selections made by other LINCS researchers
* Keywords can be in English or French
* Provide keywords for your dataset using the [dataset keywords form](https://uoguelph.eu.qualtrics.com/jfe/form/SV_0lhmZjj47BAAyLI).

## Recommended Keyword Lists

### General Keywords Lists

* [FAST](http://fast.oclc.org/searchfast/) ([FAST linked data search](https://experimental.worldcat.org/fast/))
* [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)

### Subject-specific Keywords Lists

* [Arts and Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/?find=&logic=AND&note=)
* [Brian Deer Classification Scheme](https://xwi7xwa-library-10nov2016.sites.olt.ubc.ca/files/2019/06/Xwi7xwa-Classification-ver-04March2013P.pdf)
* [Homosaurus](https://homosaurus.org/)

To recommend additional keywords lists to be added to our recommendations, [contact us](/docs/about-lincs/get-involved/contact-us).

## Image Files for Datasets

LINCS asks projects to provide a logo and 1–3 images related to the dataset. These images will be used alongside descriptions of your project on the LINCS website and in LINCS tools. Examples in the orange rectangles (below) include photographs, scanned documents, project logos, and works of art. Other images (e.g., maps, graphics) are also welcome.

For more information about images and to share images with LINCS, use the [image files for datasets form](https://uoguelph.eu.qualtrics.com/jfe/form/SV_9sNfe8XfJM1YAIK).

