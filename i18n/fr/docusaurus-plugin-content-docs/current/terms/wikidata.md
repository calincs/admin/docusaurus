---
id: wikidata
title: Wikidata
definition: La plus grande instance de Wikibase, qui agit comme un référentiel de stockage central pour les données structurées utilisées par Wikipédia, par ses projets frères et par quiconque souhaite utiliser une grande quantité de données ouvertes à usage général.
---

Wikidata est la plus grande instance de :Term[Wikibase]{#wikibase}, une base de connaissances gratuite que tout le monde peut modifier. Il agit comme un référentiel de stockage central pour les données structurées utilisées par Wikipédia, par ses projets frères et par toute autre personne souhaitant utiliser une grande quantité de données ouvertes à usage général. Le contenu de Wikidata est disponible sous une licence gratuite et peut être exporté et lié à d’autres ensembles de données ouverts sur le Web ([Mediawiki, 2021](https://www.mediawiki.org/wiki/Wikibase/FAQ#What_is_the_difference_between_Wikibase_and_Wikidata?)).

## Exemples

- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Autres ressources

- MediaWiki (2021) [“Wikibase/FAQ”](https://www.mediawiki.org/wiki/Wikibase/FAQ#:~:text=General-,What%20is%20the%20difference%20between%20Wikibase%20and%20Wikidata%3F,base%20that%20anyone%20can%20edit.)
- UCLA Library (2017) [“Semantic Web and Linked Data: Wikibase”](https://guides.library.ucla.edu/semantic-web/wikidata)
