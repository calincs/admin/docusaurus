import React from "react";
import styles from "./buttons.module.css";
import Translate, { translate } from "@docusaurus/Translate";

function CreatorButton({ creator, className }) {
  const message = {
    Partner: <Translate id="tool.origin.partner" description="Tool Origin Partner">LINCS Partner</Translate>,
    Other: <Translate id="tool.origin.Other" description="Tool Origin Other">Enhanced by LINCS</Translate>,
    LINCS: <Translate id="tool.origin.LINCS" description="Tool Origin LINCS">Made by LINCS</Translate>,
    External: <Translate id="tool.origin.External" description="Tool Origin External">Adopted by LINCS</Translate>,
  };

  const containerClass = className ? styles[className] : styles.creatorButton;
  const nameClass = className
    ? styles[`${className}Name`]
    : styles.creatorButtonName;

  return (
    <div className={containerClass}>
      <div className={nameClass}>
        {className ? (
          <>
            <div>
              <img src="/img/lincs-logo-inverted-(c-LINCS).png" alt="" />
            </div>

            {message[creator]}
          </>
        ) : (
          <>
            {message[creator]}
            <div>
              <img src="/img/lincs-logo-inverted-(c-LINCS).png" alt="" />
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default CreatorButton;
