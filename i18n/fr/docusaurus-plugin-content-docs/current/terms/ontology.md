---
id: ontology
title: Ontologie
definition: Modèle abstrait et lisible par machine d’un phénomène qui capture et structure la connaissance des entités, des propriétés et des relations dans un domaine afin qu’une conceptualisation puisse être partagée et réutilisée par d’autres.
---

“Une ontologie est une spécification formelle et explicite d’une conceptualisation partagée” ([Studer et al., 1998](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.110.8406&rep=rep1&type=pdf)).

- Formel : lisible par machine
- Explicit : le type de concepts utilisés, et les contraintes à leur utilisation, sont explicitement définis
- Partagé : capture les connaissances d’une manière qui est acceptée par un groupe
- Conceptualisation : un modèle abstrait d’un phénomène dans le monde en ayant identifié les concepts pertinents de ce phénomène

Philosophiquement, l’ontologie est l’étude de l’être : quelles pièces s’emboîtent pour constituer le monde et comment ces pièces sont interdépendantes. Dans le domaine de l’informatique et des sciences de l’information, et en relation avec :Term[Données liées (LD)]{#linked-data}, c’est la formalisation de ce concept d’une manière lisible par machine : un modèle d’un domaine—les parties qui le composent et comment ils s’emboîtent, rédigés d’une manière compréhensible par les ordinateurs. Une ontologie capture et structure formellement la connaissance du :Term[entités]{#entity}, :Term[propriétés]{#property} et les relations qui composent un domaine afin que cette conceptualisation puisse être partagée et réutilisée par d’autres. Les ontologies sont un élément important dans de nombreux domaines informatiques, notamment la conception de systèmes logiciels orientés objet, les systèmes de recherche d’informations et un certain nombre de tâches dans le domaine de l’intelligence artificielle. Les ontologies sont également un élément clé du :Term[Web sémantique]{#semantic-web}, car ils sont utilisés pour définir formellement la signification de la terminologie utilisée et la relation de ces termes avec d’autres concepts et :Term[vocabulaires]{#vocabulary}.

|                | Ontologie                                                                                            | Taxonomie                                                                                        | Thésaurus                                                   | Vocabulaire                                                              |
| -------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ | ----------------------------------------------------------- | ------------------------------------------------------------------------ |
| **Décrit**     | Concepts (contenu) et les relations entre eux (structure), y compris les axiomes et les restrictions | Relations hiérarchiques entre les concepts, et spécifie le terme à utiliser pour désigner chacun | Relations hiérarchiques et non hiérarchiques entre concepts | Terme général désignant un ensemble de concepts (mots) liés à un domaine |
| **Relations**  | Hiérarchique typé et associatif                                                                      | Fondamentalement hiérarchique, mais tous modélisés en utilisant la même notation                 | Non typé hiérarchique, associatif et équivalence            | Non spécifié (concept abstrait)                                          |
| **Propriétés** | RDFS définit les propriétés et les restrictions des relations                                        | Aucun                                                                                            | Peut être décrit dans les notes d’application si nécessaire | Non spécifié (concept abstrait)                                          |
| **Structuré**  | Réseau                                                                                               | Arbre                                                                                            | Arbre à branches croisées                                   | Non spécifié (concept abstrait)                                          |

## Exemples

- [BIBFRAME 2](http://id.loc.gov/ontologies/bibframe.html)
- [CIDOC CRM](http://cidoc-crm.org/)

## Autres ressources

- Guarino, Oberle, & Staab (2009) [“What Is an Ontology?”](https://iaoa.org/isc2012/docs/Guarino2009_What_is_an_Ontology.pdf)
- Noy & McGuinness (2001) [“Ontology Development 101: A Guide to Creating Your First Ontology”](http://ksl.stanford.edu/people/dlm/papers/ontology-tutorial-noy-mcguinness.pdf)
- [Ontology (Wikipedia)](<https://en.wikipedia.org/wiki/Ontology_(information_science)>)
