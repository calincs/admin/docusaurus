---
id: quad
title: Quadruple
definition: Une extension d’un triplet pour inclure une quatrième section qui fournit un contexte pour le triplet, comme l’URI (Uniform Resource Identifier) ​​du graphe dans son ensemble (sujet-prédicat-objet-contexte).
---

Un quad est une extension d’un :Term[triple]{#triple} pour inclure une quatrième section—donc le changement de triple, en trois parties, à quad, en quatre parties—qui fournit un contexte pour le triplet, comme le :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} du graphique dans son ensemble. Alors qu’un triplet prend la forme de `<subject><predicate><object>`, un quad étend cela pour être `<subject><predicate><object><context>`. Dans le cas d’un :Term[graphe nommé]{#named-graph}, ce `<context>` morceau est le `<graphname>`. Bien qu’un discours désinvolte sur :Term[Les données ouvertes liées (LOD)]{#linked-open-data} ont tendance à se référer le plus souvent aux triplets et :Term[triplestores]{#triplestore}, de nombreux :Term[Les référentiels de données liées (LD)]{#linked-data} sont désormais des quadstores et les quads sont couramment utilisés, y compris par LINCS.

## Exemples

- W3C (2014) _[RDF 1.1 N-Quads](https://www.w3.org/TR/n-quads/) : L’exemple suivant montre un sujet (\_spiderman_), un prédicat (_relationship/enemyOf_), et objet (_green-goblin_), et fournit un contexte au triplet en se référant à un graphique sur le domaine de Spiderman (_graphs/spiderman_).

```text
    <http://example.org/#spiderman>
    <http://www.perceive.net/schemas/relationship/enemyOf>
    <http://example.org/#green-goblin>
    <http://example.org/graphs/spiderman> .
```

## Autres ressources

- W3C (2014) _[RDF 1.1 N-Quads](https://www.w3.org/TR/n-quads/)_
