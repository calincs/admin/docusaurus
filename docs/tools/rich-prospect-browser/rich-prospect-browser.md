---
description: "Browse across and within linked databases."
title: "Rich Prospect Browser"
---


## <i className="fa-solid fa-person-digging"></i> **This page is under construction.** <i className="fa-solid fa-person-digging"></i>

Information about the team that developed [Rich Prospect Browser](/docs/about-lincs/credits/tools-credits#rich-prospect-browser) is available on the Tool Credits page.
