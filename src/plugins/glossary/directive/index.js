// Register `hName`, `hProperties` types, used when turning markdown to HTML:
/// <reference types="mdast-util-to-hast" />
// Register directive nodes in mdast:
/// <reference types="mdast-util-directive" />

import { visit } from "unist-util-visit";
import fs from "fs";


let glossary = new Map();

function loadGlossary(glossaryJSONPaths) {
  if (glossary.size === 0) {
    for (const lang of Object.keys(glossaryJSONPaths)) {
      glossary.set(lang, JSON.parse(fs.readFileSync(glossaryJSONPaths[lang], "utf8")));
    }
  }
  return glossary;
}

function isValidUsage (id, node, file) {
  if (!id) {
    file.fail("Unexpected missing `id` on `Term` directive", node);
  }
  
  const idExists = Array.from(glossary.values()).some((terms) => id in terms);
  
  if (!idExists) {
    file.fail(`${id} not found in terms folder`, node, glossary);
    return false;
  }

  if (node.type === "leafDirective") {
    file.fail(
      "Unexpected `::Term` leaf directive, use one colon for a text directive",
      node,
    );
    return false;
  } else if (node.type === "containerDirective") {
    file.fail(
      "Unexpected `:::Term` container directive, use one colon for a text directive",
      node,
      );
      return false;
  }
  return true;
}

function getCurrentLanguage(filepath) {
const regex = /i18n\/(.*?)\//;
const match = filepath.match(regex);

if (match) {
  const language = match[1];
  return language;
}
return "en";
}


export default function TermReplacementDirective(options) {

  const { termsDir, glossaryJSONPaths } = options;
  const languages = Object.keys(glossaryJSONPaths);
  loadGlossary(glossaryJSONPaths);
  
  return (tree, file) => {
    visit(tree, function (node) {
      if (
        node.type === "containerDirective" ||
        node.type === "leafDirective" ||
        node.type === "textDirective"
      ) {
        if (node.name !== "Term") return;

        const data = node.data || (node.data = {});
        const attributes = node.attributes || {};
        const id = attributes.id;


        isValidUsage(id, node, file);
        const lang = getCurrentLanguage(file.history[0]);

        data.hName = "GlossaryTerm";
        data.hProperties = {
          "data-text": glossary.get(lang)[id].definition,
          path: `${termsDir}/${id}`,
          class: "glossary-tooltip",
          definition: glossary.get(lang)[id].definition,
          termId: id,
          label: node.children[0].value,
          tooltipId: `${id}-${lang}`,
        };

      }
    });
  };
}
