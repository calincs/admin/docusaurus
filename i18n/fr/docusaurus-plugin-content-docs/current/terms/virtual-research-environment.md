---
id: virtual-research-environment
title: Virtual Research Environment (VRE)
definition: Un espace de travail en ligne qui permet aux chercheurs de collaborer.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Un environnement de recherche virtuel (ERV) est un espace de travail en ligne qui permet aux chercheurs de collaborer. Les ERV comprennent souvent des fonctionnalités telles que des wikis/forums, l'hébergement de documents et des outils spécifiques à une discipline. Ils sont particulièrement utiles pour les équipes de recherche qui travaillent dans plusieurs institutions et/ou pays, car ils permettent de partager facilement les informations relatives à la recherche.

## Exemples

- [LEAF VRE](/docs/tools/leaf-vre/)

## Autres Ressources

- OCLC (2015) [“Virtual Research Environment (VRE) Study (OCLC/JISC)”](https://www.oclc.org/research/areas/user-studies/vre.html)
- [Virtual Research Environment (Wikipedia)](https://en.wikipedia.org/wiki/Virtual_research_environment)