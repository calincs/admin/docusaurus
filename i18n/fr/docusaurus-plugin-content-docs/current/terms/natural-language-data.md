---
id: natural-language-data
title: Données en langage naturel
definition: Données au format texte libre.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les données en langage naturel sont des données en texte libre. Pour LINCS, il s'agit d'un document composé de phrases complètes écrites en anglais moderne, respectant idéalement les règles grammaticales courantes. Les données en langage naturel comprennent les documents qui sont entièrement en texte brut, comme une biographie écrite enregistrée dans un fichier TXT, ou tout document d'un format différent dans lequel du texte brut est incorporé.

## Exemples

- Extrait simplifié des données du [Projet Orlando](https://orlando.cambridge.org/), où nous avons extrait du texte en langage naturel de documents XML.

```text
By March 1643, early in this year of fierce Civil War fighting, Dorothy Osborne's mother moved with her children from Chicksands to the fortified port of St Malo.
```
