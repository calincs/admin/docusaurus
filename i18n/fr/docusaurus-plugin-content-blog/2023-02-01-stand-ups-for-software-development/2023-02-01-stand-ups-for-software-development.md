---
slug: stand-ups-for-software-development
title: "Stand-up pour le développement de logiciels"
authors: Eason Liang
tags: [UX]
hide_table_of_contents: true
image: ./stand-ups-for-software-development-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./stand-ups-for-software-development-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

Lorsque j'ai commencé mon premier stage coopératif, je n'avais aucune idée de la manière dont le développement de logiciels fonctionnait dans un environnement professionnel. Auparavant, je n'avais fait l'expérience que de la salle de classe, où mes camarades et moi-même développions des logiciels pour des travaux. Lorsque j'ai intégré un contexte professionnel, j'ai découvert de nouvelles méthodes de collaboration, notamment les réunions debout (stand-up meetings) ou les réunions debout (stand-ups)...{/* truncate */}

Qu'est-ce qu'une réunion debout ? Les réunions stand-up sont des réunions quotidiennes de courte durée au cours desquelles une équipe se réunit pour discuter des différentes tâches d'un projet. De nos jours, les réunions debout sont au cœur de nombreux projets de développement de logiciels, mais elles ont été popularisées à l'origine par le développement agile, qui est une méthodologie bien connue dans l'industrie. Les projets de développement de logiciels peuvent être compliqués, c'est pourquoi le développement agile décompose les problèmes complexes en éléments gérables et réalisables.

Se réunir tous les jours est un excellent moyen de construire un projet de grande envergure. Elle encourage l'équipe à se fixer des objectifs qui peuvent être atteints en un jour, puis à assurer un suivi le lendemain afin d'identifier les problèmes, d'y remédier et de dissiper rapidement les confusions. Les réunions debout favorisent également la communication. Elles aident l'équipe à se sentir à l'aise les uns avec les autres grâce à une communication fréquente. Les réunions debout font en sorte que tout le monde se sente inclus.

Les réunions debout permettent à tous les membres de l'équipe de rester productifs en leur rappelant quotidiennement les objectifs sur lesquels ils doivent travailler et la manière dont ces objectifs s'intègrent dans le projet dans son ensemble. Bien que le développement de logiciels soit souvent un effort individuel, obtenir un retour d'information de la part d'un superviseur ou d'un pair lorsque des problèmes surviennent est un moyen efficace non seulement de réduire les obstacles, mais aussi d'élargir les connaissances de l'équipe. Les réunions debout sont un excellent moyen de parvenir à la transparence, car elles encouragent les gens à partager leurs problèmes et leurs solutions.

Lorsque j'ai commencé à travailler chez LINCS, je ne comprenais pas la valeur des stand-ups. Mais j'ai constaté que plus je participais à des réunions stand-up, plus je pouvais facilement chercher de l'aide et plus je me sentais à l'aise pour poser des questions. À mon tour, j'ai découvert que j'étais capable d'adapter rapidement ma façon de penser et de résoudre les problèmes que je rencontrais. Les stand-ups m'ont aidé à me concentrer sur les tâches à accomplir et m'ont fourni la structure dont j'avais besoin pour travailler de manière efficace et efficiente.

Étant donné que les stand-ups sont fréquents, ils sont également censés être très courts. Grâce au temps limité que nous consacrons aux réunions, j'ai appris à poser mes questions de manière concise et à aller droit au but. Il m'est apparu clairement que je trouvais plus efficace d'organiser des réunions plus petites et plus courtes, car elles me permettent d'aborder les petites questions avant qu'elles ne deviennent de gros problèmes. J'ai toujours été timide, et les réunions debout m'ont souvent aidé à me sentir plus à l'aise pour parler à mes collègues.

Je pensais que mon stage coopératif me permettrait d'en apprendre davantage sur le développement de logiciels. Je ne m'attendais pas à en apprendre autant sur la collaboration, le travail d'équipe et sur moi-même. Les réunions debout m'ont fourni un excellent outil que j'utiliserai tout au long de ma carrière.
