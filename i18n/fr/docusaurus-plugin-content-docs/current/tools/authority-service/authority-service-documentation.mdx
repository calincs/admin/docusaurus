---
title: "Authority Service OpenRefine Documentation"
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-16
---

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

<div className="primary-button-row">
  <PrimaryButton
    link="https://openrefine.org/"
    buttonName="Vers OpenRefine"
    />
  <PrimaryButton
    link="https://docs.openrefine.org/"
    buttonName="Vers la documentation d'OpenRefine"
    />
   <PrimaryButton
    link="https://gitlab.com/calincs/conversion/authority-service"
    buttonName="Vers le service d'autorité GitLab"
    />
</div>

## Conditions préalables

### Choisir un service

Il existe deux versions du Service d'Autorité :

```text

https://authority.lincsproject.ca/reconcile

```

Renvoie uniquement les candidats :Term[entités]{#entity} du :Term[Knowledge Graph (KG)]{#knowledge-graph} du LINCS qui utilisent le :Term[namespace]{#namespace} du LINCS (c'est-à-dire les entités dont les :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} commencent par http://id.lincsproject.ca/). Il s'agit d'URIs que LINCS a :Term[minted]{#uniform-resource-identifier-minting}, généralement parce que nous n'avons pas été en mesure de les trouver dans d'autres sources :Term[Linked Open Data (LOD)]{#linked-open-data} communes.

```text

https://authority.lincsproject.ca/reconcile/any

```

Renvoie toutes les entités du LINCS KG.

:::info

Si vous envisagez de publier vos données avec LINCS, vous voudrez probablement utiliser le service `https://authority.lincsproject.ca/reconcile/any`, qui ne filtrera pas sur la base de l'espace de noms de l'entité.

:::

## Ajouter le service

:::info

Cette page explique comment utiliser le service d'autorité dans OpenRefine. Reportez-vous à la [documentation d'OpenRefine](https://openrefine.org/docs/manual/reconciling) pour savoir comment réconcilier de manière plus générale.

Si vous avez besoin de conseils pour préparer vos données pour OpenRefine, consultez nos étapes de conversion [clean](/docs/create-lod/clean-data) et [reconcile](/docs/create-lod/match-entities).

:::

Pour ajouter le service à OpenRefine:

1. Suivez la [documentation d'OpenRefine](https://openrefine.org/docs) pour créer un projet et nettoyer vos données.
2. [Commencez à réconcilier](https://openrefine.org/docs/manual/reconciling#getting-started) une colonne de vos données.
3. Choisissez **Add Standard Service...** lorsque vous êtes invité à choisir un service.
4. Collez le :Term[URL]{#uniform-resource-locator} pour le service d'autorité de votre choix.

Pour les entités de l'espace de noms LINCS uniquement:

> `https://authority.lincsproject.ca/reconcile`

Pour toutes les entités du LINCS KG:

> `https://authority.lincsproject.ca/reconcile/any`

## Filtrer par type

Lorsque vous sélectionnez une colonne et que vous lancez :Term[reconciling]{#entity-matching}, [OpenRefine vous propose quelques types ou classes d'entités](https://openrefine.org/docs/manual/reconciling#reconciling-by-type) pour filtrer vos résultats. Si vous choisissez de filtrer les résultats par type, vous n'obtiendrez que les candidats qui appartiennent à ce type ou à une sous-classe de ce type dans le système LINCS KG.

Les suggestions d'OpenRefine ne sont basées que sur les premières lignes de vos données, il se peut donc qu'elles ne correspondent pas au bon type. Vous pouvez choisir le vôtre dans la case **Reconcilier avec le type**. Commencez à taper le nom du type que vous voulez utiliser et vous obtiendrez des suggestions de types à partir du LINCS KG.

L'image ci-dessous montre le filtrage d'une demande de rapprochement par type d'entité :

![openrefine-filter-by-type](/img/documentation/openrefine-add-type.png)

Si vous ne connaissez pas les :Term[ontologies]{#ontology} que LINCS utilise, nous vous suggérons de commencer par l'option **Reconcilier avec aucun type particulier**.

## Filtrer par propriété

Vous pouvez [utiliser une autre colonne dans vos données](https://openrefine.org/docs/manual/reconciling#reconciling-with-additional-columns) et spécifier que les valeurs de cette colonne sont liées à l'entité qui vous intéresse dans LINCS KG par le :Term[property]{#property} que vous choisissez. Vous pouvez explorer les données LINCS via [ResearchSpace](/docs/tools/researchspace) ou le [LINCS SPARQL Endpoint](/docs/tools/sparql) pour comprendre quelles propriétés sont pertinentes pour vos données. Nous vous conseillons de procéder au rapprochement par lots et de tester différents filtres de propriétés ou de ne pas utiliser de filtres.

L'image ci-dessous montre le filtrage d'une demande de rapprochement à l'aide d'autres colonnes :

![openrefine-filter-by-property](/img/documentation/openrefine-add-property.png)

## Filtrer par graphe nommé

Chaque projet du LINCS KG est stocké dans son propre :Term[named graph]{#named-graph}. Vous pouvez filtrer votre requête de rapprochement pour qu'elle ne renvoie que les entités d'un seul graphe nommé. Cette fonctionnalité est utile si vous savez que vos données proviennent d'un domaine ou d'une période spécifique qui correspond à un ensemble de données particulier dans le LINCS KG.

Tout d'abord, configurez vos données de manière à avoir une colonne contenant le nom du graphique que vous souhaitez rechercher. Vous pouvez avoir le même nom de graphique ou des noms différents dans chaque ligne. Les noms de graphes peuvent être de la forme `http://graph.lincsproject.ca/name` ou `name`. Remplacez `name` par le nom réel du graphe. Voir [notre liste de projets](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/Datasets/readme.md) pour trouver le nom correct.

Lorsque vous choisissez les paramètres pour réconcilier une colonne d'entités, cliquez sur la case à cocher correspondant à votre colonne de graphe nommée sur le côté droit de la page. Dans la case **As Property** située à côté, commencez à taper _graph_. Une option pour _named graph_ qui correspond à `http://graph.lincsproject.ca/` apparaîtra. Choisissez cette option.

L'image ci-dessous montre le filtrage d'une demande de réconciliation par le graphe nommé d'un projet particulier :

![openrefine-filter-by-named-graph](/img/documentation/openrefine-named-graph.png)

## Prévisualiser les entités

Une fois que vous avez des entités candidates, passez votre souris sur les candidats pour voir un aperçu de l'information que LINCS a sur cette entité.

:::info

Vous avez besoin de plus de détails pour confirmer la correspondance ? Cliquez sur l'étiquette de l'entité pour voir l'enregistrement complet de l'entité dans [ResearchSpace](/docs/tools/researchspace).

:::

## Comprendre le score de correspondance

Chaque entité candidate est affichée avec un score compris entre 0 et 100. Ce score représente le degré de similitude entre le texte de vos données et l'étiquette de l'entité dans le LINCS KG.

Il n'y a pas de seuil pour un score de concordance qui fonctionnerait pour toutes vos données. Le rapprochement est un processus semi-automatisé. Les candidats et les scores de concordance peuvent accélérer le processus, mais une connaissance humaine est nécessaire pour déterminer l'importance de la précision et pour confirmer les concordances.

Pour obtenir les meilleurs résultats, nous vous recommandons de nettoyer vos données avant de procéder au rapprochement. Dans la mesure du possible, nous vous recommandons également d'essayer le rapprochement sur différentes versions des noms que vous avez dans vos données.

{/*## Amélioration des données */}
{/**Section pour l'avenir. La fonctionnalité n'a pas encore été ajoutée. */}
{/*https://openrefine.org/docs/manual/reconciling#fetching-more-data */}

:::info

Pour utiliser vos données nouvellement rapprochées afin de créer :Term[Linked Open Data (LOD)]{#linked-open-data}, consultez notre [Workflow de conversion pour les données structurées](/docs/create-lod/workflow-paths?workflow=structured).

:::

## Demander de l'aide

Si vous rencontrez des problèmes ou si vous avez des suggestions pour le service d'autorité, veuillez signaler les problèmes sur [notre GitLab](https://gitlab.com/calincs/conversion/authority-service/-/issues). Si vos problèmes concernent OpenRefine de manière plus générale, veuillez contacter [l'équipe OpenRefine](https://openrefine.org/community).
