import React from "react";
// Import the original mapper
import MDXComponents from "@theme-original/MDXComponents";
import GridLayout from "@site/src/components/layouts/GridLayout";
import PrimaryButton from "@site/src/components/buttons/PrimaryButton";
import FunctionButton from "@site/src/components/buttons/ToolFunctionButton";
import UnderConstruction from "@site/src/components/links/UnderConstructionLink";
import GlossaryTerm from "@site/src/components/glossary/GlossaryTerm";
import YoutubeEmbed from "@site/src/components/tools/YoutubeEmbed";
import { ToolButtons } from "../components/lists/ToolButtonList";
import DatasetButtonList from "../components/lists/DatasetButtonList";

/*import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

library.add(faUpRightFromSquare)*/

export default {
  ...MDXComponents,
  PrimaryButton,
  FunctionButton,
  UnderConstruction,
  GridLayout,
  GlossaryTerm,
  YoutubeEmbed,
  ToolButtons,
  DatasetButtonList,
};
