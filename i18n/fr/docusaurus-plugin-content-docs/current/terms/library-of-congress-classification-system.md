---
id: library-of-congress-classification-system
title: Library of Congress Classification System (LCC)
definition: Un système de classification des bibliothèques couramment utilisé par les grandes bibliothèques de recherche et universitaires pour organiser les collections imprimées.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Le système de classification de la Bibliothèque du Congrès (LCC) est un système de classification développé par la Bibliothèque du Congrès en 1897. Bien qu'il ait été créé à l'origine pour les besoins spécifiques de la Bibliothèque du Congrès, il est actuellement utilisé par les grandes bibliothèques universitaires et de recherche pour organiser leurs collections d'imprimés.

## Autres ressources

- Librarianship Studies & Information Technology (2020) [“Library of Congress Classification”](https://www.librarianshipstudies.com/2017/11/library-of-congress-classification.html)
- [Library of Congress Classification Outline](https://www.loc.gov/catdir/cpso/lcco/)
- [Library of Congress Classification (Wikipedia)](https://en.wikipedia.org/wiki/Library_of_Congress_Classification)