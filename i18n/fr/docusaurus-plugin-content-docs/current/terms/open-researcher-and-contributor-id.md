---
id: open-researcher-and-contributor-id
title: Open Researcher and Contributor ID (ORCID)
definition: A not-for-profit organization that provides free Uniform Resource Identifiers (URIs) to researchers so they can be connected to their scholarship and bibliographic output.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Open Researcher and Contributor ID (ORCID) est une organisation à but non lucratif qui fournit gratuitement des :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier} aux chercheurs afin qu'ils puissent être reliés à leurs travaux et à leur production bibliographique. Outre la fourniture d'identifiants, ORCID propose également des enregistrements pour les chercheurs qui regroupent leurs travaux et :Term[Application Programming Interfaces (API)]{#application-programming-interface} pour permettre aux chercheurs de relier leur identifiant à leurs affiliations et à leurs contributions.

Les URI ORCID peuvent être utilisés pour :Term[réconciliation]{#entity-matching}. Les participants à LINCS sont encouragés à s'inscrire pour en obtenir un et à l'ajouter à leurs informations de compte.

## Exemples

- [Susan Brown (0000-0002-0267-7344)](https://orcid.org/0000-0002-0267-7344)
- [Sarah Roger (0000-0001-9979-0353)](https://orcid.org/0000-0001-9979-0353)

## Autres ressources

- [ORCID](https://orcid.org/)
- [ORCID (Wikipedia)](https://en.wikipedia.org/wiki/ORCID)
