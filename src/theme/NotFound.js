import React from 'react';
import Link from '@docusaurus/Link';
import Translate, {translate} from '@docusaurus/Translate';
import {PageMetadata} from '@docusaurus/theme-common';
import Layout from '@theme/Layout';
import PrimaryButton from "@site/src/components/buttons/PrimaryButton";
import { faHouse } from "@fortawesome/free-solid-svg-icons";


export default function NotFound() {
  return (
    <>
      <PageMetadata
        title={translate({
          id: "theme.NotFound.title",
          message: "Page Not Found",
        })}
      />
      <Layout>
        <main className="container margin-vert--xl">
          <div className="row">
            <div className="col col--6 col--offset-3">
              <h1 className="hero__title">
                <Translate
                  id="theme.NotFound.title"
                  description="The title of the 404 page">
                  Page Not Found
                </Translate>
              </h1>
              <p>
                <Translate
                  id="theme.NotFound.p1"
                  description="The first paragraph of the 404 page">
                  We could not find what you were looking for because:
                </Translate>
              </p>
              <ul>
                <li>
                  The link that brought you here was from our old website.
                  Please retry your URL with <code>old.</code> added to the
                  start of your URL or visit our old website at{" "}
                  <a
                    href="http://old.lincsproject.ca"
                    target="_blank"
                    rel="noopener noreferrer">
                    {" "}
                    old.lincsproject.ca
                  </a>
                  .
                </li>
                <li>
                  The link that brought you here is broken. Please check the URL
                  for typos and try again or{" "}
                  <Link to="docs/about-lincs/get-involved/contact-us">
                    get in touch
                  </Link>
                  {" "}to let us know about the broken link.
                </li>
              </ul>
              <div className="primary-button-row">
                <PrimaryButton
                  link="/"
                  buttonName="Return Home"
                  icon={faHouse}
                />
              </div>
            </div>
          </div>
        </main>
      </Layout>
    </>
  );
}
