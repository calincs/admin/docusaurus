---
id: federated-sparql-search
title: Recherche SPARQL fédérée
definition: Un point d’entrée unique pour accéder aux terminaux SPARQL distants afin qu’un service de requête puisse récupérer des informations à partir de plusieurs sources de données.
---

La recherche SPARQL fédérée donne accès à un :Term[SPARQL Endpoints]{#sparql-endpoint} afin qu’un service de requête puisse récupérer des informations à partir de plusieurs sources de données. La recherche fédérée fournit un point d’entrée unique vers plusieurs sources de données. Une clause SERVICE dans le corps d’un :Term[La requête SPARQL]{#sparql-protocol-and-rdf-query-language} peut être utilisée pour appeler un point de terminaison SPARQL distant.

## Exemples

- L’exemple suivant montre une requête SPARQL vide.

```sparql
SELECT [...]
WHERE {
     SERVICE <http://example1.example.org/sparql> {
          [...]
          OPTIONAL {
               [...]
               SERVICE <http://example2.example.org/sparql> {
                    [...]   }   }
     }
}
```

## Autres ressources

- [Federated Search (Wikipedia)](https://en.wikipedia.org/wiki/Federated_search)
- W3C (2013) [“SPARQL 1.1 Federated Query”](https://www.w3.org/TR/sparql11-federated-query/)
