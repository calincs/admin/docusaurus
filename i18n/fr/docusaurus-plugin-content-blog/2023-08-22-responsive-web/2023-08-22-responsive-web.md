---
slug: responsive-web
title: "Concevoir et réaliser des applications web réactives"
authors: Marco Lian Bantolino
tags: [UX]
hide_table_of_contents: true
image: ./responsive-web-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./responsive-web-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

Lors de la conception et de la réalisation d'une application web, il est primordial de veiller à ce qu'elle soit réactive. Une application réactive est belle et fonctionne bien sur toutes les tailles d'écran et tous les appareils. Les applications LINCS sont conçues pour être visualisées de différentes manières : des tablettes aux ordinateurs portables en passant par les écrans interactifs de la taille d'un tableau blanc. Il peut être difficile d'atteindre ce niveau de réactivité, mais avec l'aide d'outils, de techniques et de cadres CSS, la tâche devient beaucoup plus facile...{/* truncate */}

Il existe des systèmes de mise en page couramment utilisés, intégrés à CSS, appelés Flexbox et Grid. En outre, CSS dispose d'une fonctionnalité appelée media queries, qui permet au contenu de s'afficher différemment en fonction de la taille de la fenêtre de visualisation. Il existe également des cadres CSS tels que Bootstrap CSS, qui sont faciles à utiliser et qui conviennent parfaitement à la conception réactive.

Si vous utilisez le CSS vanille (standard) et souhaitez contrôler entièrement la conception, vous pouvez utiliser CSS Flexbox pour une présentation unidimensionnelle ou CSS Grid pour une présentation bidimensionnelle. [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) offre des propriétés personnalisables qui contrôlent la mise en page et l'alignement. Les éléments situés à l'intérieur d'un conteneur Flexbox sont appelés éléments Flex. Les éléments flexibles peuvent être disposés horizontalement ou verticalement en fonction de la propriété flex-direction définie (ligne, colonne, ligne inversée, etc.). En outre, les éléments flex peuvent être centrés verticalement ou horizontalement et peuvent grandir ou rétrécir en fonction de l'augmentation ou de la diminution de leur conteneur parent - des options qui vous permettent de créer une mise en page adaptée à vos besoins. La grille CSS possède également des propriétés qui peuvent être configurées. J'ai souvent recours à CSS Flexbox lors de la conception de l'Espace recherche de LINCS, car il me permet d'aligner horizontalement ou verticalement les nombreux composants que nous devons afficher, en gardant les éléments de la page distincts quelle que soit la taille de l'écran.

![Diverses dispositions flexbox telles que stretch ou flex-start où les éléments descendent à partir du haut ou flex-bottom où ils remontent à partir du bas.](<./responsive-web-flexbox-(c-LINCS).png>)

Rendre l'information accessible est l'un des principaux objectifs de LINCS et de ResearchSpace, et veiller à ce que les utilisateurs puissent voir clairement l'information affichée à l'écran en fait partie. Les requêtes de média sont une excellente technique CSS pour ce type de tâche. Les requêtes de média vous permettent de créer différentes mises en page en fonction de la taille de la fenêtre de visualisation (la zone visible par l'utilisateur d'une page web), ce qui en fait un outil très puissant. Par exemple, vous pouvez réorganiser les éléments d'un conteneur pour qu'ils s'adaptent à la taille de l'écran. S'il s'agit d'un écran de grande taille, vous pouvez placer les éléments sur une seule ligne. S'il s'agit d'un écran de taille moyenne, les éléments peuvent être disposés sur deux rangées. Le rendu du contenu en fonction de la taille de l'écran est essentiel dans une application web réactive, car il permet à l'utilisateur d'afficher la bonne quantité d'informations pour son écran.

Enfin, il existe des cadres CSS tels que [Bootstrap CSS](https://getbootstrap.com/docs/3.4/css/). Avec Bootstrap, vous pouvez rapidement et facilement créer des mises en page réactives qui s'affichent correctement sur toutes les tailles d'écran et tous les appareils, y compris les appareils mobiles. Bootstrap est couramment utilisé pour créer des grilles de présentation réactives. Un conteneur Bootstrap est divisé en douze colonnes, et il vous suffit de spécifier, par exemple, le nombre de colonnes qu'un élément doit occuper. Les fonctionnalités réactives sont déjà intégrées dans la mise en page, de sorte que tout le travail est fait pour vous lorsqu'il s'agit de rendre la mise en page réactive. ResearchSpace utilise Bootstrap CSS. L'un des cas où son application s'est avérée nécessaire s'est présenté lors de la refonte de la page d'agrégation des entités. Dans ce cas, j'ai eu besoin de construire une disposition en grille dans laquelle je me suis appuyé sur Bootstrap CSS. La conception qui en résulte, comme le montrent les captures d'écran suivantes, témoigne de sa réactivité sur différentes tailles d'écran, y compris l'iPad Pro (1024px x 1366px) et le moniteur Full HD standard (1920px x 1280px).

![ResearchSpace on iPad.](<./responsive-web-ipad-(c-LINCS).png>)

_Espace recherche sur un iPad Pro (1024px x 1366px)._

![Espace recherche sur moniteur.](<./responsive-web-monitor-(c-LINCS).png>)

_L'espace de recherche sur un moniteur full HD standard (1920px x 1280px)._

Ces outils et techniques CSS m'ont aidé à redessiner l'apparence de ResearchSpace pour le rendre plus réactif. Ces outils et techniques CSS m'ont aidé à redessiner l'apparence de ResearchSpace pour le rendre plus réactif. Ils sont rapides et faciles à utiliser, et leur documentation en ligne est excellente, de sorte qu'il n'est pas difficile de trouver des informations sur leur utilisation. J'espère qu'en les appliquant à l'Espace Recherche, les utilisateurs trouveront plus facilement des informations en utilisant LINCS.
