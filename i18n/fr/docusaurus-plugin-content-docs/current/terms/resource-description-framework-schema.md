---
id: resource-description-framework-schema
title: Schéma du cadre de description des ressources (RDFS)
definition: "Une extension du vocabulaire RDF (Resource Description Framework) de base qui peut être utilisé pour définir le vocabulaire (termes) à utiliser dans un graphe RDF."
---

Resource Description Framework Schema (RDFS) est une extension de la base :Term[Resource Description Framework (RDF)]{#resource-description-framework} vocabulaire qui fournit une modélisation de données :Term[vocabulaire]{#vocabulary} pour les données RDF. RDFS peut être utilisé pour définir les termes à utiliser dans un graphe RDF. Alors que RDF définit les instances, RDFS définit les types d’instances.

Les informations RDFS sont représentées à l’aide de RDF. Les ressources RDFS ont toutes :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier} avec le préfixe `<http://www.w3.org/2000/01/rdf-schema>#`. RDFS spécifie les classes (rdfs : ressource, littéral, langString, HTML, XMLLiteral, classe, propriété, type de données, instruction, sac, seq, alt, conteneur, conteneurMembershipProperty et liste) et :Term[propriétés]{#property} (rdfs : type, subClassOf, subPropertyOf, domaine, plage, étiquette, commentaire, membre, premier, repos, voirAussi, isDefinedBy, valeur, sujet, prédicat et objet). L’utilisation de RDFS ainsi que de RDF aide les utilisateurs à faire des descriptions plus détaillées de leurs données et à construire un simple :Term[ontologie]{#ontology}. :Term[Web Ontology Language (OWL)]{#web-ontology-language} peut être utilisé à la place pour créer une ontologie plus expressive et puissante.

## Exemples

- W3C (2014) _[RDF Schema 1.1](https://www.w3.org/TR/rdf-schema/)_

## Autres ressources

- Gandon et al. (2015) [“The Resource Description Framework and its Schema”](https://hal.inria.fr/hal-01171045/document)
- [RDF Schema (Wikipedia)](https://en.wikipedia.org/wiki/RDF_Schema)
