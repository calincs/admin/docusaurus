---
title: "XTriples Documentation"
Last Translated: 2024-06-18
---

## Transform TEI to CIDOC CRM

There are two ways to create TEI documents that XTriples can transform to CIDOC CRM:

1. Create CIDOC CRM from LEAF-Writer Files: Create documents using [LEAF-Writer](/docs/tools/leaf-writer)’s entity template files (records about people, places, organisations, or events) or LEAF-Writer’s Letter, Poem, and Prose templates with :Term[entity-matched]{#entity-matching} references to people.
2. Create CIDOC CRM directly from TEI 'ographies: Transform your existing entity-matched TEI to match the CWRC entity templates.

Currently, XTriples transforms placeographies and personographies, but will transform bibliographies, eventographies, orgographies, letters, poems, and prose to CIDOC CRM by then of the summer of 2023.

## Create CIDOC CRM from LEAF-Writer Files

Once you have filled in template files (currently available for records about [people](https://leaf-writer.leaf-vre.org/edit?template=People%20list) and [places](https://leaf-writer.leaf-vre.org/edit?template=Places%20list)) for your project in LEAF-Writer, upload them to [LINCS XTriples (beta)](https://app.xtriples.stage.lincsproject.ca/exist/apps/xtriples/index.html):

1. Click **upload** and select the entity type that corresponds to your template file type (e.g., select "Placeography reconciled in LEAF-Writer" from the dropdown menu if you are transforming place records created in LEAF-Writer).
2. Choose the [serialisation](/docs/terms/resource-description-framework-serialization) (XML or TTL) that you would like your output to be in. Both output options are CIDOC CRM representations of your :Term[TEI data]{#tei-data}.

:::note

The LINCS triplestore takes TTL as an input. If you are publishing your data with LINCS, choose TTL as your output. If you are planning to use the output in your own project, choose either XML or TTL as your output.

1. Click **download** and save the output on your own computer.
2. [optional] Submit your CIDOC CRM TTL to LINCS.

:::

## Create CIDOC CRM directly from TEI 'ographies

LINCS XTriples can process any TEI 'ography files that conform to the LINCS entity templates. If you have experience writing eXtensible Stylesheet Transformations (XSLTs), you can transform your TEI to match the LINCS templates in LEAF-Writer. By the end of the summer of 2023 this documentation guide will contain XSLTs that you can modify and run to transform your project’s 'ographies into the LINCS 'ography TEI. If your TEI does not contain URIs, load your transformed ’ography files into LEAF-Writer for entity matching, and follow the steps in [Create CIDOC CRM from LEAF-Writer Files](#create-cidoc-crm-from-leaf-writer-files).

## Notes

The following notes outline which TEI elements and attributes LINCS XTriples uses to create CIDOC CRM from your TEI, and how to add these TEI elements, attributes, and values in LEAF-Writer.

#### Person URI

XTriples pulls the cannonical :Term[authority record URI]{#authority-record} from the @ref value of the first child &lt;persName> inside &lt;person>. In LEAF-Writer click on the **Find Person** icon to have LEAF-Writer match the person's name against authority records on the Web. Select the authority :Term[URI]{#uniform-resource-identifier} that matches the person for whom you are creating a TEI record.

If you are editing your TEI in another editor add a @ref attribute to &lt;persName> and paste the authority URI for your person into the @source value. Add a @type="standard" attribute and value pair to the &lt;persName>.

#### Occupation

The LINCS CIDOC CRM representation of occupation draws on the &lt;occupation> @source value. We recommend using the occupation URI from the [LINCS Occupation Vocabulary](https://vocab.lincsproject.ca/Skosmos/occupation/en/), but you may use URIs from other vocabularies (for example, the [Art & Architecture Thesaurus occupations vocabulary](https://www.getty.edu/vow/AATFullDisplay?find=painter&logic=AND&note=&page=1&subjectid=300024980)).

In LEAF-Writer, right click on **Preferred Occupation Term** in the template and select **Edit Tag**. Choose the @source attribute, and paste in the occupation URI then click **OK** to save your changes.

If you are editing your TEI in an other editor, add a @source attribute to &lt;occupation> and paste the occupation URI into the @source value.

#### Gender

The LINCS XTriples CIDOC CRM representation of gender draws on the &lt;sex> @source value in the LINCS person template. We recommend using a gender URI from the [Vocabulary Browser > Canadian Writing Research Collaboratory Vocabulary > Gender > Narrower Concepts](https://vocab.lincsproject.ca/Skosmos/cwrc/en/page/Gender) but you may use URIs from other vocabularies (for example [Homosaurus](https://homosaurus.org/))

In LEAF-Writer, right click on **sex** in the template and select **Edit Tag** to cut and paste the gender URI into the @source value.

If you are editing your TEI in another editor add a @source attribute to &lt;sex> and paste the gender URI into the @source value.

The LEAF-Writer templates will eventually be updated to reflect the latest TEI encoding guidelines for [sex and gender](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ND.html#NDPERSEpc).

#### Nationality

LINCS XTriples does not currently process the &lt;nationality> element.
