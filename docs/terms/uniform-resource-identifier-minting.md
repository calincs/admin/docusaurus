---
id: uniform-resource-identifier-minting
title: Uniform Resource Identifier (URI) Minting
definition: The process of creating a new Uniform Resource Identifier (URI) to represent an entity.
---

In :Term[mapping]{#mapping} :Term[semi-structured data]{#semi-structured-data} and :Term[structured data]{#structured-data} into :Term[Linked Open Data (LOD)]{#linked-open-data}, a number of new :Term[entities]{#entity} representing events, activities, and other containers for data are generated during data :Term[transformation]{#transformation} and :Term[ingested]{#ingestion}. To avoid using :Term[blank nodes]{#blank-node}, new :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} need to be minted to represent these new entities and to allow the building of complex new relationships between entities represented by URIs from commonly used :Term[namespaces]{#namespace}.

## Examples

- In the AdArchive Dataset: The following URI, minted by LINCS, is a `crm:E33_Linguistic_Object` entity referencing text on an advertisement for _Women’s Studies International Quarterly_.

  `lincs:gs2EnZaEjmk`

- University of Saskatchewan Art Gallery: The following URI, minted by LINCS, has the label of “buff writing paper,” used to form a materials statement for University of Saskatchewan Object 1985.007.079.

  `lincs:tfi3YsPWqES`

## Further Resources

- WC3 (2012) [“223 Best Practices URI Construction”](https://www.w3.org/2011/gld/wiki/223_Best_Practices_URI_Construction)
