---
description: "Clean and transform structured data."
title: "OpenRefine"
Last Translated: 2023-05-15
image: /img/documentation/openrefine-overview-logo-(c-owner).png
---

<ToolButtons toolName="OpenRefine"/>

OpenRefine is a data processing application that allows you to clean up and transform :Term[structured data]{#structured-data}. It has several functionalities suited for creating :Term[Linked Open Data (LOD)]{#linked-open-data}, such as :Term[entity matching]{#entity-matching}, format translation, :Term[Resource Description Framework (RDF)]{#resource-description-framework} :Term[mapping]{#mapping}, and export options.

<div className="banner">
  <img src="/img/documentation/openrefine-overview-logo-(c-owner).png" alt="" />
</div>

<div className="primary-button-row">
  <PrimaryButton
    link="https://openrefine.org/"
    buttonName="To the Tool"
    />
  <PrimaryButton
    link="https://docs.openrefine.org/"
    buttonName="To the Documentation"
    />
  <PrimaryButton
    link="https://github.com/OpenRefine/OpenRefine"
    buttonName="To GitHub"
    />
</div>

## OpenRefine and LINCS

Within the LINCS project, OpenRefine is used for [data cleaning](/docs/create-lod/clean-data/#clean-your-dataset) and [entity matching](/docs/create-lod/match-entities). It is primarily used by researchers bringing their own datasets to the project. OpenRefine allows for these domain experts to have full control over the changes made to their data.

OpenRefine is best suited for structured data, since it will represent the data in a format similar to a spreadsheet or table. Any file type that follows a similar system, such as comma separated values (CSV), is best, though it is also compatible with other file types like :Term[XML]{#xml}, :Term[JSON]{#json}, and RDF. If a researcher’s data falls within a certain domain or is unstructured, a different tool may be more appropriate:

- Use [LINCS-API](/docs/tools/lincs-api) or [NERVE](/docs/tools/nerve) for an unstructured dataset.
- Use [VERSD](/docs/tools/versd) to match entities for an entire bibliographic dataset.

The software can be downloaded from [OpenRefine’s website](https://openrefine.org/). When launched, the application will open in a browser tab that runs locally on your computer.

Though this tool can be useful for researchers and data specialists outside of LINCS, it is important for those who are in the process of getting their data into the LINCS system to begin cleaning and matching entities in OpenRefine early in the data preparation process.

Check out the [Authority Service](/docs/tools/authority-service) to match entities in your data against the LINCS Knowledge Graph from within OpenRefine.

## Prerequisites

- You don't need to create a user account.
- You do need to have your own dataset.
- A basic understanding of [entity matching](/docs/create-lod/match-entities) and [data cleaning](/docs/create-lod/clean-data/#clean-your-dataset) is required.


OpenRefine supports the following inputs and outputs:

- **Input:** CSV, TSV, XLS, XLSX, JSON, XML, RDF, plain text, and more
- **Output:** CSV, TSV, XLS, XLSX, HTML-formatted tables, and more

## Resources

To learn more about OpenRefine, see the following resources:

**Clean Data:**

- [OpenRefine User Manual](https://docs.openrefine.org/)
- Rue & Hernandez (2019) [“Using OpenRefine to Clean Your Data”](https://multimedia.report/tutorials/openrefine/)
- Hervieux (2020) [“OpenRefine Activity”](https://docs.google.com/presentation/d/1RVvRpLRP-xUONOAf502ZfEHuJPSlD32eQYq2EgovdvI/edit#slide=id.g132ca824ecf_0_413) [PowerPoint]
- van Hooland, Verborgh, & De Wilde (2021) [“Cleaning Data with OpenRefine”](https://programminghistorian.org/en/lessons/cleaning-data-with-openrefine)

**Match Entities:**

- [OpenRefine User Manual—Reconciling](https://docs.openrefine.org/manual/reconciling)
- Getty Digital (2020) [“Getty Vocabularies OpenRefine Tutorial and Tips for Advanced Users”](https://www.getty.edu/research/tools/vocabularies/obtain/getty_vocabularies_openrefine_tutorial.pdf)

Information about the team that developed [OpenRefine](/docs/about-lincs/credits/tools-credits#openrefine) is available on the Tool Credits page.
