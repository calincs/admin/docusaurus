---
id: web-annotation-data-model
title: Web Annotation Data Model (WADM)
definition: Une norme pour le formatage et la structuration des annotations Web.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Le modèle de données d'annotation web (WADM) est une norme de formatage et de structuration des :Term[annotations web]{#web-annotation}. Il a été conçu pour rendre les descriptions d'annotations web cohérentes et donc plus faciles à partager et à réutiliser dans différentes sources. LINCS utilise WADM pour décrire les sources utilisées dans les ensembles de données en cours de conversion.

WADM n'est pas une extension de :Term[CIDOC CRM]{#cidoc-crm}, ni un :Term[event-centric]{#event-oriented-ontology} par nature. Bien qu'il n'y ait pas d'alignement préexistant entre le WADM et le CIDOC CRM, les personnes qui travaillent sur les ontologies se chevauchent, ce qui signifie que des efforts sont faits pour trouver des moyens de faire travailler ensemble le WADM et le CIDOC CRM.

## Autres Ressources

- [Application Profile for Web Annotations](/docs/explore-lod/understand-lincs-data/application-profiles-main/sources-metadata)
- W3C (2017) [“Web Annotation Data Model”](https://www.w3.org/TR/annotation-model/)