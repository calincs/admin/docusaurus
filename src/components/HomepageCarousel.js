import Translate from "@docusaurus/Translate";
import React from "react";

import "swiper/css";
import "swiper/css/keyboard";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { A11y, Keyboard, Navigation, Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";

import CarouselCard from "./cards/CarouselCard";
import MobileCarouselCard from "./cards/MobileCarouselCard";
import datasets from "./datasets";
import styles from "./homepage.module.css";

import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const carouselItems = Object.values(datasets).filter((dataset) => dataset.featured);

function HomepageCarousel() {
  return (
    <div>
      <div className={styles.mobileCarousel}>
        <Swiper
          modules={[Navigation, Pagination, A11y, Keyboard]}
          spaceBetween={8}
          breakpoints={{
            880: {
              slidesPerView: 2.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            780: {
              slidesPerView: 2.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            680: {
              slidesPerView: 2.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            580: {
              slidesPerView: 1.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            480: {
              slidesPerView: 1.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            380: {
              slidesPerView: 1.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
          }}
          rewind={true}
          pagination={{ clickable: true }}
          keyboard={{ enabled: true }}
          style={{ "--swiper-pagination-color": "#107386" }}
        >
          {carouselItems.map((object) => (
            <SwiperSlide key={object.id}>
              <MobileCarouselCard dataset={object} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>

      <div className={styles.carouselContainer}>
        <div className={styles["carousel-swiper-button-next"]}>
          <span>
            <FontAwesomeIcon icon={faChevronRight} />
          </span>
        </div>
        <div className={styles["carousel-swiper-button-prev"]}>
          <span>
            <FontAwesomeIcon icon={faChevronLeft} />
          </span>
        </div>
        <div className={styles.carousel}>
          <Swiper
            modules={[Navigation, A11y, Keyboard]}
            spaceBetween={12}
            slidesPerView={2.25}
            navigation={{
              nextEl: `.${styles["carousel-swiper-button-next"]}`,
              prevEl: `.${styles["carousel-swiper-button-prev"]}`,
            }}
            keyboard={{ enabled: true }}
            style={{ "--swiper-pagination-color": "#107386" }}
          >
            {carouselItems.map((object) => (
              <SwiperSlide key={object.id}>
                <CarouselCard dataset={object} />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </div>
  );
}

export default HomepageCarousel;
