---
id: union-list-of-artist-names
title: Union List of Artist Names (ULAN)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les noms, les relations et les informations biographiques concernant les personnes et les personnes morales liées à l’art, à l’architecture et à d’autres cultures matérielles.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

L'Union List of Artist Names (ULAN) est l'un des cinq vocabulaires Getty développés par le Getty Research Institute pour fournir des termes et des :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} pour les concepts et les objets liés aux arts. LINCS utilise l'ULAN pour fournir des URI pour les noms, les relations et les informations biographiques concernant les personnes et les organismes liés à l'art, à l'architecture et à d'autres cultures matérielles.

L'ULAN comprend des termes relatifs aux :

- les architectes
- les artistes
- Entreprises
- Autres créateurs (nommés ou anonymes)
- Mécènes
- Dépositaires d'œuvres d'art

L'ULAN exclut les termes désignant les personnages fictifs et littéraires qui sont le sujet d'une œuvre visuelle, les personnes physiques et morales qui sont nommées dans la documentation, mais dont l'identité est inconnue, et les mentions d'attribution, telles que celles qui désignent des studios ou des ateliers.

## Autres ressources

- J. Paul Getty Trust (2022) [“About the ULAN”](https://www.getty.edu/research/tools/vocabularies/ulan/about.html)
- [Union List of Artist Names (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/index.html)
