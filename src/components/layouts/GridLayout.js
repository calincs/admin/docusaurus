/** @format */

import React from "react";
import styles from "./layouts.module.css";

function GridLayout({ children, maxColumns = null }) {
  if (maxColumns) {
    const fractions = 100.0 / maxColumns;
    const columns = `repeat(${maxColumns},${fractions}%)`;
    return (
      <div
        className={styles.gridLayout}
        style={{ gridTemplateColumns: columns }}>
        {children}
      </div>
    );
  }
  return <div className={styles.gridLayout}>{children}</div>;
}

export default GridLayout;
