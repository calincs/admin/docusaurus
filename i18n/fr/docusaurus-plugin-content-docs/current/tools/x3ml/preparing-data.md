---
sidebar_position: 2
title: "Préparation des données pour 3M"
description: "Conseils pour préparer les données pour la conversion 3M"
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-17
---

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

## Introduction

Pour utiliser 3M afin de convertir vos données en triples CIDOC CRM, vos données de départ - ou données sources - doivent être formatées en :Term[XML]{#xml}. Cette page couvre les principes directeurs que LINCS suit lors de la préparation des données sources pour la conversion à l'aide de 3M.

#### Commencer avec des données XML
Si vos données sources sont déjà en XML, il est possible d'apporter de petites modifications à la structure de départ pour faciliter le travail avec 3M.

### Démarrer avec des données non XML

Si vos données sources sont dans un format autre que XML, vous pouvez les convertir en XML assez facilement et utiliser ensuite 3M.

:::info
Vous ne savez pas si vous devez convertir vos données en XML et utiliser 3M, ou si vous devez convertir vos données d'une autre manière ? Pour en savoir plus sur les options qui s'offrent à vous, consultez notre [Documentation sur le processus de conversion des données](/docs/create-lod/map-data/implement-conceptual-mapping).
:::


## A quoi devrait ressembler mon XML ?

Étant donné que vous établirez des correspondances personnalisées dans 3M, il n'y a pas de structure XML spécifique ou de schéma d'affectation des noms à respecter. Cependant, cette page donne des suggestions de structures qui facilitent la conversion.

Le nom que vous donnez aux éléments de votre source XML n'a d'importance que dans la mesure où vous pouvez facilement suivre la signification des informations et que vous êtes cohérent dans l'ensemble de vos données. Les noms des éléments XML ne se retrouvent pas dans les données finales :Term[RDF]{#resource-description-framework}, de sorte que les noms ne sont importants que si vous publiez votre XML ailleurs.

Les critères importants pour votre XML sont les suivants :

- Votre XML est un XML valide. Il existe des validateurs en ligne que vous pouvez utiliser pour vérifier.
- Les éléments de votre XML ont des sous-éléments pour chaque relation que vous vous attendez à voir dans le RDF de sortie.
- Le texte pertinent est imbriqué de manière à ce que vous puissiez avoir des étiquettes significatives dans le résultat.
- Il existe des identifiants uniques cohérents dans l'ensemble des données afin que les données finales ne représentent pas la même entité avec plus d'un identifiant.

## Exemples

Voici quelques exemples de XML d'entrée et les diagrammes de mappage correspondants issus de projets antérieurs. La structure de votre propre source XML dépendra de vos propres diagrammes de correspondance, mais ces exemples devraient vous donner une idée de ce à quoi ressemblera l'étape suivante.

### Anthologia graeca

{/*Pour chaque exemple de données, résumez le point clé que nous voulons faire passer. Peut-être un exemple de données par principe */}

Les données de l'Anthologia graeca ont commencé sous la forme d'une base de données relationnelle, à partir de laquelle nous avons extrait un fichier XML pour chaque grand type d'objet dans les données. Dans chacun de ces fichiers XML, nous avons un élément XML pour chaque instance de ce type d'objet. Nous avons ensuite des sous-éléments pour chaque relation qui devrait être présente dans le RDF final. Cela nous a souvent obligés à joindre des tables lorsque nous avons interrogé les données relationnelles afin de nous assurer que nous avions accès aux bonnes informations pour les étiquettes détaillées.

Voici le [diagramme CRM](https://drive.google.com/file/d/1okP72BlKUzUzx3X0AiFQnLr7MuOvI7dn/view?usp=sharing) pour ce projet.

Le fichier [city_sample.xml](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/Anthologia_palatina/data/source_xml/samples/city_sample.xml) est un exemple où nous avions un élément `<city>` pour chaque instance d'une ville. Les sous-éléments nous ont permis de donner des détails sur cette ville dans le RDF.

```
<city>
	<city_id>116</city_id>
	<city_unique_id>122</city_unique_id>
	<city_name>Colchis</city_name>
	<city_name_language_id>eng</city_name_language_id>
	<city_urn_id>5737</city_urn_id>
	<city_urn>https://www.wikidata.org/entity/Q183150</city_urn>
</city>
```

De la même manière que nous avons des éléments `<city>`, nous avons aussi des éléments `<city_names>` et `<city_alt_urn>` séparés pour chaque nom alternatif ou chaque urne pour une ville, de sorte que nous pouvons créer une nouvelle relation de nom pour chaque instance de `<city_names>` et une nouvelle relation d'urne pour chaque instance de `<city_alt_urn>` et les relier à la `<city>` correspondante en utilisant l'élément `<city_id>` qui était commun à travers tous les éléments. 

Voici le [résultat RDF](https://gitlab.com/calincs/conversion/metadata-conversion/-/tree/master/Anthologia_palatina/X3ML/output) du mappage 3M de ce projet. Vous constaterez que notre mappage 3M a créé des URI temporaires pour chaque objet et pour de nombreux nœuds intermédiaires qui relient les objets. Pour les villes, ces URI sont de la forme `<http://temp.lincsproject.ca/anthologie/place/city/8>`. C'est là que les identificateurs internes uniques s'avèrent utiles, car nous pouvons commencer avec un XML décomposé en petits éléments qui ne représentent qu'une seule relation, mais tant que nous utilisons toujours des URI temporaires ayant le même format et le même identificateur unique, toutes ces relations seront connectées correctement dans le RDF final.

L'étape finale consiste à remplacer ces URI temporaires par des URI persistants. Il peut s'agir d'URI que vous possédez et qui resteront persistants, d'identifiants externes que nous utilisons à partir de fichiers d'autorité existants, ou d'identifiants créés par LINCS.

### Collection d'art de l'Université de Saskatchewan

{/*I think we can discuss that both ways of organizing the XML are options. In case it's how they already have their xml, they know they don't have to change it.

I think that some ways will be more intuitive to different people. Like if someone is used to relational data then they may think differently from someone very used to highly nested xml. I could be wrong and maybe there is an objectively "best" way.

We can talk about how  nesting all the information (basically un-normalizing the database) made working with 3M a bit faster. But if it's a lot easier for them to export each table of their data separately without combining all the info into the same xml element then that could be worth the time saving there.
 */}

La collection d'œuvres d'art de l'université de Saskatchewan avait une structure XML initiale plus simple, où chaque élément parent `<rdf:Description>` représentait un objet d'art et toutes les informations associées à cet objet étaient imbriquées dans un sous-élément. Tout était déjà compatible avec les URI officiels, de sorte qu'il n'était pas nécessaire d'utiliser d'autres identifiants internes.

{/*Here is an [example of the input XML](https://gitlab.com/calincs/conversion/metadata-conversion/-/raw/master/USask_art_object/data/pre_processed/combined_noTypes.xml).
You can see the converted data [here](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/Datasets/usaskart.ttl). */}
Voici le [diagramme CRM](https://drive.google.com/file/d/1hKkigJMYRTkjIwWT9THRCs9KCWd-uAWr/view?usp=sharing) pour ce projet.

Ce projet montre des exemples d'utilisation d'attributs XML pour donner des détails sur d'autres parties des données au lieu d'éléments XML complètement séparés. Il s'agit d'un exemple,

```
<rdf:Description>
	…
	<Category url="http://vocab.getty.edu/aat/300033618">painting</Category>
	…
</rdf:Description>
```

Cela nous a permis d'utiliser l'URI `<http://vocab.getty.edu/aat/300033618>` chaque fois qu'un objet entrait dans la catégorie de la peinture. La valeur de l'élément "peinture" est devenue l'étiquette de l'objet `<http://vocab.getty.edu/aat/300033618>` dans les données finales.

Il existe de nombreuses alternatives possibles quant à l'aspect et à la décomposition des données XMl d'entrée. Voici deux exemples qui auraient fonctionné de manière équivalente dans 3M, et il existe certainement d'autres manières valables de représenter les mêmes données.

#### Example 1:

```
<rdf:Description>
	…
	<Category>http://vocab.getty.edu/aat/300033618</Category>
	…
</rdf:Description>

<Category>
	<url>http://vocab.getty.edu/aat/300033618</url>
	<name>painting</name>
</Category>
```

#### Example 2:
```
<rdf:Description>
	…
	<CategoryID>40</CategoryID>
	…
</rdf:Description>

<CategoryName>
	<ID>40</ID>
	<name>painting</name>
</CategoryName>

<CategoryURL>
	<ID>40</ID>
	<URL>http://vocab.getty.edu/aat/300033618</URL>
</CategoryURL>
```

### YellowNineties

{/*If right make sure to use diagrams to make this even clearer as the XML looks a little confusing at first glance 
you needed an ID for the relationship itself right? The people already had their own IDs, but the relationship needed one too? As in, they were both colleagues of each other?
yes that's right. Each relationship needed an identifier but if two people had a relationship together then it needed to be that same relationship regardless of if we created the ID in 3M from person_A or person_B.

Agreed, some visuals can make this way clearer and cut out a lot of the text
*/}

Ce projet avait une structure XML tout aussi simple, où chaque balise parentale `<rdf:Description>` représentait une personne et les sous-éléments représentaient toutes les informations relatives à cette personne.

{/*The input XML is [here](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/YellowNineties/data/oct18_pre_processed.xml).
The output RDF is [here](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/Datasets/yellow1890s.ttl?expanded=true&viewer=simple). */}

Ces données ont nécessité une étape de prétraitement au cours de laquelle nous avons ajouté des attributs au XML afin de faciliter l'ajout d'étiquettes significatives au RDF et d'éviter la création d'identifiants multiples pour la même relation. Un exemple est celui d'une relation entre deux personnes comme "collègue_de". À l'origine, cette relation était exprimée de part et d'autre en utilisant uniquement les identifiants des personnes, comme ceci :

```
<rdf:Description rdf:about="https://personography.1890s.ca/persons/2617">
	…
	<y90s_colleague_of rdf:resource="https://personography.1890s.ca/persons/3036"/>
</rdf:Description>


<rdf:Description rdf:about="https://personography.1890s.ca/persons/3036">
	…
	<y90s_colleague_of rdf:resource="https://personography.1890s.ca/persons/2617"/>
</rdf:Description>
```

De ce fait, la relation colleague_of apparaissait comme deux événements distincts dans nos données finales. Nous avons donc ajouté un identifiant unique à la relation dans le XML source et une étiquette. Comme ceci :

```
<rdf:Description rdf:about="https://personography.1890s.ca/persons/2617">
	…
	<y90s_colleague_of 
		rdf:resource="https://personography.1890s.ca/persons/3036" 
		relationship_id="colleague_of#26173036" 
		relationship_label="Colleague relationship between Cahan, Abraham and Hapgood, Norman" />
</rdf:Description>


<rdf:Description rdf:about="https://personography.1890s.ca/persons/3036">
	…
	<y90s_colleague_of 
		rdf:resource="https://personography.1890s.ca/persons/2617" 
		relationship_id="colleague_of#26173036" 
		relationship_label="Colleague relationship between Cahan, Abraham and Hapgood, Norman" />
</rdf:Description>
```

De cette manière, nous pouvons représenter la relation à l'aide de l'identifiant unique "colleague_of#26173036" et même si nous définissons cette relation deux fois à partir de chacune des balises `<rdf:Description>` des participants à la relation, toutes les informations seront fusionnées correctement en raison de l'identifiant partagé.

## FAQ

### Je ne suis pas sûr de la meilleure façon de configurer mon XML. Comment puis-je commencer ?

Suivez notre [tutoriel guidé 3M](/docs/tools/x3ml/create-mapping) en utilisant nos données d'exemple afin de comprendre les bases de 3M.

Une fois que vous saurez comment fonctionnent les projets de cartographie dans 3M, il vous sera plus facile de réfléchir à ce qui conviendra le mieux à vos données.

Vous pouvez également créer manuellement quelques petits échantillons de vos données en utilisant les différentes configurations que vous envisagez et vous amuser à créer des exemples de mappings 3M pour eux. Essayez de configurer le même mappage de relation unique dans 3M pour chaque échantillon de données. Qu'est-ce qui vous a permis d'obtenir le plus facilement toutes les informations dont vous aviez besoin ?

### Comment convertir mes données en XML ?

Nous avons tendance à créer des scripts python personnalisés pour convertir d'autres formats :Term[structured data]{#structured-data} en XML. Revenez bientôt pour voir des exemples de scripts.

Il existe également de nombreux outils en ligne qui peuvent vous aider en fonction du format de vos données sources. [OpenRefine](/docs/tools/openrefine/) est un exemple qui permet d'utiliser différents formats d'entrée et de sortie.

{/*### What about RDF/XML? */}