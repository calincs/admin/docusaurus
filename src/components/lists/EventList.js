import eventData from "../events";
import StaticCard from "../cards/StaticCard";
import GridLayout from "../layouts/GridLayout";

const filterTypes = ["All", "Upcoming", "Past", "Ongoing"];

function filterEvents(filterType) {
  const currentDate = new Date();

  const filterConditions = {
    All: () => true,
    Upcoming: (event) => new Date(event.startDate) > currentDate,
    Past: (event) => new Date(event.endDate) < currentDate,
    Ongoing: (event) => new Date(event.startDate) <= currentDate && new Date(event.endDate) >= currentDate,
    Workshop: (event) => event.eventType === "workshop",
    Conference: (event) => event.eventType === "conference",
    Meeting: (event) => event.eventType === "meeting",
    Other: (event) => event.eventType === "other",
  };

  const filterCondition = filterConditions[filterType] || filterConditions.All;

  const filteredEvents = Object.entries(eventData).filter(([key, event]) => filterCondition(event));

  // Transform the filtered entries back into an object
  return Object.fromEntries(filteredEvents);
}

function EventList({ filter = "All" }) {
  const filteredEvents = filterEvents(filter);

  return (
    <GridLayout maxColumns={2}>
      {Object.keys(filteredEvents).map((x) => (
        <StaticCard
          key={x}
          {...filteredEvents[x]}
          src={filteredEvents[x]["image"]}
          link1={filteredEvents[x]["link"]}
          subtitle={`${filteredEvents[x]["datePretty"]} • ${filteredEvents[x]["location"]}`}
          imageFit="contain"
        ></StaticCard>
      ))}
    </GridLayout>
  );
}

export { EventList, filterEvents };