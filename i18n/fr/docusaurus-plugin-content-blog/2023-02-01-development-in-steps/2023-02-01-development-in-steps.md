---
slug: development-in-steps
title: "Le développement par étapes: Apprendre à collaborer sur un projet technique"
authors: Basil Yusef
tags: [technical]
hide_table_of_contents: true
image: ./development-in-steps-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./development-in-steps-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

LINCS utilise ResearchSpace comme plateforme pour explorer les relations dans les ensembles de données culturelles interconnectées. Avec ResearchSpace, les chercheurs peuvent parcourir, rechercher et visualiser des données dans le dépôt de données du LINCS. Durant l'été 2022, j'ai fait partie d'une équipe qui développait de nouvelles fonctionnalités pour la version de ResearchSpace du LINCS.

Notre équipe était une collaboration entre des contributeurs ayant une expérience de l'expérience utilisateur (UX) et des contributeurs ayant une expérience du développement de logiciels. Nous avons travaillé en tandem : le groupe UX a recommandé des fonctionnalités pour améliorer l'expérience d'utilisation de l'application web. Ces recommandations ont été transformées en tickets dans [GitLab](https://gitlab.com/calincs). Les tickets décrivaient le produit final souhaité et il incombait aux développeurs de déterminer comment atteindre cet objectif. Le responsable du développement a réparti les tickets entre les développeurs de logiciels, en les divisant en fonction de ce qui serait nécessaire pour construire les fonctionnalités suggérées...{/* truncate */}

La plupart des tickets qui m'ont été attribués concernaient des composants nécessitant React.js. Lorsque j'ai commencé à travailler pour LINCS, je n'avais pas beaucoup d'expérience avec React. Même si je connaissais déjà les crochets React, j'avais besoin d'apprendre le React basé sur les classes qui était utilisé dans ResearchSpace. Souvent, je devais en savoir plus que ce que mon billet me demandait de faire. Je devais comprendre l'importante base de code du projet et son fonctionnement tout en élargissant mes compétences. C'est ainsi que j'ai développé une expertise en TypeScript et React et que j'ai appris le fonctionnement de Docker et des conteneurs Docker. Ces choses ont pu être déroutantes au début, mais elles me sont désormais familières. Par exemple, j'ai maintenant une bonne compréhension du fonctionnement des fichiers Docker Compose et j'ai beaucoup d'expérience dans l'utilisation des modèles Handlebar pour les corrections de bogues.

J'ai appris bien plus que des compétences techniques. J'ai beaucoup de gratitude envers les membres de mon équipe. Zach Schoenberger, qui a supervisé mon travail, m'a aidé à développer la compétence cruciale d'expliquer des concepts techniques à des personnes non techniques. Dawson MacPhee, un étudiant développeur senior, m'a appris à aborder mon travail. Il m'a appris à apporter des améliorations progressivement, à effectuer plusieurs tests et à diviser mes objectifs en éléments gérables. Dawson m'a appris à me référer à la documentation chaque fois que nécessaire et m'a encouragé à poser des questions au fur et à mesure.

Apporter des modifications sans bouleverser ce qui avait été établi précédemment était un défi, mais grâce aux compétences acquises auprès de Zach et de Dawson, j'ai été en mesure d'apporter des changements graduels avec succès. ResearchSpace comporte de nombreux composants React interconnectés. Au début, j'ai eu du mal à faire des changements sans casser d'autres choses, mais au fil du temps, j'ai développé mes compétences techniques et non techniques, ce qui m'a permis de transformer les demandes UX en composants fonctionnels.
