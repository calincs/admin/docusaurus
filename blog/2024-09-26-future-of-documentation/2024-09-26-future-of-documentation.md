---
slug: future-of-documentation
title: "The Future of Documentation"
authors: Kirisan Suthanthireswaran
tags: [CWRC, technical, UX]
hide_table_of_contents: true
image: /future-of-documentation-unsplash.jpg
Last Translated: 2024-08-21
date: "2024-09-26"
description: "Struggling to make documentation easier to navigate? Discover how Docusaurus transformed the CWRC documentation site by improving accessibility, readability, and navigation. Read about Kirisan's experience migrating the site and how this powerful tool makes it easier for developers to focus on content while enhancing user experience."
---

![](<./future-of-documentation-unsplash-(cc0).jpg>)

## The Problem

Documentation is an integral part of all software products, and for it to be useful, it must be comprehensive and detailed. However, this presents a challenge for both users and developers. Users, especially those who are newcomers to the software, can find it daunting to sift through large blocks of text, while developers often struggle to make documentation appealing and digestible.

Throughout university, I have been in the former category. As a user, I have always found documentation difficult to grasp. It often left me with more questions than answers. It especially did not help that the documentation websites I used often had terrible navigation and few resources to help me grasp what I was reading, leaving me scouring through various sites to find information that was tucked away in some hidden corner.

As a co-op student with LINCS, I was tasked with migrating the Canadian Writing Research Collaboratory (CWRC) [documentation site](https://sparql.cwrc.ca/). This website provided information about and access to the three ontologies developed by the CWRC, each with its own set of documentation. This had me worried, because how could I develop a website that could display documentation effectively while taking into consideration navigation, aesthetics, and page layouts?

{/* truncate */}

## Docusaurus

This is where [Docusaurus](https://docusaurus.io/) comes in. Docusaurus is a powerful static site generator built with React, which enables users to quickly and easily create documentation-focused websites. Because Docusaurus is built specifically for documentation, I was able to focus on presenting the content instead of worrying about the design. Rather than relying on HTML tags to display content, Docusaurus makes use of Markdown (MD) and Markdown eXtended (MDX), a user-friendly language with a simple syntax that can be picked up easily, and which also provides support for Javascript XML (JSX). This allowed me to combine the simplistic nature of MD while also harnessing the flexibility and power of JSX.  Docusaurus combines everything as static HTML files that I could easily deploy to the CWRC website, and it even allowed for embedded React components. Without sacrificing functionality, Docusaurus has streamlined the process of creating and publishing documentation.

Docusaurus made it easy for me to create the sort of website that the CWRC documentation needed. A goal of migrating the CWRC Linked Data website was to enhance the accessibility and readability of CWRC’s information. The original website, while functional, lacked consistent structure and navigation, making it difficult to use.

![CWRC's table of contents before and after, where the after has increased readablity due to being properly nested and font/colour changes.](future-of-documentation-before-after-(c-LINCS).png)
(Figure: Before and After)

These difficulties also translated to the implementation, as the coding structure for the documentation felt cluttered due to the number of HTML tags needed to display the text. This is where the simplicity of Markdown shined, as it streamlined the process of transitioning information over to the new website. While I was unable to fully transition from HTML tags, much of the documentation is now formed with Markdown, which will aid in the process of future refactoring or editing.

## Useful Tools

This isn’t to say that Docusaurus is only useful for its Markdown capabilities, as it also has many useful components and hooks. One such component that I used often was ```<Link/>```, which enables linking for both internal and external pages. What separates this component from basic HTML linking tags like ```<a/>``` is its ability to preload resources before navigating to the resource’s page. By making use of the “onMouseOver” event to fetch the requested resources of that page, Docusaurus preloads those resources to ensure seamless and efficient page transitions. Access to additional information through both internal and external links makes documentation clearer and easier to navigate, therefore reducing for users the sorts of struggles that I have had using documentation for other software.

In tandem with its various components, Docusaurus also supports ESLint plugins with additional rules, which are compatible with its unique components. These plugins allow users who are new to Docusaurus (myself included) to develop code while adhering to best practices and making full use of the capabilities of Docusaurus. In other words, Docusaurus is built to make using it easy. This is not just a benefit of using it but also a great example of how to make documentation and tools that are easy to navigate and easy to use.

``` text
Do not use an `<a>` element to navigate. Use the `<Link />` component from `@docusaurus/Link` instead. See: https://docusaurus.io/docs/docusaurus-core#linkeslint@docusaurus/no-html-links
```

## To Sum It Up

While dense, text-heavy documentation still proves to be difficult to display in a digestible way, Docusaurus helps developers focus on their content instead of the nitty-gritty of creating a site. With its growing community and source code readily available to help newcomers learn and collaborate, it is no surprise that Docusuarus is a leading site generator for documentation-focused websites. While I continue to learn the ins and outs, I continue to be impressed by the thoughtfulness and intuitive nature of Docusaurus and will continue to make use of its capabilities to the fullest.
