---
id: ingestion
title: Ingestion
definition: Processus par lequel les données sont déplacées d’une ou plusieurs sources vers une nouvelle destination où elles peuvent être stockées et analysées plus en détail.
---

L’ingestion est un processus par lequel les données sont déplacées d’une ou plusieurs sources vers une destination où elles peuvent être stockées et analysées plus en détail. L’ingestion d’ensembles de données hétérogènes provenant de diverses sources peut impliquer :Term[crosswalking]{#crosswalking} et d’autres étapes de nettoyage des données et de préparation pour l’ingestion se déroulent sans problème. Les données peuvent être ingérées en temps réel, chaque élément entrant au fur et à mesure qu’il est expulsé par la source, ou par lots périodiquement.

## Autres ressources

- Big Data Trunk (2016) [“Introduction to Data Ingestion”](https://www.youtube.com/watch?v=YD6QCEVDw20) [Vidéo]
