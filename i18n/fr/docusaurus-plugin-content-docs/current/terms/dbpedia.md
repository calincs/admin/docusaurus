---
id: dbpedia
title: DBPedia
definition: Un projet qui crée des données structurées accessibles au public pour le cloud Données ouvertes et liés.
---

DBPedia est un projet qui crée des données structurées accessibles au public pour le :Term[lonnées ouvertes liées (LOD)]{#linked-open-data} Cloud. Il met principalement à disposition Wikipédia transformé et :Term[Wikimedia Foundation]{#wikimedia-foundation} projette dans les données de :Term[triples]{#triple}, rendant ces informations disponibles dans un :Term[graphique de connaissances]{#knowledge-graph} librement accessible à tous sur le Web. DBPedia est géré depuis 2014 par l’[Association DBPedia](https://www.dbpedia.org/), qui est actuellement affiliée à [Institute for Applied Informatics (InfAI)](https://infai.org/) à l’Université de Leipzig. À ses débuts, il était affilié à l’Université libre de Berlin, où il a été lancé en 2010 sous les auspices du Web Based Systems Group.

## Exemples

- DBPedia (2022) [“Latest Core Releases”](https://www.dbpedia.org/resources/latest-core/)
- DBPedia (2022) [“Popular Individual Datasets”](https://www.dbpedia.org/resources/individual/)

## Autres ressources

- [DBPedia (Wikipedia)](https://en.wikipedia.org/wiki/DBpedia)
- DBPedia (2022) [“About DBPedia”](https://www.dbpedia.org/about/)
- Uyi Idehen (2016) [“What is DBPedia and Why is it Important?”](https://medium.com/openlink-software-blog/what-is-dbpedia-and-why-is-it-important-d306b5324f90)
