---
title: Grants Menu
sidebar_position: 2
description: Explore collaboration and grant opportunities with LINCS! Learn about linked open data services, in-kind support, data transformation, and how we can help with your grant applications.
---

## Overview

LINCS collaborates with people from across the humanities and GLAM institutions to create linked open data (LOD) from their projects and collections. Since 2020, LINCS has seen growth in the use of its infrastructure for the preservation and connection of data on the semantic web. When collaborators consider joining the LINCS community, they often ask us to calculate the cost of our labour required to support the transformation of their data to LOD. Below, you will find rough estimates of these costs, along with some details of what our work entails. 

These descriptions and numbers are only a guide for what to include in a grant application. For more information about the LOD creation process, we encourage you to check out our data transformation workflows. Please note that all in-kind contributions from LINCS must be established in consultation with us. If you are planning on submitting a grant application that describes your intention to work with us, we ask you to get in touch first. Send an email with the header “Grants Query” to [lincs@uoguelph.ca](mailto:lincs@uoguelph.ca?subject=Grants%20Query&body=Grant%20Type%3A%0ADue%20Date%3A), and be sure to tell us the type of grant and due date of your application so we can respond in time to set up a meeting. Thanks!


## Collaboration Opportunities

As an interdisciplinary team with extensive experience in creating LOD, LINCS offers a wide range of support to those wanting to make the semantic web a part of their processes. Whether you are an individual researcher looking to work with a small, bespoke dataset or a large GLAM organization thinking about data management and sustainability, we’d love to chat with you.


### Notes on calculations

LINCS staff time is valued at $95/hour. This represents contributions from members of our technical and research teams. For each of the collaboration opportunities, we have provided two time and value estimates: the low estimate is for a small dataset with a hands-on, largely independent project team; the high estimate is for a large dataset with a project team requiring intensive support. These estimates are provided only as examples. We are happy to work with your team to come up with estimates that align with your project.


## LINCS matching and in-kind contributions

Given the nature of our funding, LINCS is able to provide modest in-kind support for grant applications. We recognize that the research ecosystem that we operate within is reciprocal, and we also recognize that grants require this reciprocity for successful applications. 

Please note that all in-kind commitments from LINCS must be confirmed and approved in consultation with the LINCS executive team. To learn more or arrange a consultation, please email [lincs@uoguelph.ca](mailto:lincs@uoguelph.ca?subject=Consultation%20Inquiry).


## Responsibilities of Data Stewards

LINCS does not provide project management support for research projects. Data stewards (e.g., researchers, project leads, GLAM institution representatives) or their organizations are responsible for managing their projects including, but not limited to, hiring staff, planning meetings, ensuring the quality of content, determining publication rights, and completing annual reporting. Before beginning work with LINCS, all collaborators are asked to sign the [LINCS Project Charter](/docs/about-lincs/policies/project-charter) and be guided by it in their conduct. 


### Grant application support

The LINCS team can assist data stewards who are looking to create or transform data from a variety of formats into LOD as part of their research process or sustainability practices. Having written many grants with a LOD component and being attuned to the details of the transformation workflow, we can collaborate with you by **writing specific sections of grants** that detail LOD work, **preparing budgets** related to LOD work (stemming from the present document), and **creating letters of support** for your grant that acknowledges LINCS contributions and efforts, in forms of both technical support and human labour. 

We anticipate meeting with your team between two and four times during this stage.

Data stewards are responsible for ensuring the application process flows smoothly and for submitting all documentation to research councils and funding bodies.


|       Dataset size      | LINCS team hours |  Value |
|:-----------------------:|:----------------:|:------:|
| Small grant application |        10        |  $950  |
| Large grant application |        40        | $3,800 |



### Research project data consultation

Our team can consult on data at any stage in the data transformation process: from the suitability of mobilizing the dataset as LOD to helping **establish research questions** that can be asked of the transformed data to **selecting the appropriate workflows, tools, and resources** to use. We can also provide dataset-specific advice on timelines, technical requirements, and human resources needed to complete the transformation process. 

We anticipate meeting with your team twice during this stage.


|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |         5        |  $475  |
| Large dataset |        15        | $1,425 |


### Linked data workshops and training

LINCS has an extensive range of workshops, which we run throughout the year for the LOD community online and in person. We can set up a schedule of **training events** that are open to the community, or we can run as bespoke sessions for your project. In some instances, we are also able to offer reserved (and reduced- or waived-fee) **spaces at workshops** held in tandem with LINCS conferences, and at events such as DH@Guelph, DHSITE, DHSI@Congress, and DHSI.

Workshop topics include introductions to :Term[linked open data (LD)]{#linked-open-data}, :Term[ontologies]{#ontology}, and :Term[vocabularies]{#vocabulary}; hands-on  sessions with tools such as [LEAF-Writer](/docs/tools/leaf-writer/), [OpenRefine](/docs/tools/openrefine/), and [ResearchSpace](/docs/tools/researchspace/); and on advanced topics such as APIs and :Term[SPARQL]{#sparql-protocol-and-rdf-query-language} queries. 

The below values are for a single workshop. We recommend that projects with participants that are new to linked data attend **Making Connections: The Semantic Web for Humanities Scholars**.


#### Open workshop

| Number of attendees |  Value |
|:-------------------:|:------:|
|          1          |  $650  |
|          2          | $1,300 |
|          5          | $3,250 |


#### Custom Workshop


| LINCS team hours | Value |
|:----------------:|:-----:|
|        10        |  $950 |


### Data creation

Our team can provide support for data creation and transformation workflows—either for individual steps or throughout the process. For detailed information about the workflows and the processes involved, check out the [Create LOD](/docs/create-lod/) section of our website.


#### Data mapping support 

Most (but not all) datasets require mapping before they can be mobilized as LOD. We encourage  data stewards to take an active role in the data mapping process; this is the best way to ensure that the LOD meets the project’s needs. We can provide **support for creating your own data map**, including guidance on ontologies, advice on using [LINCS application profiles](/docs/explore-lod/understand-lincs-data/application-profiles-main/), and more. For projects that do not have the capacity or the resources to map data, we can **create a data map for you** in consultation with your project team. 

For projects mapping their own data, we anticipate meeting with you two to four times during the process. For projects where LINCS is mapping the data, we anticipate meeting with you four to six times during the process.


##### Support for mapping your own data

|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |        10        |  $950  |
| Large dataset |        20        | $1,900 |

##### Custom dataset mapping completed by LINCS team

|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |        50        | $4,750 |
| Large dataset |        100       | $9,500 |

#### Data cleaning and entity matching support

To ensure your data is interoperable and easy to use alongside other linked datasets, we recommend that your data is clean and consistent before beginning the transformation process. We can offer support throughout this process. The best method to clean your data is determined by the data’s original format (structured, semi-structured, or natural language), and we can provide **training on off-the-shelf tools** or **custom recommendations** for this process. Sometimes a **custom script created by the LINCS team** is the best way to clean data in bulk. 

As you clean your dataset, it is also a good time to consider what vocabularies and authorities you’d like to align your dataset with, and we can help you **[find and choose vocabularies and authorities](#ontologies-and-vocabularies)** to fit your needs. 

We anticipate meeting with you three to four times during this process.


|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |        20        | $1,900 |
| Large dataset |        40        | $3,800 |


#### Data transformation support

The original format of your data dictates the transformation process LINCS will provide guidance for this stage. We can recommend—and assist you in your use of—digital tools that are created or supported by LINCS to **transform your data** into [Resource Description Framework (RDF)](/docs/terms/resource-description-framework) data.

We anticipate meeting with you once or twice during this process.


|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |        20        | $1,900 |
| Large dataset |        60        | $5,700 |


#### Data ingestion and publication support

Once your data is in RDF (or if you come to LINCS with RDF data aligned with LINCS data models and ready to ingest), we can add your dataset to the LINCS :Term[triplestore]{#triplestore}. At the end of this process, you’ll be able to work with your data in [ResearchSpace](/docs/tools/researchspace/), our main user interface as well as accessing it through SPARQL queries, the [Context Explorer](/docs/tools/context-explorer/), and our APIs. To ensure this goes smoothly, we offer support in **preparing a custom interface** for your project in ResearchSpace, including project-specific browse and search filters, and training for **reviewing, vetting, and editing your data**. 

We anticipate meeting with you twice during this process, or up to four times if your dataset requires a workflow for future updates.


|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |        20        | $1,900 |
| Large dataset |        40        | $3,800 |


### Ontologies and vocabularies


#### Ontology and vocabulary consulting

LINCS uses CIDOC CRM and extensions, plus and extensions, plus the :Term[Web Annotation Data Model]{#web-annotation-data-model}, to structure our LOD. We also use a selection of controlled vocabularies to ensure that the data is consistent and connects well with other datasets. For you to get the most out of your transformed data, we encourage you to learn about ontologies and vocabularies. We offer **workshops on ontologies and vocabularies**, and we can also support you through the **[data mapping](#data-mapping-support) and [cleaning and entity matching processes](#data-cleaning-and-entity-matching-support)**. We can also work with you to develop **dataset competency questions**, which can be used to guide ontology and vocabulary work.

For creating dataset competency questions, we anticipate meeting with you twice during this process.


##### Creating and reviewing competency questions

|  Dataset size | LINCS team hours | Value |
|:-------------:|:----------------:|:-----:|
| Small dataset |         5        |  $475 |
| Large dataset |        10        |  $950 |


#### Vocabulary publication

Wherever possible, we encourage data stewards to use existing vocabularies, as they are useful for connecting LOD within and across disciplines and sectors. There are many excellent vocabularies available, but there are also rare instances where a new vocabulary is needed. For projects looking to develop their own vocabulary, we can provide **support with both creating and publishing vocabularies**. We are also able to archive vocabularies, and **create and maintain Digital Object Identifiers** (DOI) for vocabularies. Note that this is a complex, time-consuming process that should, perhaps, be considered a separate project. 

The size of the vocabulary is not related to the size of the dataset, but rather the number of terms and the complexity of the relationship between them. We anticipate meeting with you anywhere between two and ten times, depending on the size of the vocabulary and the support needed.


|                    Dataset size                   | LINCS team hours |  Value |
|:-------------------------------------------------:|:----------------:|:------:|
|         Small vocabulary,  minimal support        |        20        | $1,900 |
|       Large vocabulary,  substantial support      |        60        | $5,700 |
| Vocabulary publication and provision of DOIs only |         5        |  $475  |



### Linked data publishing and access

Once your data has been created, it can be hosted on the LINCS triplestore. As part of this process, we can **:Term[mint URIs]{#uniform-resource-identifier-minting}** for any entities that require them, **provide a DOI for your dataset**, and store the linked data in Canada’s **long-term data preservation system** (Borealis Dataverse). At this point, you can work with us to access and manage your data through LINCS platforms (such as ResearchSpace). 


|  Dataset size | LINCS team hours |  Value |
|:-------------:|:----------------:|:------:|
| Small dataset |        20        | $1,900 |
| Large dataset |        40        | $3,800 |


### Linked data project outputs

Upon publication, the linked dataset is yours to explore! That said, the LINCS team would love to further collaborate with you if our continued involvement would be helpful. These collaborations can take a variety of forms and often generate research outputs, such as **data stories, data papers** (peer-reviewed dataset descriptions), and **interactive visualizations and essays**. Knowing what your expectations are from the outset of your grant will help us plan our time accordingly. 


|                Dataset size               | LINCS team hours |  Value |
|:-----------------------------------------:|:----------------:|:------:|
|     Small dataset,  simple data story     |        40        | $3,800 |
| Large dataset, complex data visualization |        80        | $7,600 |



### Thank you!

LINCS operates only through the collaboration and cooperation of our academic and GLAM community. We know that grant writing is hard work and we are grateful that you have considered us as part of your process.  


### Document Details

**Version**: 1.0

**Authors**: Sarah Roger, Kim Martin

**Last Updated**: 2024-10-15

**Released**: 2024-10-15
