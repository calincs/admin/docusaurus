---
id: knowledge-map
title: Carte des connaissances (ResearchSpace)
definition: Un outil de visualisation dans l’environnement ResearchSpace qui affiche les différentes entités de données dans le triplestore et comment elles sont connectées à d’autres entités de données.
---

[ResearchSpace](/docs/tools/researchspace) utilise le terme carte de connaissances pour décrire comment leur :Term[le graphe de connaissances]{#knowledge-graph} contient et visualise les :Term[Données liées (LD)]{#linked-data}. La carte des connaissances de ResearchSpace est un outil de visualisation pour afficher les différentes données :Term[entités]{#entity} dans :Term[triplestore]{#triplestore} et comment ceux-ci sont connectés à d’autres entités de données à l’aide de relations spécifiques.

## Autres ressources

- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://pdfs.semanticscholar.org/9bc8/63036314d24f2d9851b3dc6ae727a55b8b9e.pdf)
