---
title: "Authority Service OpenRefine Documentation"
Last Translated: 2024-06-16
---



<div className="primary-button-row">
  <PrimaryButton
    link="https://openrefine.org/"
    buttonName="To OpenRefine"
    />
  <PrimaryButton
    link="https://docs.openrefine.org/"
    buttonName="To OpenRefine Documentation"
    />
   <PrimaryButton
    link="https://gitlab.com/calincs/conversion/authority-service"
    buttonName="To Authority Service GitLab"
    />
</div>

## Prerequisites

### Choose a Service

There are two versions of the Authority Service:

```text

https://authority.lincsproject.ca/reconcile

```

Returns only candidate :Term[entities]{#entity} from the LINCS :Term[Knowledge Graph (KG)]{#knowledge-graph} that use the LINCS :Term[namespace]{#namespace} (i.e., entities whose :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} start with http://id.lincsproject.ca/). These are URIs LINCS has :Term[minted]{#uniform-resource-identifier-minting}, typically because we were not able to find them in other common :Term[Linked Open Data (LOD)]{#linked-open-data} sources.

```text

https://authority.lincsproject.ca/reconcile/any

```

Returns all entities in the LINCS KG.

:::info

If you plan to publish your data with LINCS, you will likely want to use the service `https://authority.lincsproject.ca/reconcile/any`, which will not filter based on entity namespace.

:::

## Add the Service

:::info

This page explains how to use the Authority Service in OpenRefine. Refer to [OpenRefine’s documentation](https://openrefine.org/docs/manual/reconciling) on how to match entities more generally.

If you need tips for preparing your data for OpenRefine, check out our [clean](/docs/create-lod/clean-data) and [reconcile](/docs/create-lod/match-entities) transformation workflow steps.

:::

To add the service to OpenRefine:

1. Follow [OpenRefine’s documentation](https://openrefine.org/docs) to create a project and to clean your data.
2. [Start matching entities](https://openrefine.org/docs/manual/reconciling#getting-started) a column from your data.
3. Choose **Add Standard Service...** when prompted to choose a service.
4. Paste in the :Term[URL]{#uniform-resource-locator} for the Authority Service of your choosing.

For only LINCS namespace entities:

> `https://authority.lincsproject.ca/reconcile`

For all entities in the LINCS KG:

> `https://authority.lincsproject.ca/reconcile/any`

## Filter by Type

When you select a column and start :Term[matching entities]{#entity-matching}, [OpenRefine will suggest a few entity types or classes](https://openrefine.org/docs/manual/reconciling#reconciling-by-type) by which to filter your results. If you choose to filter the results by a type, you will only get back candidate matches that belong to that type or a sub-class of that type in the LINCS KG.

OpenRefine’s suggestions are based only on the first several rows in your data, so they may not be the right type. You can choose your own in the **Reconcile against type** box. Start typing the name of the type you want to use and you will get suggested types from the LINCS KG.

The image below shows the filtering of a reconciliation (entity matching) request by entity type:

![Example of matching entities against a type person, which will filter and give you results who belong to the type person. To filter, click the checkbox near the bottom third of the page labeled Reconcile against type and enter your desired type in the box to the right of it.](/img/documentation/openrefine-add-type.png)

If you are new to the :Term[ontologies]{#ontology} that LINCS uses, we suggest you start with the **Reconcile against no particular type** option.

## Filter by Property

You can [use another column in your data](https://openrefine.org/docs/manual/reconciling#reconciling-with-additional-columns) and specify that the values in that column are connected to your entity of interest in the LINCS KG by the :Term[property]{#property} that you choose. You can explore the LINCS data through [ResearchSpace](/docs/tools/researchspace) or the [LINCS SPARQL Endpoint](/docs/tools/sparql) to understand what properties make sense for your data. We suggest matching entities in batches and testing out different property filters as well as using no filters.

The image below shows the filtering of a reconciliation request using other columns:

![To match entities by property, nagivate to the right side of the window and under Column Include? As property, next to identifer add your desired values. As you type, suggestions may come up.](/img/documentation/openrefine-add-property.png)

## Filter by Named Graph

Each project in the LINCS KG is stored in its own :Term[named graph]{#named-graph}. You can filter your entity matching request so that it will only return entities from a single named graph. This feature is useful if you know that your data is from a specific domain or time period that matches a particular dataset in the LINCS KG.

First, setup your data so that you have a column that contains the graph name you want to search. You can have the same or different graph names in each row. The graph names can be of the form `http://graph.lincsproject.ca/name` or `name`. Replace `name` with the actual graph name. See [our list of projects](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/Datasets/readme.md) to find the correct name.

When choosing the settings to match a column of entities, click on the check-box that corresponds to your named graph column on the right-hand side of the page. In the **As Property** box next to it, start typing _graph_. An option for _named graph_ that corresponds to `http://graph.lincsproject.ca/` will pop up. Choose that option.

The image below shows the filtering of an entity matching request by a particular project’s named graph:

![](/img/documentation/openrefine-named-graph.png)

## Preview Entities

Once you have candidate entity matches, hover your mouse over the candidates to see a preview of the information LINCS has about that entity.

:::info

Need more details to confirm the match? Click on the entity’s label to see the full entity record in [ResearchSpace](/docs/tools/researchspace).

:::

## Understand the Match Score

Each candidate entity will be displayed with a score between 0 and 100. This score represents how similar the text in your data is to the entity’s label in the LINCS KG.

There is no threshold for a match score that will work for all of your data. Entity matching is a semi-automated process. Candidates and match scores can speed up the process but human knowledge is needed to determine how important accuracy is and to confirm matches.

For best results, we recommend that you clean your data before matching entities. Where possible, we also recommend you try matching entities on different versions of names that you have in your data.

{/*## Enhancing Data  */}
{/*Section for the future. Don’t have the functionality added yet. */}
{/*https://openrefine.org/docs/manual/reconciling#fetching-more-data */}

:::info
To use your newly enhanced data to create :Term[Linked Open Data (LOD)]{#linked-open-data}, check out our [Transformation Workflow for Structured Data](/docs/create-lod/workflow-paths?workflow=structured).
:::

## Ask for Support

If you find problems or have suggestions for the Authority Service, please report issues on [our GitLab](https://gitlab.com/calincs/conversion/authority-service/-/issues). If your problems have to do with OpenRefine more generally, please connect with the [OpenRefine team](https://openrefine.org/community).
