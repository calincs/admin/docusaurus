---
id: property
title: Property
definition: A specified relationship between two classes or entities, such as the predicate in a triple (subject-predicate-object).
---

A property defines a specified relationship between two classes or :Term[entities]{#entity}. In the `<subject><predicate><object>` statements (:Term[triples]{#triple}) that comprise the :Term[Semantic Web]{#semantic-web}, the property is the predicate or “verb” in the sentence and is defined with reference to both the subject (:Term[domain]{#domain}) and object (:Term[range]{#range}) of the :Term[triple]{#triple}.

## Examples

- The following example shows the property, or predicate, connecting two entities or classes in a triple statement.

![Margaret Laurence and Canada are connected by the property was born in to create the triple statement "Margaret Laurence was born in Canada".](</img/documentation/glossary-property-example-(c-LINCS).jpg>)
