---
id: object-oriented-ontology
title: Ontologie orientée objet
definition: Une ontologie qui utilise des objets pour relier des choses, des concepts, des personnes, du temps et des lieux.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Une :Term[ontologie]{#ontology} orientée objet utilise des objets pour relier les choses, imitant la pensée humaine dans ses liens, par exemple, entre un objet et son fabricant. Elle place les objets au centre de sa structure et relie les éléments d'information directement en tant qu'attributs de ces objets, contrairement aux :Term[ontologies orientées événements]{#event-oriented-ontology}.

## Exemples

- L'exemple suivant montre un livre modélisé comme un objet central, dont l'auteur et la date de publication sont des attributs connectés.

![](</img/documentation/glossary-object-oriented-ontology-(c-LINCS).jpg>)

## Autres ressources

- Bogost (2023) [“What is Object-Oriented Ontology?”](http://bogost.com/writing/blog/what_is_objectoriented_ontolog/)
- [Object-Oriented Ontology (Wikipedia)](https://en.wikipedia.org/wiki/Object-oriented_ontology)
