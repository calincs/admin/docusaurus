---
id: node
title: Nœud
definition: Représentation d’une entité ou d’une instance à suivre dans une base de données de graphes ou un triplestore, telle qu’une personne, un objet ou une organisation.
---

Un nœud représente une entité ou une instance à suivre dans une :Term[base de données de graphes]{#graph-database} ou :Term[triplestore]{#triplestore}, comme une personne, un objet ou une organisation. Les nœuds sont connectés par :Term[edges]{#edge} et les informations sur un nœud sont appelées une propriété.

## Exemples

- L’image suivante montre un :Term[Resource Description Framework (RDF)]{#resource-description-framework} graphique mettant en évidence les arêtes qui relient trois nœuds.

![alt=""](</img/documentation/glossary-node-example-(c-LINCS).jpg>)
