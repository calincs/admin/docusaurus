---
id: graph-database
title: Graph Database
definition: A database that structures information as a graph or network, where a set of resources, or nodes, are connected together by edges that describe the relationships between each resource.
---

A graph database structures information as a graph or network, where a set of resources, or :Term[nodes]{#node}, are connected together by :Term[edges]{#edge} that describe the relationships between each resource. Graph databases are a type of :Term[NoSQL database]{#nosql-database} and represent :Term[Linked Data (LD)]{#linked-data} as nodes, edges, and :Term[properties]{#property}.

| Graph Database                                           | Triplestore                             |
| -------------------------------------------------------- | --------------------------------------- |
| Accommodates a variety of query languages such as Cypher | Uses SPARQL as the query language       |
| Stores various types of graphs                           | Stores rows of triples                  |
| Node/property-centric                                    | Edge-centric                            |
| Does not provide inferences on data                      | Provides inferences on data             |
| Less academic                                            | More synonymous with the “semantic web” |

## Examples

- [ArangoDB](https://www.arangodb.com/)
- [Amazon (Web Services) Neptune](https://aws.amazon.com/neptune/)
- [JanusGraph](https://janusgraph.org/)
- [Neo4j](https://neo4j.com/)
- [OrientDB](https://orientdb.com/)

## Further Resources

- [Graph Database (Wikipedia)](https://en.wikipedia.org/wiki/Graph_database)
- Neo4j (2022) [“Introduction to Graph Databases Video Series”](https://neo4j.com/developer/intro-videos/)
- Robinson, Webber, Eifrem (2013) _[Graph Databases](https://hura.hr/wp-content/uploads/2016/10/Graph_Databases_2e_Neo4j-5.pdf)_
