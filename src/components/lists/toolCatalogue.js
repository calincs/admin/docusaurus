/** @format */

import React from "react";
import Tools from "../tools";
import ToolCard from "../cards/ToolCard";
import buttonStyles from "../buttons/buttons.module.css";
import styles from "./toolCatalogue.module.css";
import { useState, useEffect } from "react";
import Translate, { translate } from "@docusaurus/Translate";
import functionTypes from "../functions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
);

const CreatorText = ({ creator, active }) => {

    const message = {
      Partner: (
        <Translate id="tool.origin.partner" description="Tool Origin Partner">
          LINCS Partner
        </Translate>
      ),
      Other: (
        <Translate id="tool.origin.Other" description="Tool Origin Other">
          Enhanced by LINCS
        </Translate>
      ),
      LINCS: (
        <Translate id="tool.origin.LINCS" description="Tool Origin LINCS">
          Made by LINCS
        </Translate>
      ),
      External: (
        <Translate id="tool.origin.External" description="Tool Origin External">
          Adopted by LINCS
        </Translate>
      ),
    };

  const imgSrc = active
    ? "/img/lincs-logo-(c-LINCS).png"
    : "/img/lincs-logo-inverted-(c-LINCS).png";

  return (
    <div className={styles.functionButtonName}>
      <div>
        <img src={imgSrc} alt="" />
      </div>
      {message[creator]}
    </div>
  );
};

const FunctionFilters = ({ activeFilters, handleClick }) => {
  return (
    <>
      <p className={styles["filter-label"]}>
        <Translate
          id="toolCatalogue.filterByToolHeader"
          description="Tool Catalogue Filter by Tool Header">
          Filter by Tool Function
        </Translate>
      </p>
      <div className="function-button-row">
        {/* generates function filter buttons */}
        {Object.keys(functionTypes).map((filter) => (
          <button
            key={filter}
            className={`${
              activeFilters.includes(filter)
                ? [buttonStyles.functionButtonFilter, styles.filterButton].join(
                    " ",
                  )
                : buttonStyles.functionButtonFilter
            } `}
            onClick={() => handleClick(filter)}>
            <div>
              <FontAwesomeIcon icon={functionTypes[filter].icon} />
            </div>
            <div className={styles.functionButtonName}>
              {functionTypes[filter].id}
            </div>
          </button>
        ))}
      </div>
    </>
  );
};

const LevelFilters = ({ activeFilters, handleClick }) => {
  const filterLevels = [
    translate({
      id: "tool.expertise.beginner",
      message: "Beginner",
    }),
    translate({
      id: "tool.expertise.intermediate",
      message: "Intermediate",
    }),
    translate({
      id: "tool.expertise.expert",
      message: "Expert",
    }),
  ];
  return (
    <>
      <p className={styles["filter-label"]}>
        <Translate
          id="toolCatalogue.filterByUserLevelHeader"
          description="Tool Catalogue Filter by User Level Header">
          Filter by User Level
        </Translate>
      </p>
      <div className="function-button-row">
        {/* generates user level filter buttons */}
        {filterLevels.map((filter) => (
          <button
            key={filter}
            className={`${
              activeFilters.includes(filter)
                ? [styles.filterButton, buttonStyles.functionButtonFilter].join(
                    " ",
                  )
                : buttonStyles.functionButtonFilter
            } `}
            onClick={() => handleClick(filter)}>
            <div className={styles.functionButtonName}>{filter}</div>
          </button>
        ))}
      </div>
    </>
  );
};

const OriginFilters = ({ activeFilters, handleClick }) => {
  const filterCreatorTypes = ["External", "Other", "Partner", "LINCS"];


  const isAnyCreatorFilterActive = filterCreatorTypes.some((filter) =>
    activeFilters.includes(filter),
  );
  return (
    <>
      <p className={styles["filter-label"]}>
        <Translate
          id="toolCatalogue.filterByOriginHeader"
          description="Tool Catalogue Filter by Origin Header">
          Filter by Origin
        </Translate>
      </p>
      <div className="function-button-row">
        {/* generates user level filter buttons */}
        {filterCreatorTypes.map((filter) => (
          <button
            key={filter}
            disabled={
              isAnyCreatorFilterActive && !activeFilters.includes(filter)
            } // Disable if any creator filter is active and it's not the current filter
            aria-disabled={
              isAnyCreatorFilterActive && !activeFilters.includes(filter)
            } // Disable if any creator filter is active and it's not the current filter
            className={`${
              activeFilters.includes(filter)
                ? [styles.filterButton, buttonStyles.functionButtonFilter].join(
                    " ",
                  )
                : buttonStyles.functionButtonFilter
            } `}
            onClick={() => handleClick(filter)}>
            <CreatorText
              creator={filter}
              active={activeFilters.includes(filter)}
            />
          </button>
        ))}
      </div>
    </>
  );
};

function ToolFilters({ activeFilters, setActiveFilters }) {
  const handleClick = (filter) => {
    let _activeFilters = [...activeFilters];
    // Resetting filter
    if (activeFilters.includes(filter)) {
      _activeFilters = _activeFilters.filter((item) => item !== filter);
    } else {
      _activeFilters = _activeFilters.filter((item) => item !== "All");
      _activeFilters.push(filter);
    }

    //Resets and sets filters
    if (_activeFilters.length === 0) {
      setActiveFilters(["All"]);
    } else {
      setActiveFilters(_activeFilters);
    }
  };

  return (
    <div className={styles.filters}>
      <FunctionFilters
        activeFilters={activeFilters}
        handleClick={handleClick}
      />
      <LevelFilters activeFilters={activeFilters} handleClick={handleClick} />
      <OriginFilters activeFilters={activeFilters} handleClick={handleClick} />
    </div>
  );
}

const ToolList = ({ tools, resetFilter, resetQuery, sortOrder }) => {
  if (tools.length === 0) {
    return (
      <div className={styles["empty-list"]}>
        <p>
          <Translate
            id="toolCatalogue.noToolsFoundLabel"
            description="Tool Catalogue No Tools Found label">
            No Tools Found!
          </Translate>
        </p>
        <button
          className={buttonStyles.primaryButton}
          onClick={() => {
            resetFilter(["All"]);
            resetQuery("");
          }}>
          <Translate
            id="toolCatalogue.resetFiltersLabel"
            description="Tool Catalogue Reset Filters label">
            Reset Filters
          </Translate>
        </button>
      </div>
    );
  }

  if (sortOrder === "featured") {
    tools = tools.sort((a, b) => a["Featured Order"] - b["Featured Order"]);
  } else if (sortOrder === "alphabetical") {
    tools = tools.sort((a, b) => a["Tool Name"].localeCompare(b["Tool Name"]));
  }

  return (
    <div className={styles["tool-list"]}>
      {tools.map((x) => (
        <ToolCard
          key={x["Tool Name"]}
          {...{
            tool: x["Tool Name"],
            image: x["Image Path"],
            description: x["Description"],
            path: x["Path"],
            functions: x["Functions"],
            lvl: x["Level"],
            creator: x["Creator"],
          }}></ToolCard>
      ))}
    </div>
  );
};

function ToolCatalogue({ preFilter = "All", sortOrder = "alphabetical" }) {
  const [items, setItems] = useState([]);
  const [query, setQuery] = useState("");
  const [searchParam] = useState([
    "Tool Name",
    "Description",
    "Functions",
    "Keywords",
    "Level",
    "Creator",
  ]);
  const [filters, setFilters] = useState(
    Array.isArray(preFilter) ? preFilter : [preFilter],
  );

  useEffect(() => {
    setItems(search(Tools));
  }, []);

  const search = (items) => {
    return items.filter((item) => {
      // Checking both lvl and fx filters for a match

      if (
        filters.every(
          (filt) =>
            item.Functions.includes(filt) ||
            item.Level.includes(filt) ||
            item.Creator.includes(filt) ||
            (filt == "featured" && item["Featured Order"] > 0),
        )
      ) {
        // Looking for exact string match, may want to change this to a more forgivable regex
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      } else if (filters == "All") {
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      }
    });
  };

  // Allows list to be prefiltered to be used on other pages
  // REVIEW: is it desirable to have input field and filters visible
  return preFilter != "All" ? (
    <ToolList tools={search(items)} sortOrder={sortOrder} />
  ) : (
    <>
      <ToolFilters
        setActiveFilters={setFilters}
        activeFilters={filters}></ToolFilters>

      <input
        type="search"
        className={styles["tool-search"]}
        placeholder={translate({
          id: "toolCatalogue.searchBarPlaceholder",
          description: "Tool Catalogue Search Bar Placeholder",
          message: "Search by title or keyword",
        })}
        aria-label={translate({
          id: "toolCatalogue.searchBarPlaceholder",
          description: "Tool Catalogue Search Bar Placeholder",
          message: "Search by title or keyword",
        })}
        value={query}
        onChange={(e) => setQuery(e.target.value)}></input>
      <ToolList
        tools={search(items)}
        resetFilter={setFilters}
        resetQuery={setQuery}
        sortOrder={sortOrder}
      />
    </>
  );
}

export default ToolCatalogue;
