---
id: thesaurus-of-geographic-names
title: Thesaurus of Geographic Names (TGN)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les noms, les relations, les types de lieux, les dates et les coordonnées.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Le Thesaurus of Geographic Names (TGN) est l'un des cinq vocabulaires Getty développés par le Getty Research Institute pour fournir des termes et des :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier} pour les noms, les relations, les types de lieux, les dates et les coordonnées des lieux liés à l'art, à l'architecture et à d'autres patrimoines culturels visuels.

Le TGN comprend des termes relatifs à :

- les sites archéologiques
- Empires
- les lieux habités (villes, villages)
- Colonies perdues (historiquement documentées)
- Zones générales nommées
- Nations
- Caractéristiques physiques
- Zones tribales

Le TGN exclut les lieux imaginaires ou légendaires et les caractéristiques sous-marines.

## Autres ressources

- The Collections Trust (2023) [“Thesaurus of Geographic Names (TGN) (Getty)”](https://collectionstrust.org.uk/resource/thesaurus-of-geographic-names-tgn-getty/)
- [Thesaurus of Geographic Names (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn)
