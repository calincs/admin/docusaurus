---
slug: invisible-barrier
title: "Invisible to the Sighted, Barrier to the Blind"
authors: Humna Chaudhry
tags: [UX]
hide_table_of_contents: true
image: /invisible-barrier-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-08-21
---

![](<./invisible-barrier-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

J'ai passé la majeure partie de mon été à améliorer l'accessibilité du site web du projet LINCS. Au cours de ce processus, j'ai beaucoup appris sur la façon dont les personnes handicapées naviguent sur Internet. Il y a eu beaucoup à faire et cela m'a pris beaucoup de temps, mais ce n'est pas une fatalité ! Si les concepteurs intègrent l'accessibilité dès le départ, un effort supplémentaire minime permet d'améliorer l'expérience de tous les utilisateurs, et pas seulement de ceux qui ont des barrières.

{/* truncate */}

**27 % des Canadiens ont un ou plusieurs handicaps qui les empêchent d'accomplir leurs activités quotidiennes, selon l'[Enquête canadienne sur le handicap.](https://www150.statcan.gc.ca/n1/daily-quotidien/231201/dq231201b-eng.htm).**

En outre, on estime que [1,5 million de Canadiens ont perdu la vue et que 6 millions d'autres souffrent d'une maladie qui pourrait entraîner une perte de la vue](https://www.cnib.ca/en/sight-loss-info/blindness/blindness-canada?region=on#:~:text=Today%2C%20an%20estimated%201.5%20Million,Canadian%20Survey%20on%20Disabilities%202017.). Sans l'utilisation de lecteurs d'écran, ces personnes auraient des difficultés à naviguer sur le web, voire n'auraient aucun moyen de le faire. Pensez à la façon dont vous utilisez votre téléphone ou votre ordinateur pour effectuer des opérations bancaires, contacter des personnes, rechercher des informations, prendre des rendez-vous, trouver un emploi, créer des calendriers - les utilisations sont infinies. Si le web n'était pas accessible, il serait difficile, voire impossible, pour des millions de Canadiens d'effectuer une multitude de tâches quotidiennes.

![Personne utilisant Google Maps sur son téléphone pour rechercher des coordonnées.](invisible-barrier-maps-unsplash-(cc0).png)

En effectuant un petit audit d'accessibilité de la page d'accueil du projet LINCS, j'ai découvert que le premier niveau de titre (H1) était utilisé sur toute la page. Bien que cela me paraisse correct, j'ai réalisé à quel point il est important d'avoir une descente logique des niveaux de titres lorsque l'on navigue sur une page web à l'aide d'un lecteur d'écran. J'ai également découvert l'importance du « texte alt » pour ceux qui ne peuvent pas voir les images - et j'ai par la suite passé beaucoup de temps à ajouter un texte alt aux photos du site LINCS.

Si certains problèmes d'accessibilité sont évidents, beaucoup passent inaperçus. Les obstacles sont souvent invisibles pour ceux qui ont une bonne vue, une bonne ouïe ou une bonne dextérité. Heureusement, les outils de test d'accessibilité et les logiciels conçus pour les personnes handicapées permettent aux concepteurs de faire l'expérience de leurs propres sites web du point de vue de leurs utilisateurs. Ma première action lors de l'audit du site web LINCS a été d'activer le lecteur d'écran de Windows et d'essayer de naviguer les yeux fermés ; c'était beaucoup plus difficile qu'on ne le pense ! Une grande partie de cette difficulté était due à la surutilisation des titres H1.

La vidéo suivante montre comment une application de lecture d'écran populaire, JAWS, annonce les informations d'une page web bien structurée. Remarquez que JAWS s'appuie fortement sur les niveaux de titres pour communiquer l'importance et regrouper les informations pertinentes.

<YoutubeEmbed id="auUwAuJ8P-I" />

Les améliorations en matière d'accessibilité permettent à un plus grand nombre d'utilisateurs d'accéder au contenu ou d'utiliser des outils, tout en améliorant l'expérience de tous les utilisateurs. Prenons l'exemple des fonctions d'accessibilité audio. Plus de 50 % des Américains utilisent le sous-titrage quelquefois ou tout le temps [selon CBS](https://www.cbsnews.com/news/subtitles-why-most-people-turn-tv-captions-on), et ce chiffre atteint même 80 % pour les membres de la génération Z. Comme l'explique CBS, des services de streaming populaires tels que Netflix ont initialement mis en place des sous-titres pour l'ensemble de leur contenu à la suite d'un plaidoyer de l'Association nationale des sourds en 2010. Depuis, les sous-titres sont devenus un élément essentiel du streaming, car ils sont utiles à tous : si vous êtes dans un environnement bruyant, si vous n'avez pas d'écouteurs dans une bibliothèque, si vous voulez regarder quelque chose dans une autre langue, etc. Une [étude conjointe de Verizon et Publicis Media](https://www.3playmedia.com/blog/verizon-media-and-publicis-media-find-viewers-want-captions/) a fourni les statistiques suivantes après avoir mené une enquête sur les consommateurs américains de vidéos et de publicités :

- 80 % des spectateurs sont plus enclins à terminer la vidéo si elle est sous-titrée
- 69 % regardent des vidéos dont le son est désactivé
- 50 % déclarent que les sous-titres sont importants, citant généralement des raisons telles que le fait d'être dans un endroit calme, de ne pas avoir accès à des écouteurs ou d'être multitâches.
- 37 % des spectateurs sont enclins à activer le son si les sous-titres sont disponibles.

Un autre exemple de l'accessibilité qui profite à tous est celui des capacités mains libres que les gens utilisent souvent dans la voiture ou lorsqu'ils sont occupés à d'autres tâches telles que la cuisine ou le nettoyage. Les livres audio, destinés à l'origine aux personnes souffrant de troubles de la vue, ont connu une popularité croissante ; l'enquête annuelle sur les ventes de la [Audio Publishers Association](https://www.publishersweekly.com/pw/by-topic/industry-news/audio-books/article/92444-the-audiobook-market-and-revenue-keeps-growing.html) a révélé que les chiffres de vente ont connu une croissance à deux chiffres pendant **11 ans d'affilée**.

Tout cela prouve que **la conception accessible est tout simplement une bonne conception**. C'est précisément pour cette raison que j'ai été si reconnaissant que ma première expérience dans le monde du développement web ait été axée sur l'accessibilité, car il s'agit d'une base essentielle qui m'a sans aucun doute permis de devenir un développeur web inclusif et plus efficace.

## Que pouvez-vous faire pour aider ?

Même si vous n'êtes pas développeur web ou concepteur de produits, vous pouvez contribuer à rendre le web plus accessible en plaidant en faveur de l'accessibilité, en faisant entendre votre voix et en demandant des choses telles que le sous-titrage ou les transcriptions. Si vous constatez que quelque chose n'est pas accessible - par exemple, un texte difficile à lire sur une affiche - donnez votre avis !

Si vous utilisez des médias sociaux tels qu'Instagram, LinkedIn, Facebook ou X, veillez à inclure un texte alt, un texte descriptif expliquant le contenu de la photo postée. Pour obtenir des conseils utiles sur le texte alternatif, consultez le [Guide de Tug sur le texte alternatif dans les médias sociaux](https://www.tugagency.com/tug-life/tug-blog/2021/09/15/social-media-accessibility-a-guide-to-alt-text-on-social-media/).

Veillez également à mettre une majuscule à chaque nouveau mot d'un hashtag ! Les lecteurs d'écran ont du mal à lire une phrase trop courte. Par exemple, #ILoveAccessibleWebsites se lira exactement comme vous le souhaitez, mais #Iloveaccessiblewebsites pourrait être lu comme un seul mot bizarre et très long.

Pour en savoir plus sur l'accessibilité du web, consultez [cette introduction](https://www.w3.org/WAI/fundamentals/accessibility-intro/) de W3.org.

Plaider en faveur de l'accessibilité, fournir un retour d'information et utiliser des pratiques inclusives sur les médias sociaux sont des moyens simples mais efficaces pour que chacun puisse apporter sa contribution. Je me réjouis à l'idée de réaliser de bonnes conceptions accessibles au cours de ma carrière. Engageons-nous tous à faire du web un endroit où personne n'est laissé pour compte.
