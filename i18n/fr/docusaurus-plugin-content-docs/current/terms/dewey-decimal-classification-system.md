---
id: dewey-decimal-classification-system
title: Dewey Decimal Classification System (DDC)
definition: Un système de classification des bibliothèques couramment utilisé par les bibliothèques publiques et les petites bibliothèques universitaires pour organiser les collections imprimées.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Le système de classification décimale de Dewey (CDD) est un système de classification des bibliothèques développé par Melvil Dewey en 1876. Il est actuellement utilisé par les bibliothèques publiques et les petites bibliothèques universitaires pour organiser leurs collections d'imprimés.

## Autres Ressources

- [Classification décimale de Dewey (Wikipedia)](https://en.wikipedia.org/wiki/Dewey_Decimal_Classification)
- OCLC (2003) ["Summaries : DDC Dewey Decimal Classification"](https://www.oclc.org/content/dam/oclc/dewey/resources/summaries/deweysummaries.pdf)
