---
id: entity
title: Entité
definition: Une chose discrète, souvent décrite comme le sujet et l’objet (ou le domaine et la gamme) d’un triplet (sujet-prédicat-objet).
---

Les entités sont des choses discrètes qui existent, qui peuvent être liées entre elles à l’aide d’attributs qui établissent des relations entre elles. Dans :Term[Données ouvertes liées (LOD)]{#linked-open-data}, les entités sont souvent décrites comme le sujet et l’objet (ou le :Term[domaine]{#domain} et :Term[plage]{#range}) d’un :Term[triple]{#triple} (sujet-prédicat-objet).

## Exemples

- CIDOC CRM (2022) [“E1 CRM Entity in version 7.1.1”](https://cidoc-crm.org/Entity/E1-CRM-Entity/version-7.1.1)

## Autres ressources

- [Entity (Wikipedia)](https://en.wikipedia.org/wiki/Entity)
