import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import MobileEntityButton from "../buttons/MobileEntityButton";

function MobileCarouselCard({ dataset }) {
  const { longTitle, shortDescription, page, images, buttons, published } =
    dataset;
  const image = images["entity"] || images["logo"];
  const entityLink = "entity" in images ? image.entityLink : null;
  return (
    <div className={styles.background}>
      <div className={styles.mobileCarouselContent}>
        <h3>{longTitle}</h3>
      </div>

      <div className={styles.entityContainer}>
        {image ? (
          <img
            className={styles.mobileCarouselImage}
            src={image.file}
            alt={image.altText}
          />
        ) : (
          <div className={styles.mobileRectangle}></div>
        )}

        <div className={styles.mobileEntity}>
          {entityLink ? <MobileEntityButton link={entityLink} /> : null}
        </div>
      </div>

      <div className={styles.mobileCarouselDescription}>
        <p>{shortDescription}</p>
      </div>

      <div className="mobile-carousel-primary-button-row">
        {published ? (
          <PrimaryButton
            link={buttons["RS Search"].link}
            buttonName={buttons["RS Search"].label}
          />
        ) : null}
        <PrimaryButton
          link={buttons["Project"].link}
          buttonName={buttons["Project"].label}
        />
      </div>
    </div>
  );
}

export default MobileCarouselCard;
