---
sidebar_position: 8
title: "Sources & Metadata Application Profile"
description: "Represent dataset or data object metadata"
---

## Purpose

To document how various facets of LINCS data are modelled, along with reference :Term[authorities]{#authority-file} for the populating :Term[vocabularies]{#vocabulary}. This will provide a basis for instruction for how to model data in a LINCS-compatible manner, as well as aid in navigation and discovery.

“Sources and Metadata” describes patterns that are unique or specific to representing information about dataset or data object :Term[metadata]{#metadata}, as well as the use of the :Term[Web Annotation Data Model (WADM)]{#web-annotation-data-model} to connect to sources.

This document introduces the concepts as used by LINCS, and are not complete definitions of the :Term[CIDOC CRM]{#cidoc-crm} ontology class or :Term[property]{#property} concepts. Consult [CIDOC CRM v. 7.1.1 documentation](https://cidoc-crm.org/Version/version-7.1.1) for full class descriptions and property descriptions.

## Acronyms

**Ontology Acronyms:**

* CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)
* CRMdig - [Model for digital objects](https://cidoc-crm.org/crmdig/)
* OA - [Web Annotation Data Model](https://www.w3.org/TR/annotation-model/)

**Vocabulary and Authority Acronyms:**

* LINCS - LINCS minted entities

## Main Classes

<table>
  <tr>
    <td>
      <strong>Entity Type</strong>
    </td>
    <td>
      <strong>Class</strong>
    </td>
    <td>
      <strong>Declaration Snippet (TTL)</strong>
    </td>
  </tr>
  <tr>
    <td>Attribute assignment</td>
    <td>crm:E13_Attribute_Assignment</td>
    <td>
      ```turtle
      <attribute_assignment> a crm:E13_Attribute_Assignment ;
        rdfs:label "<attribute_assignment>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Annotation</td>
    <td>crm:E33_Linguistic_Object, oa:Annotation</td>
    <td>
      ```turtle
      <annotation> a crm:E33_Linguistic_Object, oa:Annotation ;
        rdfs:label "<annotation>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Annotation source</td>
    <td>E73_Information_Object, oa:SpecificResource</td>
    <td>
      ```turtle
      <annotation_source> a E73_Information_Object, oa:SpecificResource ;
        rdfs:label "<annotation_source>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Annotation source quote</td>
    <td>E33_Linguistic_Object, oa:TextQuoteSelector</td>
    <td>
      ```turtle
      <annotation_source_quote> a E33_Linguistic_Object, oa:TextQuoteSelector ;
        rdfs:label "<annotation_source_quote>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Metadata method</td>
    <td>crm:E29_Design_or_Procedure</td>
    <td>
      ```turtle
      <metadata_method> a crm:E29_Design_or_Procedure ;
        rdfs:label "<metadata_method>" .
      ```
    </td>
  </tr>
  <tr>
    <td>Type</td>
    <td>crm:E55_Type</td>
    <td>
      ```turtle
      <type> a crm:E55_Type ;
        rdfs:label "<type>" .
      ```
    </td>
  </tr>
</table>


## Overview Diagram

Below is an image of the application profile overview diagram. Follow this link for a [zoomable, more readable version](https://drive.google.com/file/d/1aBUgJyoPBomv00EHDduQYRiUBRAKcctB/view?usp=share_link). The segments below align with the document sections.

![Application profile overview diagram.](/img/documentation/application-profile-sources-overview-(c-LINCS).jpg)

## Nodes

### Annotations

LINCS uses WADM to support the representation of sources of assertions. This involves aligning WADM with CIDOC CRM; this has been done through an object-centric alignment which declares that an Annotation is the text object that is produced by the act of annotating, and thus can also be classed as a crm:E33_Linguistic_Object. For more on this, see the [LINCS Web Annotation Model Use](https://docs.google.com/document/d/19-B9RBDCNsVLsv7A7D6mO7MoB2UwdRwznRAh6lVRuZk/edit?usp=sharing) document.{/*Do we actually want this link to GoogleDocs? */}

This pattern area can be broken down into three parts:

1. The connection between the entity and the assertion
2. Describing the source of the assertion
3. Identifying the specific resource and text quote used for the assertion

![Application profile annotation](/img/documentation/application-profile-sources-annotation-(c-LINCS).png)

### Connection between the Entity and the Assertion

![Application profile connection](/img/documentation/application-profile-sources-connection-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an attribute that is made about an entity is
      the subject of an annotation.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E1_CRM_Entity → crm:P140i_was_attributed_by →
          E13_Attribute_Assignment
          <br />→ crm:P129i_is_subject_of → **E33_Linguistic_Object, oa:Annotation**
          <br />→ crm:P67_refers_to → crm:E1_CRM_Entity
          <br />→ oa:hasBody → crm:E1_CRM_Entity
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that Rosamund Marriort Watson was identified
        in an annotation of a text which attributed her with having a gender of
        woman.
      </p>
      <p>
        <code>
          orlando:9e381429-5019-48dd-88fd-79e8b3f4825f →
          crm:P140i_was_attributed_by → crm:E13_Attribute_Assignment
          <br />→ crm:P129i_is_subject_of → <strong>lincs:cCCGa6IkVuU</strong>
          <br />→ crm:P67_refers_to →
          orlando:9e381429-5019-48dd-88fd-79e8b3f4825f
          <br />→ oa:hasBody → orlando:9e381429-5019-48dd-88fd-79e8b3f4825f
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ;
    crm:P140i_was_attributed_by <attribute_assignment> .

<attribute_assignment> a crm:E13_Attribute_Assignment ; 
    rdfs:label "<attribute_assignment>" ;
    crm:P129i_is_subject_of <annotation> .

<annotation> a crm:E33_Linguistic_Object, oa:Annotation ; 
    rdfs:label "<annotation>" ;
    crm:P67_refers_to <entity> ; 
    oa:hasBody <entity> .
```

### Describing the Source of the Assertion

![Application profile assertion](/img/documentation/application-profile-sources-assertion-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an annotation is made from a source.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E33_Linguistic_Object, oa:Annotation → oa:hasTarget →{" "}
          <strong>crm:E73_Information_Object, oa:Specific Resource</strong>
          <br />→ oa:hasSource → <strong>crmdig:D1_Digital_Object</strong>
          <br />→ oa:hasSelector →{" "}
          <strong>crm:E73_Information_Object, oa:XPathSelector</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that the annotation connected to Rosamund
        Marriort Watson being attributed with having a gender of woman was made
        from a source.
      </p>
      <p>
        <code>
          lincs:cCCGa6IkVuU → oa:hasTarget → <strong>lincs:7wNnMYeWw3l</strong>
          <br />→ oa:hasSource → **&lt;https://orlando.cambridge.org/profiles/alcolo#alcolo-chapter-birthandbackground>**
          <br />→ oa:hasSelector → <strong>lincs:ufFzkYoOJNM</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<annotation> a crm:E33_Linguistic_Object, oa:Annotation ; 
    rdfs:label "<annotation>" ;
    oa:hasTarget <source> .

<source> a crm:E73_Information_Object, oa:SpecificResource ;
    rdfs:label "<source>" ;
    oa:hasTarget <digital> ;
    oa:hasSelector <source_detail> .

<digital> a crmdig:D1_Digital_Object ; 
    rdfs:label "<digital>" .

<source_detail> a crm:E73_Information_Object, oa:XPathSelector ;
    rdfs:label "<source_detail>" .
```

### Identifying the Specific Resource and Text Quote used for the Assertion

![Application profile specific resource](/img/documentation/application-profile-sources-specificresource-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that an annotation source includes a specific quote
      of text.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E73_Information_Object, oa:XPathSelector → oa:refinedBy →{" "}
          <strong>crm:E33_Linguistic_Object, oa:TextQuoteSelector</strong>
          <br />→ oa:exact → <strong>rdfs:literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that the source of the annotation connected
        to Rosamund Marriort Watson being attributed with having a gender of
        woman includes a specific quote of text.
      </p>
      <p>
        <code>
          lincs:ufFzkYoOJNM → oa:refinedBy → <strong>lincs:mgAMtF4hV10</strong>
          <br />→ oa:exact → 
            **“LMA was raised in a household of middle-class, presumably white,
            AmericanTranscendentalist thinkers whose philosophies often
            precluded financial solvency for the Alcott family. Her childhood
            poverty fuelled her adult writing and earning. The Trancendentalists
            stand accused by a recent commentator of smug sexism and narrow
            mindededness.”**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<source_detail> a crm:E73_Information_Object, oa:XPathSelector ;
    rdfs:label "<source_detail>" ;
    oa:refinedBy <source_quote> .

<source_quote> a crm:E33_Linguistic_Object, oa:TextQuoteSelector ; 
    rdfs:label "<source_quote>" ;
    oa:exact "<quote_text>" .
```

## Metadata

![Application profile metadata](/img/documentation/application-profile-sources-metadata-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a data object has metadata.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E1_CRM_Entity → crm:P140i_was_attributed_by →
          crm:E13_Attribute_Assignment
          <br />→ crm:P33_used_specific_technique → **crm:E29_Design_or_Procedure**
          <br />→ rdfs:comment → rdfs:literal
          <br />→ crm:P2_has_type → crm:E55_Type
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Orlando dataset states that the administrative metadata of the
        creation of the MODS record for Hopes and Fears was made using the
        generation process of the MODS Record of Orlando bibliographic records.
      </p>
      <p>
        <code>
          &lt;lincs:MXtpaxIkxG7> → crm:P140i_was_attributed_by →
          crm:E13_Attribute_Assignment
          <br />→ crm:P33_used_specific_technique → **&lt;lincs:z7kKbF0M7cw>**
          <br />→ rdfs:comment → “Record has been transformed into MODS from an
          XML Orlando record using an XSLT stylesheet. Metadata originally
          created in Orlando Document Archive’s bibliographic database formerly
          available at nifflheim.arts.ualberta.ca/wwp.”
          <br />→ crm:P2_has_type → &lt;GenerationProcess>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label <entity> ; 
    crm:P140i_was_attributed_by <admin_metadata_ref> .

<admin_metadata_ref> a crm:E13_Attribute_Assignment ; 
    rdfs:label "<admin_metadata_ref>" ;
    crm:P33_used_specific_technique <process> .

<process> a crm:E29_Design_or_Procedure ; 
    rdfs:label "<process>" ;
    crm:P2_has_type <process_type> ;
    rdfs:comment “<process_description>” .

<process_type> a crm:E55_Type ; 
    rdfs:label "<prcoess_type>" .
```
