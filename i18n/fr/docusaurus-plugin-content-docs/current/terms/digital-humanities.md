---
id: digital-humanities
title: Humanités numériques (DH)
definition: Domaine scientifique dans lequel les outils et technologies numériques sont utilisés pour explorer les questions de recherche en sciences humaines.
---

Les humanités numériques (DH) sont un domaine scientifique dans lequel les outils et technologies numériques sont utilisés pour explorer les questions de recherche en sciences humaines. Les projets en DH peuvent être grands ou petits; les grands projets sont souvent transdisciplinaires et impliquent une équipe de collaborateurs, qui peut comprendre des professeurs, des étudiants, des spécialistes des technologies de l’information et des partenaires institutionnels (universités, archives, bibliothèques et musées). Les activités courantes de DH comprennent la numérisation, la compilation de données, la conception de bases de données, la cartographie du système d’information géographique (SIG), l’extraction et l’analyse de texte, la visualisation de données et le développement Web.

## Exemples

- [Map of Early Modern London](https://mapoflondon.uvic.ca/)
- [Orlando](http://www.artsrn.ualberta.ca/orlando/)
- [Yellow Nineties](https://1890s.ca/)

## Autres ressources

- [Digital Humanities (Wikipedia)](https://en.wikipedia.org/wiki/Digital_humanities)
- Heppler (2015) [“What Is Digital Humanities?”](https://whatisdigitalhumanities.com/)
