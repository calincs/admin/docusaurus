---
id: linked-open-data
title: Données ouvertes liées (LOD)
definition: Données liées et utilisant des sources ouvertes.
---

Les données ouvertes liées (LOD) sont des données qui répondent aux exigences des deux :Term[Données liées (LD)]{#linked-data} et Open Data : elles sont à la fois liées et utilisent des sources ouvertes.

Le niveau de détail 5 étoiles décrit les données qui permettent d’obtenir une liaison et une ouverture idéales :

1. ☆ : Rendez vos contenus disponibles sur le Web sous une licence ouverte
2. ☆☆ : Rendez-le disponible sous forme de données structurées (par exemple, Excel au lieu d’un scan d’un tableau)
3. ☆☆☆ : Utilisez des formats non propriétaires (par exemple, CSV au lieu d’Excel)
4. ☆☆☆☆ : Utilisez :Term[Uniform Resource Locator (URL)]{#uniform-resource-locator} pour identifier les choses, afin que les gens puissent pointer vers vos affaires
5. ☆☆☆☆☆ : reliez vos données aux données d’autres personnes pour fournir un contexte

## Exemples

- [Linked Open Data Cloud](https://lod-cloud.net/)

## Autres ressources

- Blaney (2017) [“Introduction to the Principles of Linked Open Data”](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- 5-Star Open Data (2015) [“5-Star Open Data”](https://5stardata.info/en/)
- Uyi Idehen (2019) [“What is the Linked Open Data Cloud, and Why is it Important?”](https://medium.com/virtuoso-blog/what-is-the-linked-open-data-cloud-and-why-is-it-important-1901a7cb7b1f)
