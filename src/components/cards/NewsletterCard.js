import React from 'react';
import styles from "./cards.module.css";
import Link from '@docusaurus/Link';

function NewsletterCard({ src, alt, date, link }) {
    return (
        <div className={styles.newsletterCard}>
            <Link to={link}>
                <img src={src} alt={alt} />
            </Link>

            <h3>{date}</h3>
        </div>
    );
}

export default NewsletterCard;