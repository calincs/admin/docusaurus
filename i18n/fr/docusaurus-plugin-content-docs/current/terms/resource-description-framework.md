---
id: resource-description-framework
title: Cadre de description des ressources (RDF)
definition: Une norme pour les données liées (LD) qui représente des informations dans une série d’«instructions» en trois parties appelées un triplet, qui comprend un sujet, un prédicat et un objet sous la forme sujet-prédicat-objet.
---

Le Resource Description Framework (RDF) est une norme pour l’échange de données et le format pour :Term[Données liées (LD)]{#linked-data}. RDF représente les informations dans une série d’«instructions» en trois parties appelées :Term[triple]{#triple} qui comprend un sujet, un prédicat et un objet sous la forme : `<subject><predicate><object>`. De cette manière, RDF décrit les données en définissant les relations entre les objets de données. RDF permet également l’utilisation de :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier} afin que les données puissent être identifiées de manière unique et reliées entre elles : plusieurs triplets, et des triplets provenant de plusieurs sources, peuvent tous pointer vers le même URI pour coder qu’ils font tous référence au même :Term[entité]{#entity} ou concept. Cette utilisation partagée de :Term[authorities]{#authority-record} fait partie de ce qui rend possible la partie “liée” de LD. Les données RDF (triples) sont stockées dans une :Term[triplestore]{#triplestore} et peut être interrogé à l’aide de :Term[SPARQL]{#sparql-protocol-and-rdf-query-language}.

## Exemples

- Lincoln (2015) [“Using SPARQL to access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL) : L’exemple suivant montre comment vous représenteriez le concept que Rembrandt van Rijn a créé _The Nightwatch_, à la suite de RDF.

`<Rembrandt van Rijn><created><The Nightwatch> .`

- Lincoln (2015) [“Using SPARQL to access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL) : L’exemple suivant montre comment vous représenteriez le concept que Rembrandt van Rijn a créé _The Nightwatch_, suivant RDF en utilisant des URI.

```turtle
<http://vocab.getty.edu/page/ulan/500011051> <http://purl.org/dc/terms/creator>
<http://data.rijksmuseum.nl/item/8909812347> .
```

## Autres ressources

- Fullstack Academy (2017) [“RDF Tutorial—An Introduction to the Resource Description Framework”](https://www.youtube.com/watch?v=zeYfT1cNKQg) [Vidéo]
- [Resource Description Framework (Wikipedia)](https://en.wikipedia.org/wiki/Resource_Description_Framework)
- W3C (2014) _[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)_
