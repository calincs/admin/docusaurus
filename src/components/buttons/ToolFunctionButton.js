import React from "react";
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
} from "@fortawesome/free-solid-svg-icons";
import functionTypes from "../functions";

library.add(
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
);

function FunctionButton({
  link = null,
  buttonName = null,
  icon = null,
  className,
}) {
  const containerClass = className ? styles[className] : styles.functionButton;
  const nameClass = className
    ? styles[`${className}Name`]
    : styles.functionButtonName;

  return (
    <div className={containerClass}>
      <div className={nameClass}>
        {className ? (
          <>
            <div>
              <FontAwesomeIcon icon={functionTypes[buttonName].icon} />
            </div>
            {functionTypes[buttonName].id}
          </>
        ) : (
          <>
            {functionTypes[buttonName].id}
            <div>
              <FontAwesomeIcon icon={functionTypes[buttonName].icon} />
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default FunctionButton;
