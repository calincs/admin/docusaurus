---
id: turtle
title: Turtle
definition: Un langage de balisage lisible par l’homme et la machine qui permet aux utilisateurs de sérialiser des triples.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Turtle (Terse RDF Triple Language) est un langage de balisage hautement lisible par l'homme et la machine qui permet aux utilisateurs d'exprimer des :Term[triples]{#triple} conformément au :Term[Resource Description Framework (RDF)]{#resource-description-framework}.

La syntaxe de Turtle permet aux utilisateurs de regrouper trois :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier} pour former un triple. Elle permet également d'abréger les informations contenues dans chaque triple, puisque les utilisateurs peuvent définir des préfixes au début de leur fichier TTL de manière à ce que les parties communes des URI puissent être supprimées.

## Exemples

- L'exemple suivant montre une déclaration TTL de base dans laquelle la relation entre Margaret Laurence et son roman, _The Stone Angel_, est exprimée :

```turtle

<http://example.org/person/Margaret_Laurence>
   <http://example.org/relation/author>
   <http://example.org/books/The_Stone_Angel> .

```

{/*TODO: add example using prefixes */}

## Autres ressources

- DBpedia (2023) [“About: Turtle (Syntax)”](<https://dbpedia.org/page/Turtle_(syntax)>)
- [Turtle (Wikipedia)](<https://en.wikipedia.org/wiki/Turtle_(syntax)>)
- W3C (2014) [“RDF 1.1 Turtle”](https://www.w3.org/TR/turtle/)
