import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";

function EntityLink({ entityLink, imageCredits }) {
  if (imageCredits) {
    return (
      <div className={styles.entity}>
        {entityLink ? (
          <EntityButton link={entityLink} tooltip={imageCredits} />
        ) : null}
      </div>
    );
  } else {
    return (
      <div className={styles.entity}>
        {entityLink ? <EntityButton link={entityLink} /> : null}
      </div>
    );
  }
}

function CarouselCard({ dataset }) {
  const { longTitle, shortDescription, page, images, buttons, published } =
    dataset;
  const image = images["entity"] || images["logo"];
  const entityLink = "entity" in images ? image.entityLink : null;

  return (
    <div className={styles.slide}>
      <div className={styles.carouselBackground}>
        <div className={styles.carouselContent}>
          <h3>{longTitle}</h3>
        </div>

        <div className={styles.carouselImage}>
          {image ? (
            <img src={image.file} alt={image.altText}></img>
          ) : (
            <div className={styles.rectangle}></div>
          )}
          <EntityLink entityLink={entityLink} imageCredits={image.credits} />
        </div>

        <div className={styles.carouselDescription}>
          <p>{shortDescription}</p>
        </div>

        <div className="carousel-primary-button-row">
          {published ? (
            <PrimaryButton
              link={buttons["RS Search"].link}
              buttonName={buttons["RS Search"].label}
            />
          ) : null}
          <PrimaryButton
            link={buttons["Project"].link}
            buttonName={buttons["Project"].label}
          />
        </div>
      </div>
    </div>
  );
}

export default CarouselCard;
