---
id: triple
title: Tripler
definition: Une déclaration sous la forme sujet-prédicat-objet qui suit le cadre de description des ressources (RDF).
---

Un triplet est un énoncé sous la forme sujet-prédicat-objet. Ces assertions sont faites à l’aide de la :Term[Resource Description Framework (RDF)]{#resource-description-framework} et sont ce qui constitue le :Term[Web sémantique]{#semantic-web}.
