import Translate from "@docusaurus/Translate";
import React from "react";

const functionTypes = {
  Clean: {
    id: <Translate id="functionButton.cleanLabel" description="Clean Function Button Label">Clean</Translate>,
    link: "/docs/create-lod/clean-data/#clean-your-dataset",
    icon: "fa-broom",
  },
  Reconcile: {
    id: <Translate id="functionButton.reconcileLabel" description="Reconcile Function Button Label">Match Entities</Translate>,
    link: "/docs/create-lod/match-entities",
    icon: "fa-link",
  },
  Transform: {
    id: <Translate id="functionButton.transformLabel" description="Transform Function Button Label">Transform</Translate>,
    link: "/docs/create-lod/map-data/implement-conceptual-mapping#transform-your-data",
    icon: "fa-arrows-rotate",
  },
  Browse: {
    id: <Translate id="functionButton.browseLabel" description="Browse Function Button Label">Browse</Translate>,
    link: "/docs/explore-lod/#browse",
    icon: "fa-eye",
  },
  Search: {
    id: <Translate id="functionButton.searchLabel" description="Search Function Button Label">Search</Translate>,
    link: "/docs/explore-lod/#search",
    icon: "fa-magnifying-glass",
  },
  Visualize: {
    id: <Translate id="functionButton.visualizeLabel" description="Visualize Function Button Label">Visualize</Translate>,
    link: "/docs/explore-lod/#visualize",
    icon: "fa-diagram-project",
  },
};

export default functionTypes;