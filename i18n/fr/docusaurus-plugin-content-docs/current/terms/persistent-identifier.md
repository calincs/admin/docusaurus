---
id: persistent-identifier
title: Persistent Identifier (PID)
definition: Une référence durable à un document, un fichier, une page Web ou un autre objet numérique.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Un identifiant persistant (PID) est une référence durable à un document, un fichier, une page web ou un autre objet numérique. Les PID sont différents des :Term[Uniform Resource Locators (URLs)]{#uniform-resource-locator} car ils ne peuvent pas être cassés. Ils sont généralement fournis par des services qui créent des identifiants pointant systématiquement vers le même objet numérique, même si son emplacement change au fil du temps.

Les :Term[FAIR Principles]{#fair-principles} suggèrent d'utiliser les PID pour atteindre les objectifs de la science ouverte.

## Exemples

- Un :Term[ORCID]{#open-researcher-and-contributor-id} ID
- Un :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} du :Term[Virtual International Authority File (VIAF)]{#virtual-international-authority-file}
- A :Term[Digital Object Identifier (DOI)]{#digital-object-identifier}

## Autres ressources

- Brown (2021) [“PIDs: What Do Researchers Need to Know?”](https://vimeo.com/525702819) [Video]
- Canadian Research Knowledge Network (2024) [“PIDs”](https://www.crkn-rcdr.ca/en/what-is-a-persistent-identifier-pid)
- CERN Scientific Information Service (2020) [“What are Persistent Identifiers?”](https://scientific-info.cern/submit-and-publish/persistent-identifiers/what-are-pids)
Meadows, Haak, & Brown (2019) [“Persistent Identifiers: the Building Blocks of the Research Information Infrastructure”](https://insights.uksg.org/articles/10.1629/uksg.457)
- Goddard (2020) [“Persistent Identifiers (PIDs) in Canada”](https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/4_PIDs%20in%20Canada_0_Complete%20-%20EN.pdf)
- ORCID (2024) ["What are Persistent Identifiers (PIDs)?"](https://support.orcid.org/hc/en-us/articles/360006971013-What-are-persistent-identifiers-PIDs)
- [Persistent Identifier (Wikipedia)](https://en.wikipedia.org/wiki/Persistent_identifier)