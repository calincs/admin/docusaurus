---
id: international-image-interoperability-framework
title: International Image Interoperability Framework (IIIF)
definition: A set of tools and standards that make digital images interoperable, providing a standardized method of describing and delivering images online.
---

The International Image Interoperability Framework (IIIF) is a set of tools and standards that make digital images interoperable, providing a standardized method of describing and delivering images online. IIIF allows the delivery of both image and structural and presentational :Term[metadata]{#metadata}, structured as :Term[Linked Open Data (LOD)]{#linked-open-data} generally using JSON-LD.

IIIF comprises two main :Term[Application Programming Interfaces (APIs)]{#application-programming-interface}: an Image API and a Presentation API. Image formats are delivered in a :Term[Uniform Resource Locator (URL)]{#uniform-resource-locator} string that specify the format of the final image, ensuring consistency across platforms. The Presentation API gives contextual information, in JSON-LD, and comprises metadata such as a title label, the sequence of the image in its original context, and other such information. These APIs are realized in IIIF-compatible software, such as [Universal Viewer](http://uvviewsoft.com/uviewer/).

## Examples

- IIIF (2017) [“What is IIIF?”](https://www.youtube.com/watch?v=8LiNbf4ELZM): The following presentation and image information demonstrates IIIF compatibility.

### Presentation Information

```JSON
{
  "@context": "http://iiif.io/api/presentation/3/context.json",
  "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/manifest.json",
  "type": "Manifest",
  "label": {
    "en": [
      "Single Image Example"
    ]
  },
  "items": [
    {
      "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/canvas/p1",
      "type": "Canvas",
      "height": 1800,
      "width": 1200,
      "items": [
        {
          "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/page/p1/1",
          "type": "AnnotationPage",
          "items": [
            {
              "id": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/annotation/p0001-image",
              "type": "Annotation",
              "motivation": "painting",
              "body": {
                "id": "http://iiif.io/api/presentation/2.1/example/fixtures/resources/page1-full.png",
                "type": "Image",
                "format": "image/png",
                "height": 1800,
                "width": 1200
              },
              "target": "https://iiif.io/api/cookbook/recipe/0001-mvm-image/canvas/p1"
            }
          ]
        }
      ]
    }
  ]
}
```

### Image Information

![Image information from IIF's Image API, demonstrating the cropping and rotating of an example picture](https://iiif.io/api/image/3.0/img/transformation.png)

## Further Resources

- Cramer (2017) [“03 Introduction to IIIF”](https://www.youtube.com/watch?v=EE1YskDrzPs) [Video]
- IIIF (2017) [“What is IIIF?”](https://www.youtube.com/watch?v=8LiNbf4ELZM) [Video]
- [International Image Interoperability Framework (Wikipedia)](https://en.wikipedia.org/wiki/International_Image_Interoperability_Framework)
- GitHub Inc (2022) [“International Image Interoperability Framework”](https://github.com/iiif)
