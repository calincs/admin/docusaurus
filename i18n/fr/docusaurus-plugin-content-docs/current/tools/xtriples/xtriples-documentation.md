---
title: "Documentation XTriples"
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-18
---

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

## Convertir la TEI en CIDOC CRM

Il y a deux façons de créer des documents TEI que XTriples peut convertir en CIDOC CRM :

1. Créer un CIDOC CRM à partir de fichiers LEAF-Writer : Créer des documents en utilisant les fichiers modèles d'entités de [LEAF-Writer](/docs/tools/leaf-writer) (enregistrements concernant des personnes, des lieux, des organisations ou des événements) ou les modèles de lettres, de poèmes et de prose de LEAF-Writer avec des références aux personnes :Term[reconciled]{#entity-matching}.
2. Créer des CIDOC CRM directement à partir des ographies TEI : Transformez votre TEI réconciliée existante pour qu'elle corresponde aux modèles d'entités du CWRC.

Actuellement, XTriples convertit les placeographies et les personographies, mais convertira les bibliographies, les eventographies, les orgographies, les lettres, les poèmes et la prose en CIDOC CRM d'ici l'été 2023.

## Créer CIDOC CRM à partir des fichiers LEAF-Writer

Une fois que vous avez rempli les fichiers modèles (actuellement disponibles pour les notices concernant les [personnes](https://leaf-writer.leaf-vre.org/edit?template=People%20list) et les [lieux](https://leaf-writer.leaf-vre.org/edit?template=Places%20list)) pour votre projet dans LEAF-Writer, téléchargez-les sur [LINCS XTriples (beta)](https://app.xtriples.stage.lincsproject.ca/exist/apps/xtriples/index.html) :

1. Cliquez sur **upload** et sélectionnez le type d'entité qui correspond à votre type de fichier modèle (par exemple, sélectionnez "Placeography reconciled in LEAF-Writer" dans le menu déroulant si vous convertissez des enregistrements de lieux créés dans LEAF-Writer).
2. Choisissez la [sérialisation](/docs/terms/resource-description-framework-serialization) (XML ou TTL) dans laquelle vous souhaitez que votre sortie soit effectuée. Les deux options de sortie sont des représentations CIDOC CRM de votre :Term[TEI data]{#tei-data}.

:::note

Le triplestore LINCS prend le TTL en entrée. Si vous publiez vos données avec LINCS, choisissez TTL comme sortie. Si vous envisagez d'utiliser la sortie dans votre propre projet, choisissez XML ou TTL comme sortie.

1. Cliquez sur **download** et enregistrez le résultat sur votre ordinateur.
2. [optionnel] Soumettez votre CIDOC CRM TTL à LINCS.

:::

## Créer un CIDOC CRM directement à partir des 'ographies TEI

LINCS XTriples peut traiter n'importe quel fichier d'ographie TEI conforme aux modèles d'entité LINCS. Si vous avez de l'expérience dans l'écriture de transformations de feuilles de style extensibles (XSLT), vous pouvez transformer votre TEI pour qu'il corresponde aux modèles LINCS dans LEAF-Writer. D'ici la fin de l'été 2023, ce guide de documentation contiendra des XSLT que vous pourrez modifier et exécuter pour convertir les 'ographies de votre projet en 'ographie TEI LINCS. Si votre TEI ne contient pas d'URI, chargez vos fichiers 'ographiques convertis dans LEAF-Writer pour la réconciliation, et suivez les étapes de [Créer un CIDOC CRM à partir de fichiers LEAF-Writer](#create-cidoc-crm-from-leaf-writer-files).

## Notes

Les notes suivantes décrivent les éléments et attributs TEI utilisés par LINCS XTriples pour créer CIDOC CRM à partir de votre TEI, et comment ajouter ces éléments, attributs et valeurs TEI dans LEAF-Writer.

### URI de personne

XTriples extrait le :Term[authority record URI]{#authority-record} canonique de la valeur @ref du premier enfant &lt;persName> à l'intérieur de &lt;person>. Dans LEAF-Writer, cliquez sur l'icône **Trouver une personne** pour que LEAF-Writer compare le nom de la personne avec les notices d'autorité sur le Web. Sélectionnez l'autorité :Term[URI]{#uniform-resource-identifier} qui correspond à la personne pour laquelle vous créez un enregistrement TEI.

Si vous éditez votre TEI dans un autre éditeur, ajoutez un attribut @ref à &lt;persName> et collez l'URI d'autorité pour votre personne dans la valeur @source. Ajoutez une paire d'attributs et de valeurs @type="standard" à &lt;persName>.

#### Profession

La représentation de l'occupation dans le LINCS CIDOC CRM s'appuie sur la valeur @source &lt;occupation>. Nous recommandons d'utiliser l'URI de l'occupation du [LINCS Occupation Vocabulary](https://vocab.lincsproject.ca/Skosmos/occupation/en/), mais vous pouvez utiliser des URI d'autres vocabulaires (par exemple, le [Art & Architecture Thesaurus occupations vocabulary](https://www.getty.edu/vow/AATFullDisplay?find=painter&logic=AND&note=&page=1&subjectid=300024980)).

Dans LEAF-Writer, faites un clic droit sur **Preferred Occupation Term** dans le modèle et sélectionnez **Edit Tag**. Choisissez l'attribut @source et collez l'URI de la profession, puis cliquez sur **OK** pour enregistrer vos modifications.

Si vous éditez votre TEI dans un autre éditeur, ajoutez un attribut @source à &lt;occupation> et collez l'URI de la profession dans la valeur @source.

#### Genre

La représentation du genre dans LINCS XTriples CIDOC CRM s'appuie sur la valeur @source &lt;sex> du modèle de personne LINCS. Nous recommandons d'utiliser un URI de genre provenant du [Navigateur de vocabulaire > Vocabulaire du Centre canadien de collaboration en recherche sur les écrits > Genre > Concepts plus étroits](https://vocab.lincsproject.ca/Skosmos/cwrc/en/page/Gender), mais vous pouvez utiliser des URI provenant d'autres vocabulaires (par exemple [Homosaurus](https://homosaurus.org/)).

Dans LEAF-Writer, faites un clic droit sur **sex** dans le modèle et sélectionnez **Edit Tag** pour couper et coller l'URI du genre dans la valeur @source.

Si vous éditez votre TEI dans un autre éditeur, ajoutez un attribut @source à &lt;sex> et collez l'URI du genre dans la valeur @source.

Les modèles LEAF-Writer seront éventuellement mis à jour pour refléter les dernières directives d'encodage TEI pour [sex and gender](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ND.html#NDPERSEpc).

#### Nationalité

LINCS XTriples ne traite pas actuellement l'élément &lt;nationality>.
