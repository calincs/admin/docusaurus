---
id: tei-data
title: Date du TEI
definition: Données qui suivent les directives de la Text Encoding Initiative (TEI).
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les données TEI sont des documents conformes à un schéma généré à partir d'une personnalisation des lignes directrices de la :Term[Text Encoding Initiative (TEI)]{#text-encoding-initiative}. Tous les documents TEI sont valides par rapport au schéma d'une personnalisation, même s'il s'agit de "TEI All" (une personnalisation qui contient l'intégralité de la TEI). En outre, les données TEI conformes obéissent au "modèle abstrait" normatif de la TEI décrit dans la prose des [TEI Guidelines](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html).

## Exemples

- L'exemple suivant montre quelques données TEI fragmentaires concernant la personne Joan Braderman :

```xml
<person xml:id="h-nr254">
   <idno type="Wikidata">http://www.wikidata.org/entity/Q28911659</idno>
   <persName>
      <name>Braderman, Joan</name>
   </persName>
   <persName type="preferred">
      <forename>Joan</forename>
      <surname>Braderman</surname>
   </persName>
   <floruit>
      <date>1977</date>
   </floruit>
   <occupation cert="high">Film maker</occupation>
   <affiliation cert="high">Heresies Collective</affiliation>
</person>
```
- L'exemple suivant montre un document TEI complet et valide
```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" 
   type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" 
   type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <!-- Every TEI document requires a title -->
            <title>A Sample TEI Document</title>
            <author ref="#JamesC">James Cummings</author>
         </titleStmt>
         <publicationStmt>
            <publisher>A variety of information about the publisher can go
               here.</publisher>
            <authority>Information about the authority responsible for the
               document can go here</authority>
         </publicationStmt>
         <sourceDesc>
            <p>A variety of bibliographic references about the source file, even
               if born-digital, can go here.</p>
         </sourceDesc>
      </fileDesc>
   </teiHeader>
   <standOff>
      <listPerson>
         <!-- Information about people mentioned in the document can go here -->
         <person xml:id="JamesC">
            <persName type="main">
               <forename>James</forename>
               <surname>Cummings</surname>
            </persName>
            <affiliation>Newcastle University</affiliation>
         </person>
      </listPerson>
   </standOff>
   <text>
      <body>
         <div>
            <head>About the TEI</head>
            <p> The Text Encoding Initiative (TEI) is a membership consortium
               which, amongst many other things, maintains a community-developed
               standard for the representation of texts in digital form. Its
               chief deliverable is a set of Guidelines which specify encoding
               methods for machine-readable texts, chiefly in the humanities,
               social sciences and linguistics. </p>
         </div>
         <div>
            <head>About the TEI Guidelines</head>
            <p>The <ref
                  target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html"
                  >TEI Guidelines</ref> are a very generalised encoding
               framework which enables many and disparate uses across a wide
               range of (usually Humanities-based) domains. TEI projects
               customise their use of the TEI framework through a TEI ODD
               customisation, often created through web tools such as <ref
                  target="https://roma.tei-c.org/">Roma</ref>.</p>
            <!-- More documentation here... -->
         </div>
      </body>
   </text>
</TEI>

```
