// src/components/GlossaryTerm.js

import React from "react";
import { Tooltip } from "react-tooltip";
import Link from "@docusaurus/Link";
import styles from "./glossary.module.css";

const GlossaryTerm = ({ termId, label, definition, path, tooltipId }) => {
  return (
    <>
      <Link to={path} data-tooltip-id={tooltipId} className={styles["glossary-term"]}>
        {label}
      </Link>
      <Tooltip
        id={tooltipId}
        wrapper="span"
        className={styles["glossary-term-tooltip"]}
        content={definition}
      />
    </>
  );
};

export default GlossaryTerm;
