---
sidebar_position: 2
title: "Instructions & Introductions"
description: "Model data in a LINCS-compatible manner"
---

import ZoomableImage from "@site/src/components/imageGallery/ZoomableImage";


## Purpose

To document how various facets of LINCS data are modelled, along with reference authorities for the populating :Term[vocabularies]{#vocabulary}. This document will provide a basis for instruction for how to model data in a LINCS-compatible manner, as well as aid in navigation and discovery.

## General Guidelines

For a detailed description on implementing the :Term[CIDOC CRM]{#cidoc-crm} ontology using an :Term[RDF]{#resource-description-framework} encoding, see: [Implementing CIDOC CRM in RDF](https://cidoc-crm.org/sites/default/files/issue%20443%20-%20Implementing%20CIDOC%20CRM%20in%20RDF%20v1.1.pdf).

## Document Structure

Each :Term[node]{#node} (pattern entry) includes the following information to answer the following five questions:

1. What is the concept?
    * Pattern category
    * Pattern name
    * Pattern definition
2. How is the concept represented?
    * Visual representation of pattern (diagram)
    * Pattern abstraction (class → :Term[property]{#property} → class chains)
3. How does the data populate or fit into this representation?
    * Content descriptions
        * Type of value
        * Expected value
        * Format or other requirements for the value
    * Example with abstraction
        * Typical use case
        * Edge case (if application)
4. What else should I know?
    * Resource links
        * Resources from the project shout the entity or property definitions
        * Additional related resources
    * Discussion elements pertaining to the pattern (if applicable)
    * Datasets that include the pattern
5. What does the data look like when it is in RDF (:Term[TTL]{#turtle}) and following this pattern?
    * Pattern in TTL
    * Link to :Term[SPARQL]{#sparql-protocol-and-rdf-query-language} query searching for an example instance of the pattern from LINCS

## Data Requirements

The patterns documented are intended to show the possibility for data that can be represented. The only element that is required is for an :Term[entity]{#entity} to be declared as a class so that it can be included in discovery for types of entities. Beyond that, the properties that connect that entity to other entities or datum are optional based on the requirements of the data.

## Overview Diagram

Below is an image of the application overview diagram. Follow this link for a [zoomable, more readable version](https://drive.google.com/file/d/18RQAeF9VN97BKCe64nE9Hf6kLFaSqKJr/view?usp=share_link). The segments below align with sectioned documents.

<ZoomableImage path="/img/documentation/application-profile-introduction-overview-(c-LINCS).png" title="Application profile overview diagram" altlabel="Overview of the application profiles" caption=""/>

## Application Profile Section Documents

### Basic Patterns

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns"
    buttonName="Basic Patterns Application Profile"
    ></PrimaryButton>
</div>

#### Scope

“Basic Patterns” describes patterns that are used across all facets of the data: they are general concepts that are important basic building blocks for more specific data patterns.

#### Ontologies

* CIDOC-CRM
* :Term[Web Annotation Data Model (WADM)]{#web-annotation-data-model}

**Topics covered:**

* Identifiers
  * Unique identifiers
  * Linguistic identifiers (e.g., titles, names)
  * Identifiers as parts of other identifiers
  * Identifier use time-spans
* Types
  * Categorization, classification, and other conceptual groupings
  * Vocabularies
* Events and Activities
  * Time-spans of activities
  * Locations of activities
  * Participating in activities (e.g., roles)
  * Using things (e.g., specific objects, types of objects)
  * Activities as parts of other activities
* Talking about entities (e.g., references, citations, and sources)
  * Describing or referencing an entity (e.g., notes, comments, other common text statements)
  * Visual representations of an entity
  * Assertions and attribute assignments

### People & Organizations

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/explore-lod/understand-lincs-data/application-profiles-main/people-organizations"
    buttonName="People & Organizations Application Profile"
    ></PrimaryButton>
</div>

#### Scope

“People and Organizations” describes patterns that are unique or specific to representing information about persons and groups of persons.

#### Ontologies

* CIDOC-CRM
* FRBRoo

#### Topics

* Actor vs. Person vs. Group
* Identifiers (e.g., names and pseudonyms)
* Contact points (e.g., address, phone number)
* Participation in activities
* Person
  * Birth
  * Death
  * Parent-child relationships
  * Occupation and education activities
  * Interpersonal relationships and marriages
  * Identities (e.g., gender, ethnicity)
* Group
  * Membership
  * Formation

### Places

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/explore-lod/understand-lincs-data/application-profiles-main/places"
    buttonName="Places Application Profile"
    ></PrimaryButton>
</div>

#### Scope 

“Places” describes patterns that are unique or specific to representing information about places as both geographic and social concepts.

#### Ontologies 

* CIDOC-CRM

#### Topics

* Identifiers (e.g., toponyms, gazetteers)
* Types (e.g., uses, functions)
* Location references (e.g., address, latitude/longitude, geo-coordinates)
* Places as parts of other places

### Physical Objects

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/explore-lod/understand-lincs-data/application-profiles-main/physical-objects"
    buttonName="Physical Objects Application Profile"
    ></PrimaryButton>
</div>

#### Scope

“Physical Objects” describes patterns that are unique or specific to representing information about physical objects, and includes reference to their relationship to conceptual things.

#### Ontologies

* CIDOC-CRM
* CRMtex

#### Topics

* Physical objects, conceptual ideas
* Written and ancient texts: CRMtex
* Physical carriers of conceptual things
* Transcribing written texts (CRMtex)
* Talking about entities (e.g., curatorial notes, descriptions, comments)
* Identifiers (e.g., acquisition numbers, titles)
* Types (e.g., classifications and categories)
* Production (including artists, makers, date of production, place of production etc.)
* Physical characteristics
  * Materials (statement and facets)
  * Dimensions (statement and facets)
  * Number of physical parts
  * Objects as parts of other objects
  * Physical features of objects (CRMtex)
  * Segments of written texts (CRMtex)
* Provenance
  * Credit line statement
  * Current and former owners
  * Acquisition
  * Transfer of custody
  * Moves
* Collections
  * Holding institution

### Conceptual Objects

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/explore-lod/understand-lincs-data/application-profiles-main/conceptual-objects"
    buttonName="Conceptual Objects Application Profile"
    ></PrimaryButton>
</div>

#### Scope

“Conceptual Objects” describes patterns that are unique or specific to representing information about conceptual objects, including bibliographic and digital records, and includes reference to their relationship to physical things.

#### Ontologies 

* CIDOC-CRM
* FRBRoo
* CRMtex

#### Topics

* Physical objects, conceptual ideas
* Bibliographic records: WEMI, FRBRoo, and LRMoo
* Written and ancient texts: CRMtex
* Physical carriers of conceptual things
* Transcribing written texts (CRMtex)
* Talking about entities (e.g., descriptions, comments)
* Identifiers (e.g., call numbers, titles)
* Types (e.g., classifications and categories)
* Creation (including authors, makers, date of creation, place of creation etc.)
* Part-whole relationships
  * Number of conceptual parts
  * Conceptual objects as parts of other conceptual objects
  * Linguistic objects as parts of conceptual objects
* Contents
  * “About-ness” and content subjects
  * Referring to other entities
  * Text contents
  * Language
* Bibliographic records
  * Publishing
    * Frequency of publication
  * Making a recording
    * Recording a performance
* Digital records

### Sources & Metadata

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/explore-lod/understand-lincs-data/application-profiles-main/sources-metadata"
    buttonName="Sources & Metadata Application Profile"
    ></PrimaryButton>
</div>

#### Scope

“Sources and Metadata” describes patterns that are unique or specific to representing information about dataset or data object :Term[metadata]{#metadata}, as well as the use of the Web Annotation Data Model (WADM) to connect to sources.

#### Ontologies 

* CIDOC-CRM
* CRMdig
* WADM

#### Topics

* Annotations and the WADM
* Data object metadata
