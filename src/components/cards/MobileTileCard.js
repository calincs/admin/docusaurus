import React, { useState } from "react";
import styles from "./cards.module.css";
import TileButton from "../buttons/TileButton";

function MobileTileCard({ src, title, description, link, buttonName }) {
  const [content, setContent] = useState(false);

  function handleClick() {
    setContent(!content);
  }

  return (
    <div onClick={handleClick} className={styles.mobileTile}>
      {content ? (
        <img src={src} alt="" className={styles.mobileTileBackgroundOnClick} />
      ) : (
        <img src={src} alt="" className={styles.mobileTileBackground} />
      )}

      {content ? (
        <div className={styles.mobileTileContentOnClick}>
          <h3>{title}</h3>
          <p>{description}</p>
          <div className="tile-primary-button-row">
            <TileButton link={link} buttonName={buttonName} />
          </div>
        </div>
      ) : (
        <div className={styles.mobileTileContent}>
          <span>{title}</span>
        </div>
      )}
    </div>
  );
}

export default MobileTileCard;
