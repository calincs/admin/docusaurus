---
id: art-&-architecture-thesaurus
title: Art & Architecture Thesaurus (AAT)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les termes génériques liés à l’art, à l’architecture et au patrimoine culturel visuel.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Le Thésaurus de l'art et de l'architecture (AAT) est l'un des cinq vocabulaires Getty développés par le Getty Research Institute pour fournir des termes et des :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} pour les concepts et les objets liés à l'art. LINCS utilise l'AAT pour fournir des URI pour les termes génériques liés à l'art, à l'architecture et au patrimoine culturel visuel.

L'AAT comprend des termes relatifs à :

- les cultures
- les matériaux
- Rôles
- les styles
- Sujets
- Techniques
- Types d'œuvres

L'AAT exclut les termes iconographiques, les noms propres et les termes composés non liés (par exemple, "cathédrale baroque").

## Ressources complémentaires

- [Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- J. Paul Getty Trust (2022) [“About the AAT”](https://www.getty.edu/research/tools/vocabularies/aat/about.html)