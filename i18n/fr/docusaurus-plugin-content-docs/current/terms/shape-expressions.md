---
id: shape-expressions
title: Expressions de forme (ShEx)
definition: Un langage pour valider et décrire les structures de graphes RDF (Resource Description Framework).
---

Shape Expressions est un langage de validation et de description :Term[Resource Description Framework (RDF)]{#resource-description-framework} structures de graphes. Les descriptions ShEx identifient les prédicats et leurs cardinalités et types de données associés. Les formes ShEx peuvent être utilisées pour communiquer des structures de données associées à certains processus ou interfaces, générer ou valider des données ou piloter des interfaces utilisateur.

## Exemples

- [ShEX (Wikipedia)](https://en.wikipedia.org/wiki/ShEx) : l’exemple suivant indique que les nœuds Person doivent avoir une propriété schema:name avec une valeur de chaîne, et zéro ou plusieurs propriétés schema:knows dont la valeur doit être une Personne.

```shex
  PREFIX: <http://example.org/>
  PREFIX schema: <http://schema.org/>
  :Person {
    schema:name  xsd:string   ;
    schema:knows @:Person   * ;
  }
```

## Autres ressources

- [ShEX (Wikipedia)](https://en.wikipedia.org/wiki/ShEx)
- W3C (2019) _[Shape Expressions (ShEx) 2.1 Primer](https://shex.io/shex-primer/)_
- W3C (2019) [“ShEx”](https://github.com/shexSpec/shex/wiki/ShEx)
