---
id: functional-requirements-for-bibliographic-records
title: Functional Requirements for Bibliographic Records (FRBR)
definition: A model developed by the International Federation of Library Associations and Institutions (IFLA) to restructure catalogues around the conceptual structure of WEMI.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Functional Requirements for Bibliographic Records (FRBR) est un modèle développé par la [Fédération internationale des associations de bibliothécaires et des bibliothèques (IFLA)](https://www.ifla.org/) pour restructurer les catalogues autour de la structure conceptuelle de :Term[WEMI]{#wemi}. Il a ensuite été remplacé par le modèle de référence de la bibliothèque dans [Resource Description and Access (RDA)](https://www.loc.gov/aba/rda/).

## Autres ressources

- [Functional Requirements for Bibliographic Records (Wikipedia)](https://en.wikipedia.org/wiki/Functional_Requirements_for_Bibliographic_Records)
- IFLA (2023) [“Functional Requirements for Bibliographic Records (FRBR)”](https://www.ifla.org/references/best-practice-for-national-bibliographic-agencies-in-a-digital-age/resource-description-and-standards/bibliographic-control/functional-requirements-the-frbr-family-of-models/functional-requirements-for-bibliographic-records-frbr/)
- Tillett (2004) _[What is FRBR?: A Conceptual Model for the Bibliographic Universe](https://www.loc.gov/cds/downloads/FRBR.PDF)_
- Žumer, Aalberg, & O’Neill (2019) [“Application of the FRBR/LRM Model to Continuing Resources”](http://library.ifla.org/id/eprint/2464/1/208-zumer-en.pdf)
