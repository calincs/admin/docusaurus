---
id: xml
title: XML
definition: Un langage de balisage lisible par l’homme et la machine qui permet aux utilisateurs de créer leurs propres balises pour décrire les documents.
---

XML (Extensible Markup Language) est un langage de balisage qui a été publié par :Term[World Wide Web Consortium (W3C)]{#world-wide-web-consortium} en 1998. Il permet aux utilisateurs de créer leurs propres balises pour “marquer” (c’est-à-dire décrire) les documents. Les documents XML peuvent être facilement lus par les humains et les machines. Étant donné que XML stocke les données dans un format texte brut, il est indépendant du logiciel et du matériel, ce qui le rend utile pour le partage de données. XML prend également en charge [Unicode](https://home.unicode.org/), ce qui signifie qu’il peut être utilisé pour transmettre des informations dans la plupart des langues.

XML utilise des éléments pour créer la structure des documents XML. Les éléments décrivent généralement ce qu’est quelque chose. Chaque élément a une balise d’ouverture (par exemple, `<title>`) et une balise de fermeture (par exemple, `</title>`), sauf si l’élément se ferme automatiquement (par exemple, `<lb/>`). Tous les documents XML sont formés comme des “arborescences d’éléments” qui commencent à un élément racine qui contient tous les autres éléments. En imbriquant des éléments, les encodeurs peuvent créer des relations parent-enfant et frère-sœur. Des attributs peuvent être ajoutés aux éléments pour ajouter des informations supplémentaires et les valeurs sont utilisées pour spécifier les attributs.

## Exemples

- L’exemple suivant montre une arborescence XML de base où lélément `<personne>` a été spécifié par l’attribut `@type` et la valeur `"author"`.

```xml
<person type="author">Margaret Laurence</person>
```

- L’exemple suivant montre une arborescence XML de base où `<personne>` est le parent de `<persName>` et `<persName>` est l’enfant de `<personne>`. Les éléments `<reg>`, `<prénom>` et `<nom>` sont tous les enfants de `<persName>` et `<reg>`, `<prénom>` et `<nom>` sont frères et sœurs l’un de l’autre.

```xml
<personne>
    <persName>
        <reg>Margaret Laurence</reg>
        <prénom>Margaret</prénom>
        <nom>Laurence</nom>
    </persName>
</personne>
```

## Autres ressources

- Birnbaum (2021) [“What is XML and Why Should Humanists Care? An Even Gentler Introduction to XML”](http://dh.obdurodon.org/what-is-xml.xhtml)
- Flynn, Silmaril Consultants, & Textual Therapy Division (2022) [“Section 1: Basics General Information About XML”](http://xml.silmaril.ie/basics.html)
- Walsh (1998) [“A Technical Introduction to XML”](https://www.xml.com/pub/a/98/10/guide0.html)
- W3C (2015) [“XML Essentials”](https://www.w3.org/standards/xml/core)
- W3C (2016) [“Extensible Markup Language (XML)”](https://www.w3.org/XML/)
- W3Schools (2022) [“XML Tree”](https://www.w3schools.com/xml/xml_tree.asp)
- W3Schools (2022) [“XML Tutorial”](https://www.w3schools.com/xml/)
