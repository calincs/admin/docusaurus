---
id: simple-knowledge-organization-system
title: Simple Knowledge Organization System (SKOS)
definition: A standard that provides a way to represent thesauri, taxonomies, and controlled vocabularies following the Resource Description Framework (RDF).
---

The Simple Knowledge Organization System (SKOS) is a standard that provides a way to represent :Term[thesauri]{#thesaurus}, :Term[taxonomies]{#taxonomy}, and :Term[controlled vocabularies]{#controlled-vocabulary} following the :Term[Resource Description Framework (RDF)]{#resource-description-framework}.

SKOS allows LINCS vocabularies to be connected to other vocabularies and RDF datasets. All vocabulary terms that are declared in SKOS at LINCS are defined as instances of the class E55 type in :Term[CIDOC CRM]{#cidoc-crm} and are given their own :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier}.

## Further Resources

- [Simple Knowledge Organization System (Wikipedia)](https://en.wikipedia.org/wiki/Simple_Knowledge_Organization_System)
- W3C (2012) [“Introduction to SKOS”](https://www.w3.org/2004/02/skos/)
- Zaytseva & Durco (2020) [“Controlled Vocabularies and SKOS”](https://campus.dariah.eu/resource/posts/controlled-vocabularies-and-skos)
