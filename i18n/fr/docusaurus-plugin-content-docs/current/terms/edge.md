---
id: edge
title: Bord
definition: Une ligne qui relie un nœud à un autre dans une base de données de graphes, représentant une relation entre les nœuds.
---

Une arête est une ligne qui relie un :Term[nœud]{#node} à un autre dans un :Term[base de données de graphes]{#graph-database}, représentant une relation entre eux. Les bords peuvent être dirigés ou non dirigés. Lorsqu’ils sont dirigés, ils ont des significations différentes selon leur direction.

## Exemples

- L’image suivante montre un :Term[Resource Description Framework (RDF)]{#resource-description-framework} graphique mettant en évidence les arêtes qui relient trois nœuds.

![alt=""](/img/documentation/glossary-edge-example-(c-LINCS).jpg)
