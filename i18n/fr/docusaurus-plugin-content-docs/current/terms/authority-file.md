---
id: authority-file
title: Fichier d’autorité
definition: Un fichier d’autorité est une liste qui contient la manière faisant autorité de référencer des personnes, des lieux, des choses ou des concepts, généralement sous la forme d’un en-tête ou d’un identifiant numérique.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Un fichier d'autorité est une liste qui contient la manière autorisée de référencer des personnes, des lieux, des choses ou des concepts, généralement sous la forme d'un titre ou d'un identifiant numérique. Les fichiers d'autorité sont utilisés pour promouvoir le contrôle de l'autorité, c'est-à-dire l'utilisation d'une orthographe et/ou d'un format unique pour référencer quelque chose.

Les fichiers d'autorité sont souvent créés par des organismes faisant autorité. Par exemple, la Bibliothèque du Congrès, la plus grande bibliothèque du monde, a créé des fichiers d'autorité pour les noms d'auteurs et les titres de livres. Ces fichiers d'autorité sont utilisés par les bibliothèques d'Amérique du Nord pour guider les décisions de catalogage. Si plusieurs bibliothèques utilisent la même vedette ou le même identifiant numérique pour désigner le même auteur dans leurs catalogues, les notices de catalogue peuvent être plus facilement reliées entre elles.

De nombreux fichiers d'autorité sont considérés comme des :Term[vocabulaires contrôlés]{#controlled-vocabulary}.

## Exemples

- [Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Cultural Objects Name Authority (CONA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- [Iconography Authority (IA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- [Library of Congress Authorities](https://authorities.loc.gov/)
- [Thesaurus of Geographic Names (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn)
- [Union List of Artist Names (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/index.html)
- [VIAF: The Virtual International Authority File](https://viaf.org/)

## Ressources Complémentaires

- [Authority Control (Wikipedia)](https://en.wikipedia.org/wiki/Authority_control)
- Mason (2023)[“Purpose of Authority Work and Files”](https://www.moyak.com/papers/libraries-bibliographic-control.html)
- OCLC (2023) [“Available Authority Files”](https://help.oclc.org/Metadata_Services/Authority_records/Authorities_Format_and_indexes/Get_started/40Available_authority_files)
