---
description: "Mappez et convertissez les données XML."
title: "X3ML (3M)"
image: /img/documentation/x3ml-overview-logo-(c-owner).png
Last Translated: 2023-07-26
---

<ToolButtons toolName="X3ML (3M)"/>

X3ML est une boîte à outils qui comprend 3M, le Mapping Memory Manager, un outil de mappage de schéma open source qui peut être utilisé pour mapper et convertir des données d’un format ou d’une structure à un autre. Il permet aux experts en données de définir, modifier et enregistrer leurs règles de mappage pour faciliter la conversion des données.

<div className="banner">
  <img src="/img/documentation/x3ml-overview-logo-(c-owner).png" alt="" />
</div>

<div className="primary-button-row">
  <PrimaryButton
    link="https://3m.lincsproject.ca/3M/"
    buttonName="Vers l’outil"
    />
  <PrimaryButton
    link="https://github.com/isl/Mapping-Memory-Manager"
    buttonName="Vers GitHub"
    />
</div>

## X3ML et LINCS

Dans le cadre du projet LINCS, X3ML est utilisé pour convertir des données provenant de diverses sources vers notre :Term[ontologies]{#ontology}, y compris [CIDOC CRM](https://www.cidoc-crm.org/) et [Web Annotation Data Model](https://www.w3.org/TR/annotation-model/).

Le projet LINCS héberge sa propre instance de 3M accessible via une [connexion](https://3m.lincsproject.ca/3M/). N’importe qui peut créer un compte pour utiliser cette instance de 3M. Cependant, le téléchargement de vos données sur cette instance de 3M ne signifie pas que vous contribuez à LINCS car 3M est un outil de conversion de données et non notre :Term[triplestore]{#triplestore}. Bien que vous puissiez utiliser cette instance de 3M pour convertir vos données et télécharger vos fichiers, LINCS ne conservera aucun des fichiers téléchargés dans un stockage à long terme.

Bien que 3M puisse être utile pour les experts en données en dehors de LINCS, il est important pour ceux qui sont en train de préparer leurs données pour la contribution au triplestore LINCS d’assurer la liaison avec l’équipe LINCS avant de cartographier leurs données dans 3M.

:::info

Intéressé par l'ingestion de vos données dans LINCS ? Consultez notre documentation sur les [workflows de conversion](//docs/create-lod/workflow-paths) pour plus d'informations.

:::

## Conditions préalables

Utilisateurs de X3ML :

- Besoin de venir avec son propre ensemble de données
- Besoin d’une compréhension approfondie des [ontologies](/docs/learn-lod/linked-open-data-basics/concepts-ontologies)
- Nécessité de créer un [compte utilisateur](https://3m.lincsproject.ca/3M/SignUp?lang=en)

X3ML prend en charge les entrées et sorties suivantes :

- **Entrée :** XML
- **Sortie :** RDF/XML, Tortue, N-Triples

## Ressources

Pour en savoir plus sur X3ML, consultez les ressources suivantes.

**Articles techniques et informations générales :**

* Kritsotakis et al. (2022) [“3M Guidelines”](https://gitlab.isl.ics.forth.gr/cci/3m-docker/-/blob/master/manual.pdf)
* Theodoridou et al. (2010) [“Mapping Memory Manager (3M) Instance and Label Generator Rule Builder”](https://mapping.d4science.org/3M/Manuals/en/X3ML_Generators_Manual.pdf)

**Introductions et tutoriels accessibles :**

* Bruseker (2018) [“General Introduction to the Use of X3ML Toolkit”](https://cidoc.mini.icom.museum/wp-content/uploads/sites/6/2018/12/25.09_WS_CRM.5_Bruseker_X3MLToolkitTutorial.pdf)
* Kräutli (2018) [“CIDOC-CRM by Practice”](https://dh-tech.github.io/workshops/2018-10-15-CIDOC-CRMbyPractice/#/) [Vidéo]
* Theodoridou (2021) [“Mapping & Transforming Data for Semantic Integration: The X3ML Toolkit”](https://www.rd-alliance.org/system/files/documents/4_3M-X3ML-RDA%2017VPM%20%5B2021-04-20%5D.pptx)
* Theodoridou (2018) [“The X3ML Toolkit”](https://projects.ics.forth.gr/isl/cci/demos/brochures/CIDOC2018-X3ML-Tutorial.pdf) 

**Manuels techniques officiels :**

* Marketakis et al. (2017) [“X3ML Mapping Framework for Information Integration in Cultural Heritage and Beyond”](https://dl.acm.org/doi/10.1007/s00799-016-0179-1)
* Minadakis et al. (2015) [“X3ML Framework: An Effective Suite for Supporting Data Mappings”](http://users.ics.forth.gr/~fgeo/files/X3ML15.pdf)

**Étude de cas :**

* Theodoridou (2014) [“Mapping Cultural Heritage Information to CIDOC-CRM”](https://www.slideshare.net/MariaTheodoridou/london-meetup2014-mappingchi2cidoccrm)
