---
sidebar_position: 3
title: "Tool Credits"
description: "Credits for LINCS’s tools"
sidebar_class_name: "hide"
---

Credits are provided for the tools built by LINCS collaborators. For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite).

## Account Service

- **Pieter Botha**, Lead Developer (2020–)
- **Alliyya Mo**, Developer (2020–)
- **Huma Zafar**, Developer (2020–2022)

## Authority Service

- **Pieter Botha**, Developer (2022–)
- **Matej Kosmajac**, Developer (2022)
- **Natalie Hervieux**, Developer (2022–)
- **Dawson MacPhee**, Contributor (2022–)

## Context Explorer

- **Dawson MacPhee**, Lead Developer (2021–)
- **Susan Brown**, UX (2021-)
- **Pieter Botha**, Developer (2020–)
- **Alice Hinchliffe**, Translator (2024-)
- **Rajvi Khatri**, Developer (2022–2023)
- **Jordan Lum**, UX (2021–)
- **Kathleen McCulloch-Cop**, Developer (2020–2022)
- **Alliyya Mo**, Developer (2024-)
- **Sarah Roger**, Contributor (2024-)

## Corpora

Corpora was not developed by LINCS. To learn more about the tool, see [Corpora’s documentation](https://corpora-docs.dh.tamu.edu/).

## CWRC

For more information, see [CWRC’s credits](https://cwrc.ca/about/cwrc_team).

- **Susan Brown**, Director (2010–)
- **Jeffery Antoniuk**, Technical Director (2021–), Programmer and Systems Analyst (2010–2021)
- **Michael Brundin**, Data Integrity and Metadata Co-ordinator (2010–2017)
- **Mihaela Ilovan**, Assistant Director (2021–), Project Manager (2014–2021), Interface Design and Development Consultant (2010–)

## LEAF VRE

For more information, see [LEAF VRE’s credits](https://www.leaf-vre.org/docs/about-leaf/people).

- **Diane Jakacki**, Project Lead
- **James Cummings**, Project Lead
- **Susan Brown**, Project Lead
- **Mihaela Ilovan**, Assistant Director
- **Jeff Antoniuk**, Technical Director
- **Nia Kathoni**, Senior Drupal Developer
- **Umed Singh**, Interface Drupal Developer
- **Luciano Dos Reis Frizzera**, Javascript Developer and Designer
- **Nigel Banks**, Islandora Consultant
- **Jennifer Blair**, Designer
- **Carolyn Black**, Research Associate

## LEAF-Writer

For more information, see [LEAF-Writer’s credits](https://leaf-writer.stage.lincsproject.ca/).

- **Susan Brown**, Director (2011–)
- **James Chartrand**, Lead Developer (2011–2018)
- **Luciano Frizzera**, Lead Developer (2020–), Developer (2019–2020)
- **Andrew MacDonald**, Lead Developer (2018–2020), Developer (2013–2018)
- **Mihaela Ilovan**, Project Manager (2014–)
- **Geoffrey Rockwell**, Scope and Direction Manager (2011–2014)
- **Megan Sellmer**, Tester and Documentation Writer (2011–2016)

## Linked Data Enhancement API

- **Natalie Hervieux**, Developer (2020–)
- **Mohammed Marzookh Farook**, Developer (2021–2022)
- **Ananya Rao**, Developer (2022)
- **Justin Francis**, Developer (2021)

## NERVE

- **Luciano Frizzera**, Lead Developer (2020–)
- **Mihaela Ilovan**, Project Manager (2015–)

## LINCS-API

- **Pieter Botha**, Lead Developer (2020–)

## OpenRefine

OpenRefine was not developed by LINCS. To learn more about the tool, see [OpenRefine’s documentation](https://openrefine.org/).

## ResearchSpace

For more information, see [ResearchSpace’s credits](https://researchspace.org/about-us/).

- **Zach Schoenberger**, LINCS Lead Developer (2020–)
- **Rajvi Khatri**, Developer (2022–)
- **Marco Lian Bantolino,** Contributor (2023)
- **Jordan Lum**, UX (2021–)
- **Kim Martin**, UX (2020–)
- **Evan Rees**, UX (2021–2022)
- **Pieter Botha**, Developer (2020–)

## Rich Prospect Browser

- **Bryan Tarpley**, Lead Developer (2020–)
- **Akseli Palen**, Developer (2022–)
- **Jordan Lum**, UX (2022–)

## SPARQL Endpoint

- **Alliyya Mo**, Lead Developer (2020–)

## Spyral

For more information, see [Spyral’s credits](https://voyant.lincsproject.ca//#!/guide/about-section-credits).

- **Stéfan Sinclair**, Project Lead, Principal Designer, and Principal Programmer (2017–2020)
- **Geoffrey Rockwell**, Project Lead (2020–)
- **Andrew MacDonald**, Principal Programmer (2019–2020), Programmer (2008–2020)

## VERSD

- **Alliyya Mo**, Lead Developer (2020–)
- **Natalie Hervieux**, Reconciliation API Developer (2020-)

## Vocabulary Browser

- **Alliyya Mo**, Developer (2020–)
- **Pieter Botha**, Developer (2020–)

## Voyant

For more information, see [Voyant credits](https://voyant.lincsproject.ca//#!/guide/about-section-credits).

- **Stéfan Sinclair**, Project Lead, Principal Designer, and Principal Programmer (2003–2020)
- **Geoffrey Rockwell**, Project Lead (2020–)
- **Andrew MacDonald**, Principal Programmer (2020–), Programmer (2008–2020)

## X3ML

X3ML was not developed by LINCS. To learn more about the tool, see [X3ML’s documentation](https://github.com/isl/x3ml/blob/master/README.md).

## XTriples

- **Torsten Schrader**
- **Huma Zafar**
- **Alice Defours**
- **Nem Brunell**
- **Rajvi Khatri**
- **Constance Crompton**
