---
title: Code de contribution
description: "Collaborer avec LINCS en contribuant au code et en améliorant notre base de code"
Last Translated: 2023-07-26
---

LINCS accueille favorablement les contributions et les extensions de son code. Les développeurs qui souhaitent collaborer à des projets de conversion, de stockage et d’accès aux :Term[Données ouvertes et liées (LOD)]{#linked-open-data} sont encouragés à [nous contacter](/docs/about-lincs/get-involved/contact-us).

Notre code est accessible sur [GitLab](https://gitlab.com/calincs/) et, dans la mesure du possible, il a été mis à disposition pour être réutilisé sous des licences open source.

Le code et la documentation technique de chaque outil sont accessibles via le bouton **Vers GitLab** de la page d’accueil de chaque outil.

Des informations sur l’équipe à l’origine de chaque outil sont disponibles sur la page [Crédits d’outils](/docs/about-lincs/credits/tools-credits). LINCS vous encourage à [créditer nos développeurs](/docs/about-lincs/cite) pour leur travail.
