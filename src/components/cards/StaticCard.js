/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";
import Link from "@docusaurus/Link";

function StaticCard({
  src,
  title,
  description,
  buttonName1,
  buttonName2,
  link1,
  link2,
  subtitle,
  imageFit = "cover",
}) {
  return (
    <Link to={link1} className={styles.staticCardLink} title={description}>
      <div className={styles.cardContainer}>
      <div className={styles.cardTitle}>
          <h3>{title}</h3>
          {subtitle ? (
            <div className={styles.staticCardSubtitle}>
              <span className={styles.staticCardSubtitle}>{subtitle}</span>
            </div>
          ) : null}
        </div>

        <img
          src={src}
          alt=""
          className={styles.cardImage}
          style={{ objectFit: imageFit }}
        />

        <div className={styles.staticCardDescription}>
          <p>{description}</p>
        </div>

        {/*<div className="spotlight-primary-button-row">
          <PrimaryButton link={link1} buttonName={buttonName1} />
          <PrimaryButton link={link2} buttonName={buttonName2} />
      </div>*/}
      </div>
    </Link>
  );
}

export default StaticCard;
