import React from 'react';
import Translate from '@docusaurus/Translate';
import Link from '@docusaurus/Link';
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";
import { Tooltip } from "react-tooltip";
function EntityButton({ link, tooltip}) {
    if (tooltip) {
        return (
          <>
            <Link
              className={styles.entityButton}
              data-tooltip-id="entityButton"
              data-tooltip-content={tooltip}
              data-tooltip-place="top"
              to={link}
            >
              <Translate
                id="homepage.carousel.viewEntityLabel"
                description="Carousel View Entity Button Label"
              >
                View Entity
              </Translate>
              <div className={styles.entityIconShift}>
              </div>
            </Link>
            <Tooltip id="entityButton" className="entityButton-tooltip" place='top' />
          </>
        );
    }


    return (
        <Link
            className={styles.entityButton}
            to={link}>
            <Translate id="homepage.carousel.viewEntityLabel" description="Carousel View Entity Button Label">View Entity</Translate>
            <div className={styles.entityIconShift}>
            </div>
        </Link>
    );
}

export default EntityButton;