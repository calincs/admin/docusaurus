---
id: mapping
title: Cartographie
definition: Processus conceptuel consistant à associer des valeurs ou des champs de métadonnées équivalents d’un schéma à un autre.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

La mise en correspondance fait référence au processus conceptuel consistant à associer des valeurs ou des champs :Term[metadata]{#metadata} équivalents d'un schéma avec des valeurs ou des champs de métadonnées d'un autre schéma. La mise en correspondance est également appelée :Term[crosswalking]{#crosswalking}.

## Exemples

- Le tableau suivant montre comment vous pouvez mapper le "titre" entre les modèles de données.

| Nom du modèle de départ | Début de la période du modèle | Destination Nom du modèle | Destination Modèle Terme                             |
| ----------------------- | ----------------------------- | ------------------------- | -------------------------------------------------- |
| CDWA                    | Title Text                    | VRA Core                  | `<vra: title> in <vra: work> or <vra: collection>` |
| Dublin Core             | Tile                          | MODS                      | `<titleInfo><title>`                               |
| DACS                    | 2.3 Title                     | CIDOC CRM                 | `E35_Title`                                        |

## Autres ressources

- Harping (2022) [“Metadata Standards Crosswalk”](https://www.getty.edu/research/publications/electronic_publications/intrometadata/crosswalks.html)
- [Schema Crosswalk (Wikipedia)](https://en.wikipedia.org/wiki/Schema_crosswalk)
- TEI (2018) [“Crosswalks”](https://wiki.tei-c.org/index.php/Crosswalks)