---
id: triplestore
title: Triplestore
definition: Une base de données NoSQL qui stocke les triplets.
---

Un triplestore est une sorte de :Term[Base de données NoSQL]{#nosql-database} qui stocke :Term[triples]{#triple}. Il présente des similitudes avec :Term[bases de données de graphes]{#graph-database}, mais les deux termes ne sont pas synonymes—voir le tableau de comparaison ci-dessous. Les triplestores peuvent être interrogés à l’aide de :Term[SPARQL]{#sparql-protocol-and-rdf-query-language}.

| Base de données graphique                                          | Triplestore                             |
| ------------------------------------------------------------------ | --------------------------------------- |
| Prend en charge une variété de langages de requête tels que Cypher | Utilise SPARQL comme langage de requête |
| Stocke divers types de graphiques                                  | Stocke des rangées de triples           |
| Centré sur les nœuds/propriétés                                    | Centré sur les bords                    |
| Ne fournit pas d’inférences sur les données                        | Fournit des inférences sur les données  |
| Moins académique                                                   | Plus synonyme de “web sémantique”       |

## Exemples

- [Apache Jena](https://jena.apache.org/)
- [BlazeGraph](https://blazegraph.com/)
- [GraphDB](https://www.ontotext.com/products/graphdb/)
- [Virtuose](https://virtuoso.openlinksw.com/)

## Autres ressources

- Gilbert (2016) [“Triplestores 101: Storing Data for Efficient Inferencing”](https://www.dataversity.net/triplestores-101-storing-data-efficient-inferencing/#)
- Ontotext (2022) [“What is an RDF Triplestore?”](https://www.ontotext.com/knowledgehub/fundamentals/what-is-rdf-triplestore/)
- [Triplestore (Wikipedia)](https://en.wikipedia.org/wiki/Triplestore)
