---
title: "VERSD Documentation"
Last Translated: 2024-06-17
---

## Prerequisites

### Create an Account

You need a LINCS account to access VERSD. See [Account Service Documentation](/docs/tools/account-service/account-service-documentation) for more information.

### Prepare your Dataset

You need a :Term[structured data]{#structured-data} dataset (:Term[JSON]{#json} or CSV) to use VERSD. Make sure your dataset is clean before starting the :Term[entity matching]{#entity-matching} process. For more information about cleaning your data, check out the [data cleaning step](/docs/create-lod/clean-data) of the data conversion workflow and the LINCS [data cleaning guide](/docs/create-lod/clean-data/data-cleaning-guide).

:::note

VERSD’s entity matching algorithm takes into account the relationships between the records in your dataset when determining candidate matches. If the records in your dataset are related (e.g., they are all authors from the same time period), VERSD will use this information to make more accurate recommendations. While it takes longer for VERSD to process large datasets, the results tend to be more accurate when there are more records.

:::

## Create an Entity Matching Request

Complete an Entity Matching Request to start using VERSD. This request will allow VERSD to save your job so you do not have to re-upload and re-map your dataset each time you want to match entities :Term[entities]{#entity}.

To complete an Entity Matching Request, fill out the fields on the Entity Matching Request page:

1. Give your Entity Matching Request a name.
2. Upload your dataset (JSON or CSV). If you have a small JSON dataset, you can paste it directly into the textbox to upload it.
3. Choose the type(s) of data you have in your dataset (bibliographic, prosopographic, or geospatial). The data type you choose will change the :Term[authorities]{#authority-file} that are recommended by VERSD.
4. Choose which authorities you would like to match entities to. You can select multiple authorities and order them by priority.
5. Click **Configure Mappings of Headings**.

:::note

VERSD primarily supports bibliographic data. While :Term[Wikidata]{#wikidata} will give you the greatest number of results, results from :Term[VIAF]{#virtual-international-authority-file} are more authoritative. For more information on authorities and how to choose between them, check out the [LINCS entity matching guide](/docs/create-lod/match-entities/entity-matching-guide.md).

:::

## Complete your Mapping

### Map the Fields

After uploading your dataset, you need to :Term[map]{#mapping} the fields in your dataset to the fields of the authority.

Click the down arrow next to each field to see a drop-down list of the fields in your dataset. Note that you can map multiple fields from your data to each field of the authority. For example, if your dataset has a “First Name” field and a “Last Name” field, you can add them both to the “Name(s)” field in VERSD.

:::warning

It is important to capture everything you can during the mapping phase. If you do not map a field from your dataset, it will not be used by the reconciliation service to find candidate entities. The field will still be available as extra context for vetting matches.

:::

Click **Done** once you are happy with your mapping.

### Adjust the Threshold

Use the first slider to determine how many candidates you would like to be suggested for each record in your dataset. The more candidates you want to see, the longer it will take for the entity matching service to process your dataset.

Use the second slider to determine how close the candidate entities should match the records in your dataset. The higher the threshold, the closer the match. LINCS suggests using a threshold of 70-99% for the best results.

## Submit Your Request

Click **Submit** to start processing your dataset using the entity matching service. On the processing page you will be able to see all of your past jobs and whether they are _processing_ or _ready_. Since you can return to this page at any time, you do not need to keep VERSD open locally while your dataset is being processed.

Once your job is _ready_, click **Quick Match** to start matching entities.

## Match Entities with Quick Match

VERSD will present you with a record from your dataset and the candidate matches from the authorities you chose during the mapping process.

Compare the candidate entities to the record in your dataset. You have the following options when matching:

- **Full Match:** A candidate entity matches the record in your dataset. A popup will appear where you can double-check the information and accept or reject the candidate entity.
- **Partial Match:** A candidate entity partially matches the record in your dataset. A popup will appear where you can accept and reject parts of the authority’s information and accept or reject the candidate entity.
- **Manual Match:** You already have a :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} for the entity. A popup will appear where you can manually enter the URI for the record.
- **No Match:** None of the candidate entities match the record in your dataset.

VERSD has a few keyboard shortcuts that allow you to speed up the matching process.

| Key |            Action             |
| :-: | :---------------------------: |
|  ←  | Select the previous candidate |
|  →  |   Select the next candidate   |
|  ↑  |       Make a full match       |
|  ↓  |     Make a partial match      |
|  M  |      Make a manual match      |
|  X  |         Make no match         |

A progress bar at the top of the page will show you how far you are into matching the entities in your dataset. VERSD automatically saves your progress. If you refresh your page, VERSD will take you to where you left off in the matching process.

Once you are done matching, you will get a summary popup that gives you statistics about your matches. In this popup, you can click **Export Results** or **New Request**. **Export Results** will take you to a page where you can export the dataset with its newly matched entities as JSON or CSV. On the export page, you can choose which fields you want to be added to your dataset. **New Request** will take you to the Entity Matching Request page.

## Manage Jobs

To access your previous jobs, click **My Requests** in the top navigation toolbar. Here you can delete jobs by clicking **Quick Match** and then **Delete** in the drop-down list.

## View Your Statistics

Each VERSD user has a profile. Click the profile icon in the top right corner to see your profile page. On your profile page, you can view your entity matching statistics and see how your entity matching work compares against other VERSD users.
