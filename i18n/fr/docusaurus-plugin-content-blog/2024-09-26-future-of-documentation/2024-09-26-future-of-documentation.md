---
slug: future-of-documentation
title: "The Future of Documentation"
authors: Kirisan Suthanthireswaran
tags: [CWRC, technical, UX]
hide_table_of_contents: true
image: /future-of-documentation-unsplash.jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-08-21
date: "2024-09-26"
---

![](<./future-of-documentation-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

## Le problème

La documentation fait partie intégrante de tous les produits logiciels et, pour être utile, elle doit être complète et détaillée. Cependant, cela représente un défi à la fois pour les utilisateurs et les développeurs. Les utilisateurs, en particulier ceux qui découvrent le logiciel, peuvent trouver décourageant de passer au crible de gros blocs de texte, tandis que les développeurs ont souvent du mal à rendre la documentation attrayante et digeste.

Tout au long de mes études universitaires, j'ai fait partie de la première catégorie. En tant qu'utilisateur, j'ai toujours trouvé la documentation difficile à comprendre. Elle me laissait souvent avec plus de questions que de réponses. Le fait que les sites web de documentation que j'utilisais avaient souvent une mauvaise navigation et peu de ressources pour m'aider à comprendre ce que je lisais ne m'a pas aidé, me laissant parcourir différents sites pour trouver des informations qui étaient cachées dans un coin.

En tant qu'étudiant coopératif chez LINCS, j'ai été chargé de migrer le [site de documentation](https://sparql.cwrc.ca/) du Canadian Writing Research Collaboratory (CWRC). Ce site web fournissait des informations sur les trois ontologies développées par le CWRC et permettait d'y accéder, chacune avec son propre ensemble de documentation. Cela m'a inquiété, car comment développer un site web capable d'afficher efficacement la documentation tout en tenant compte de la navigation, de l'esthétique et de la mise en page ?

{/* truncate */}

## Docusaurus

C'est là que [Docusaurus](https://docusaurus.io/) entre en jeu. Docusaurus est un puissant générateur de site statique construit avec React, qui permet aux utilisateurs de créer rapidement et facilement des sites web axés sur la documentation. Comme Docusaurus est conçu spécifiquement pour la documentation, j'ai pu me concentrer sur la présentation du contenu au lieu de me préoccuper du design. Plutôt que de s'appuyer sur des balises HTML pour afficher le contenu, Docusaurus utilise Markdown (MD) et Markdown eXtended (MDX), un langage convivial à la syntaxe simple et facile à assimiler, qui prend également en charge Javascript XML (JSX). Cela m'a permis de combiner la nature simpliste de MD tout en exploitant la flexibilité et la puissance de JSX.  Docusaurus combine le tout sous forme de fichiers HTML statiques que je pouvais facilement déployer sur le site web du CWRC, et il permettait même d'intégrer des composants React. Sans sacrifier la fonctionnalité, Docusaurus a rationalisé le processus de création et de publication de la documentation.

Docusaurus m'a permis de créer facilement le type de site web dont la documentation du CCRF avait besoin. L'un des objectifs de la migration du site web des données liées du CCRF était d'améliorer l'accessibilité et la lisibilité des informations du CCRF. Le site original, bien que fonctionnel, manquait de structure et de navigation cohérentes, ce qui le rendait difficile à utiliser.

![Table des matières du CCRF avant et après, la table des matières après étant plus lisible en raison de son imbrication correcte et des changements de police/couleur.](future-of-documentation-before-after-(c-LINCS).png)
(Figure : Avant et après)

Ces difficultés se sont également répercutées sur la mise en œuvre, car la structure de codage de la documentation semblait encombrée en raison du nombre de balises HTML nécessaires pour afficher le texte. C'est là que la simplicité de Markdown s'est avérée précieuse, car elle a permis de rationaliser le processus de transition des informations vers le nouveau site web. Bien que je n'aie pas été en mesure d'abandonner complètement les balises HTML, une grande partie de la documentation est maintenant formée avec Markdown, ce qui facilitera le processus de remaniement ou d'édition à l'avenir.

## Outils utiles

Cela ne veut pas dire que Docusaurus n'est utile que pour ses capacités Markdown, car il possède également de nombreux composants et crochets utiles. L'un de ces composants que j'ai souvent utilisé est ```<Link/>```,qui permet de créer des liens à la fois pour les pages internes et externes. Ce qui différencie ce composant des balises HTML de base comme ```<a/>``` est sa capacité à précharger les ressources avant de naviguer vers la page de la ressource. En utilisant le “onMouseOver” pour récupérer les ressources demandées sur cette page, Docusaurus précharge ces ressources afin d'assurer des transitions fluides et efficaces entre les pages. L'accès à des informations supplémentaires par le biais de liens internes et externes rend la documentation plus claire et plus facile à parcourir, réduisant ainsi pour les utilisateurs le type de difficultés que j'ai rencontrées en utilisant la documentation d'autres logiciels.

En tandem avec ses divers composants, Docusaurus supporte également les plugins ESLint avec des règles supplémentaires, qui sont compatibles avec ses composants uniques. Ces plugins permettent aux utilisateurs qui ne connaissent pas Docusaurus (moi y compris) de développer du code tout en adhérant aux meilleures pratiques et en utilisant pleinement les capacités de Docusaurus. En d'autres termes, Docusaurus est construit pour faciliter son utilisation. Il ne s'agit pas seulement d'un avantage, mais aussi d'un excellent exemple de documentation et d'outils faciles à naviguer et à utiliser.

``` text
N'utilisez pas d'élément `<a>` pour naviguer. Utilisez plutôt le composant `<Link />` de `@docusaurus/Link`. Voir: https://docusaurus.io/docs/docusaurus-core#linkeslint@docusaurus/no-html-links
```

## En résumé

Alors qu'une documentation dense et riche en texte s'avère toujours difficile à présenter de manière digeste, Docusaurus aide les développeurs à se concentrer sur leur contenu plutôt que sur les détails de la création d'un site. Avec sa communauté grandissante et son code source facilement disponible pour aider les nouveaux venus à apprendre et à collaborer, il n'est pas surprenant que Docusuarus soit un générateur de site de premier plan pour les sites web axés sur la documentation. Tout en continuant à apprendre les tenants et les aboutissants, je suis toujours impressionné par le caractère réfléchi et intuitif de Docusaurus et je continuerai à utiliser ses capacités au maximum.
