---
id: semantic-narrative
title: Récit sémantique (ResearchSpace)
definition: Un document interactif dans l’environnement ResearchSpace qui combine une narration textuelle et des données liées (LD) pour communiquer des idées sur les personnes, les lieux et les événements.
---

Un récit sémantique est un document interactif qui combine un récit textuel et :Term[Données liées (LD)]{#linked-data} pour communiquer des idées sur des personnes, des lieux et des événements. Dans l’environnement [ResearchSpace](/docs/tools/researchspace), un récit sémantique prend la forme d’un document texte interactif qui intègre des données intégrées, des visualisations de données et est connecté à l’:Term[graphique des connaissances]{#knowledge-graph} pour s’assurer que le récit est automatiquement mis à jour lorsqu’il est modifié ou développé.

## Autres ressources

- British Museum (2021) [“ResearchSpace: Semantic Tools”](https://researchspace.org/semantic-tools/)
- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://link.springer.com/chapter/10.1007/978-3-030-00668-6_20)
