---
slug: marvels-of-api
title: "Les merveilles de la communication API: L'outil de vérification NLP Diffbot"
authors: [Mohammed Marzookh, Ananya Rao]
tags: [technical]
hide_table_of_contents: true
image: ./marvels-of-api-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./marvels-of-api-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

Si vous avez déjà écouté une conversation technique entre informaticiens, vous les avez peut-être entendus parler de ce qu'on appelle une :Term[Application Programming Interface (API)]{#application-programming-interface}. Qu'est-ce qu'une API ? Décortiquons-le...{/* truncate */}

Une **application** est un logiciel qui exécute une fonction distincte. Microsoft Excel et Google Sheets sont de bons exemples d'applications courantes que beaucoup de gens utilisent, même ceux qui ne sont pas informaticiens.

Or, chaque application est soumise à des règles spécifiques quant à la manière d'interagir avec elle. Par exemple, la plupart des utilisateurs de Microsoft Excel savent qu'ils peuvent mettre en gras le texte d'une cellule en cliquant sur le bouton **B** de la barre d'outils du ruban qui apparaît en haut de l'interface d'Excel. Une **interface** est un contrat qui définit les règles d'interaction entre les logiciels.

Ces deux termes - **programme d'application** + **interface** - se rejoignent pour former l'_Application Programming Interface_ ou _API_. Une API est un moyen pour deux ou plusieurs applications de communiquer entre elles à l'aide de demandes et de réponses. En tant qu'analystes de recherche juniors travaillant pour LINCS, nous avons eu de nombreuses occasions de travailler avec des API en essayant de réunir les applications qui composent LINCS.

L'une des API sur lesquelles nous avons travaillé est le flux de travail Diffbot NLP Vetting. L'objectif de ce workflow est d'évaluer les performances de [Diffbot Natural Language Processing (NLP) API](https://docs.diffbot.com/reference/introduction-to-natural-language-api) sur les ensembles de données du LINCS. L'API Diffbot NLP utilise des algorithmes d'apprentissage automatique et de vision par ordinateur pour rechercher du sens dans le texte. En particulier, il identifie des entités (par exemple, des personnes, des organisations, des produits) et des données les concernant (par exemple, les relations entre elles).

Par exemple, à partir du texte "Le 11 novembre 1741, Abigail Smith (plus tard Abigail Adams) est née à Boston, Massachusetts", Diffbot a créé le graphe de connaissances suivant :

![Graphique contenant divers détails tels que la date, le nom et les lieux mentionnés, ainsi que des cercles et des lignes reliant les cercles et décrivant les relations. Par exemple, le cercle Abagail Adams est relié par la ligne intitulée lieu de naissance au cercle Boston.](<./marvels-of-api-graph-(c-LINCS).png>)

En utilisant les données du web, Diffbot a pu identifier les :Term[entités]{#entity} (cercles) du texte ainsi que les relations (flèches) entre eux, qu'il appelle _facts_. L'algorithme d'IA de Diffbot identifie toujours correctement les entités et les faits dans de nombreuses applications pour lesquelles il est utilisé en dehors du LINCS ; cependant, une identification sans erreur n'est pas toujours possible étant donné la complexité des données avec lesquelles le LINCS travaille. Pour y remédier, nous avons voulu créer un outil qui analyserait l'exactitude de ce que Diffbot nous dit : le _Diffbot NLP Vetting Workflow_.

Le _Diffbot NLP Vetting Workflow_ est essentiellement constitué de quelques étapes contenues dans un carnet Jupyter ; son utilisation ne nécessite pas d'expérience approfondie en programmation ni de connaissances approfondies sur le fonctionnement de Diffbot. Dans les coulisses, cependant, les quelques lignes du workflow renvoient à un code Python complexe qui gère la communication API et adhère aux nombreux standards de Diffbot.

À partir d'un texte brut, le flux de travail envoie une requête à Diffbot pour qu'il identifie toutes les entités et tous les faits contenus dans le texte. La réponse qui en découle ressemble à ceci :

``` JSON
{
  "entities": [
    {
      "name": "Boston",
      "diffbotUri": "https://diffbot.com/entity/E7vnJ0j-OP4qXP4s9wNDbEQ", 
      "confidence": 0.9994205,
      "salience": 0.4442726,
      "isCustom": false,
      "allUris": [
        "http://www.wikidata.org/entity/Q100"
      ],
      "allTypes": [
        {
          "name": "location",
          "diffbotUri": "https://diffbot.com/entity/BiCyWUm41NziqgLHx47iAIQ", 
          "dbpediaUri": "http://dbpedia.org/ontology/Place"
        },
        {
          "name": "administrative area",
          "diffbotUri": "https://diffbot.com/entity/EcTIultWKPoula6qZtSpc4A", 
          "dbpediaUri": "http://dbpedia.org/ontology/PopulatedPlace"
        },
        {
          "name": "city",
          "diffbotUri": "https://diffbot.com/entity/EzdJrGHiyMWu0XbSn101rFA", 
          "dbpediaUri": "http://dbpedia.org/  ontology/City"
        }
  ],
  "mentions": [
    {
      "text": "Boston",
      "beginoffset": 163,
      "endOffset": 169,
      "confidence": 0.9994205
    }
  ],
  "location": {
    "latitude": 42.36028,
    "longitude": -71.05778, 
    "precision": 15.231546
  }
},
```

Ce résultat, qui prend la forme d'une réponse JSON, n'est pas facile à évaluer. Pour faciliter l'examen des données, notre programme fait appel à l'API Google Sheets pour générer une feuille de calcul :

![Tableau composé des résultats JSON.](<./marvels-of-api-table-(c-LINCS).png>)

Google Sheets, à son tour, applique un formatage par défaut : masquer, redimensionner et figer les colonnes, ainsi qu'appliquer du gras et des couleurs au texte. Chaque fonction est associée à une requête API différente ; de nombreux essais et erreurs ont été nécessaires pour créer quelque chose qui serait facile à examiner par un humain.

La feuille de calcul qui en résulte montre l'entité, la confiance de Diffbot dans sa propre prédiction (un chiffre entre 0 et 1, 0 étant le moins sûr et 1 le plus sûr), et un lien vers une entité correspondante dans :Term[Wikidata]{#wikidata}, et plus encore. Une personne qui examine les données peut utiliser toutes ces informations pour confirmer ou corriger l'exactitude du travail de Diffbot. Pour rendre cette vérification aussi simple que possible, la dernière colonne de la feuille indique où Diffbot a trouvé l'entité dans le texte original, de sorte que l'examinateur puisse facilement se référer à l'original si d'autres vérifications sont nécessaires. À l'aide des informations contenues dans la feuille de calcul, le réviseur peut alors approuver ou rejeter les résultats de Diffbot (en inscrivant _oui_ ou _non_ dans la feuille de calcul ; colonnes L, M et N dans l'image ci-dessus).

Les informations nouvellement vérifiées (ainsi que les scores de précision globaux pour les données examinées) poursuivent ensuite leur chemin, à travers une nouvelle série d'étapes qui impliquent à la fois des applications et des personnes - un processus en plusieurs étapes qu'il est préférable de réserver pour de futurs articles de blog. Au final, ce qui a commencé comme une phrase dans un document peut être découvert sous forme d'entités et de relations dans le :Term[triplestore]{#triplestore} du LINCS. Et, grâce au _Diffbot NLP Vetting Workflow_, ceux qui utilisent les données du LINCS peuvent être sûrs que le contenu qu'ils explorent est exact !
