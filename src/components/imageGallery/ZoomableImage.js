/** @format */

import React from "react";
import LightGallery from "lightgallery/react";
import "lightgallery/css/lightgallery.css";
import "lightgallery/css/lg-zoom.css";
import "lightgallery/css/lg-thumbnail.css";

import lgThumbnail from "lightgallery/plugins/thumbnail";
import lgZoom from "lightgallery/plugins/zoom";
import lgFullScreen from "lightgallery/plugins/fullscreen";

import useDocusaurusContext from "@docusaurus/useDocusaurusContext";

function ZoomableImage({ path, altlabel, title, caption }) {
  // Potentially could include copyright?

  const { siteConfig } = useDocusaurusContext();
  const fullCaption = `<h1>${title}</h1><em>${caption}</em>`;

  return (
    <div className="zoomable-image">
      <LightGallery
        speed={500}
        plugins={[lgZoom, lgThumbnail, lgFullScreen]}
        counter={false}
        actualSize={false}
        showZoomInOutIcons
        zoom
        licenseKey={siteConfig.customFields.LightGalleryLicense}
        mode={"lg-soft-zoom"}>
        <a href={path} data-sub-html={fullCaption} data-src={path}>
          <img alt={altlabel} title={title} src={path} />
        </a>
      </LightGallery>
    </div>
  );
}

export default ZoomableImage;
