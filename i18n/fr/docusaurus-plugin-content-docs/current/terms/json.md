---
id: json
title: JSON
definition: Un format d’échange de données lisible par l’homme et la machine.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

JSON (JavaScript Object Notation) est un format d'échange de données lisible par l'homme et la machine, souvent utilisé pour envoyer des données entre ordinateurs.

JSON représente les choses sous forme de littéraux d'objets. Chaque objet littéral est entouré d'accolades (`{}`). À l'intérieur du littéral d'objet se trouvent des paires clé-valeur. Les clés et les valeurs sont séparées par deux points et les paires clé-valeur par une virgule.

Toutes les clés sont des chaînes de caractères. Toutes les valeurs doivent être un type de données JSON valide : une chaîne, un nombre, un objet, un tableau, un booléen ou null.

## Exemples

- L'exemple suivant montre une chaîne JSON de base dans laquelle l'objet littéral possède trois paires clé-valeur :

```json

{"name":"Margaret Laurence", "birth":1926, "nationality":Canadian}

```

## Autres ressources

- [JSON](https://www.json.org/json-en.html)
- [JSON (Wikipedia)](https://en.wikipedia.org/wiki/JSON)
- Mozilla (2023) [“Working with JSON”](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON)
- W3Schools (2023) [“JSON - Introduction”](https://www.w3schools.com/js/js_json_intro.asp)
- W3Schools (2023) [“JSON Object Literals”](https://www.w3schools.com/js/js_json_objects.asp)