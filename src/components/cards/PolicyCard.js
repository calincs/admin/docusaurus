import React from 'react';
import styles from "./cards.module.css";
import Link from '@docusaurus/Link';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function PolicyCard({ icon, title, link }) {
    return (
        <div className={styles.policyCard}>
            <FontAwesomeIcon icon={icon} size="4x" />

            <Link to={link}>
                <h3>{title}</h3>
            </Link>
        </div>
    );
}

export default PolicyCard;