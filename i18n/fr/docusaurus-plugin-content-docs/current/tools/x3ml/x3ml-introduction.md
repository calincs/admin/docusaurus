---
sidebar_position: 1
title: "Présentation de 3M"
description: "Présentation de 3M"
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-17
---

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

La documentation sur les LINCS 3M se concentre sur la création de données pour le :Term[Knowledge Graph (KG)]{#knowledge-graph}. Nous vous recommandons de consulter le [manuel officiel de 3M](https://gitlab.isl.ics.forth.gr/cci/3m-docker/-/blob/master/manual.pdf) pour une introduction générale à 3M et pour des informations plus approfondies qui sortent du cadre de la documentation LINCS. Des [ressources communautaires](/docs/tools/x3ml#ressources) sont également disponibles.

L'[instance LINCS 3M](https://mapper.lincsproject.ca/3m/) est préchargée avec les fichiers :Term[ontologie]{#ontology} et le générateur utilisés pour la conversion des données LINCS. Pour utiliser l'instance LINCS 3M, [s'enregistrer pour un compte](https://mapper.lincsproject.ca/3m/Registration) et attendre l'approbation du compte par LINCS.

LINCS utilise la [version 2022 de 3M](https://gitlab.isl.ics.forth.gr/cci/3m-docker), et toutes les captures d'écran proviennent de cette version. Bien que certains documents externes contiennent des captures d'écran de versions antérieures de 3M, les informations fournies sont toujours utiles.

## Pages 3M

Cette section donne un aperçu des pages 3M que vous utiliserez pour convertir les données en vue de leur publication avec LINCS. Il ne s'agit pas d'une liste exhaustive de toutes les fonctions de 3M. Des instructions plus détaillées pour 3M sont disponibles :

- Le [manuel officiel de 3M](https://gitlab.isl.ics.forth.gr/cci/3m-docker/-/blob/master/manual.pdf)
- [Création d'un projet de cartographie dans 3M](/docs/tools/x3ml/create-mapping)
- L'interface d'un projet de cartographie{/* ADD LINK */}

## Projet Mapping

| ![](</img/documentation/x3ml-intro-projects-(c-LINCS).png>) |
| :---------------------------------------------------------------: |
|       La page **Mappings de projet** avec les annotations.        |

La page **Mappages de projets** est l'écran qui s'affiche après la connexion. C'est ici que vous gérez vos projets de conversion. Les règles de conversion que vous définissez pour chaque projet sont appelées :Term[mapping]{#mapping}.

Sur la page **Mappings de projet**, vous pouvez :

1. Voir tous les projets dans l'instance LINCS 3M
2. Organiser vos projets
3. Ouvrir vos projets
4. Créer de nouveaux projets
   1. Copiant des projets existants
   2. en créant de nouveaux projets
   3. Téléchargement de fichiers ZIP 3M exportés

:::warning

Lorsque vous utilisez l'instance LINCS 3M, vos fichiers sont stockés sur les serveurs LINCS. Nous vous recommandons d'exporter régulièrement vos projets 3M et de les stocker localement, car LINCS ne garantit pas le stockage à long terme des projets 3M. Les projets exportés peuvent être retéléchargés vers 3M. Plus de détails sont disponibles dans [Export & Extras](/docs/tools/x3ml/create-mapping/export-extras)

:::

## Gestion des métadonnées des fichiers

| ![](</img/documentation/x3ml-intro-metadata-(c-LINCS).png>) |
| :---------------------------------------------------------------: |
| Fenêtre **Gestion des métadonnées des fichiers** avec annotations.|

La fenêtre **Gestion des métadonnées des fichiers** s'affiche lorsque vous ouvrez un projet. Vous pouvez utiliser cette fenêtre pour spécifier les données que vous souhaitez convertir et les ontologies que les données converties doivent suivre.

Dans la fenêtre **Gestion des métadonnées des fichiers**, vous pouvez :

1. Donner un titre à votre fichier d'entrée de données source (votre fichier :Term[XML]{#xml})
2. Modifier votre fichier d'entrée de données source
3. Ajoutez et supprimez les fichiers :Term[Resource Description Framework Schema (RDFS)]{#resource-description-framework-schema} pour les schémas cibles (ontologies) que vous avez choisis.

## Aperçu des correspondances

| ![](</img/documentation/x3ml-intro-hrid-(c-LINCS).png>) |
| :-----------------------------------------------------------: |
| Page de **Vue d'ensemble des cartographies** avec annotations. |

La page **Mappings Overview** se charge lorsque vous fermez la fenêtre **Files Metadata Management**. Vous pouvez utiliser cette page pour définir les règles de conversion de vos données.

Sur la page **Présentation des correspondances**, vous pouvez :

1. Modifier le projet
   1. Titre
   2. Description
2. Modifier la vue
   1. Développer ou réduire toutes les correspondances
   2. Développer ou réduire une seule correspondance
3. Importation à partir d'un fichier X3ML
4. Exporter tous les fichiers de cartographie dans un fichier ZIP

| ![](</img/documentation/x3ml-intro-mapping-(c-LINCS).png>) |
| :--------------------------------------------------------------: |
| Page de **Vue d'ensemble des cartographies** avec annotations.  |

Pour chaque mappage, vous pouvez également modifier les éléments suivants :

1. :Term[Domain]{#domain}
2. Lien
3. Source :Term[Nodes]{#node}
4. Générateurs
5. Étiquettes
6. Variables

## Gestionnaire des définitions de générateurs

|             ![](</img/documentation/x3ml-intro-open-(c-LINCS).png>)              |
| :------------------------------------------------------------------------------------: |
| L'écran de la page **Mappings Overview** avec **Open Generator Definition Manager** présenté. |

Vous pouvez accéder au **Generator Definitions Manager** en cliquant sur **Open Generator Definition Manager** (icône de lien) dans le coin supérieur droit.
| ![](</img/documentation/x3ml-intro-generator-(c-LINCS).png>) |
| :----------------------------------------------------------------: |
| Fenêtre **Generator Definitions Manager Dialog** avec des annotations.  |

La fenêtre **Generator Definitions Manager Dialog** vous permet de spécifier comment vous souhaitez créer des étiquettes et des identifiants :Term[entity]{#entity}.

Dans la fenêtre **Generator Definitions Manager** vous pouvez :

1. Exporter les définitions vers un fichier XML
2. Utiliser un fichier générateur existant
3. Ajouter de nouvelles définitions au projet directement dans le gestionnaire de définitions
4. Supprimer des définitions existantes de votre projet

Vous pouvez également modifier les définitions des générateurs. Pour plus d'informations, voir <UnderConstruction>Générateurs</UnderConstruction>.

## "Produire RDF" (sortie transformée)

| ![](</img/documentation/x3ml-intro-rdf-(c-LINCS).png>) |
| :----------------------------------------------------------: |
|  La page **Mappings Overview** avec **Produce RDF** esquissée.   |

| ![](</img/documentation/x3ml-intro-output-(c-LINCS).png>) |
| :-------------------------------------------------------------: |
|         Fenêtre **Sortie transformée** avec annotations.         |

En bas de la page **Mappings Overview** se trouve le bouton **Produce RDF**. Le projet de mapping est automatiquement sauvegardé dans l'instance LINCS 3M. Pour sortir vos données converties de 3M, vous devez produire des données RDF.

Sur la page **Mappings Overview** et la fenêtre **Transformed Output** vous pouvez :

1. Modifier le :Term[RDF serialization]{#resource-description-framework-serialization} (format) de la sortie.
2. Télécharger la sortie dans le format de votre choix (RDF/XML, :Term[TTL]{#turtle}, N-Triples, TriG)
