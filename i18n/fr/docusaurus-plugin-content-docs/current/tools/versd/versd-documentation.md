---
title: "Documentation VERSD"
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-17
---

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

## Prérequis

### Créer un compte

Vous avez besoin d'un compte LINCS pour accéder au VERSD. Voir [Account Service Documentation](/docs/tools/account-service/account-service-documentation) pour plus d'informations.

### Préparer votre jeu de données

Vous avez besoin d'un jeu de données :Term[structured data]{#structured-data} (:Term[JSON]{#json} ou CSV) pour utiliser le VERSD. Assurez-vous que votre jeu de données est propre avant de lancer le processus de :Term[entity matching]{#entity-matching}. Pour plus d'informations sur le nettoyage de vos données, consultez l'[étape de nettoyage des données](/docs/create-lod/clean-data) du flux de travail de conversion des données et le [guide de nettoyage des données](/docs/create-lod/clean-data/data-cleaning-guide) du LINCS.

:::note

L'algorithme de rapprochement du VERSD prend en compte les relations entre les enregistrements de votre jeu de données lorsqu'il détermine les correspondances candidates. Si les enregistrements de votre jeu de données sont liés (par exemple, s'il s'agit d'auteurs de la même période), le système VERSD utilisera cette information pour formuler des recommandations plus précises. Bien que le traitement de grands ensembles de données prenne plus de temps, les résultats ont tendance à être plus précis lorsqu'il y a plus d'enregistrements.

:::

## Créer une demande de rapprochement

Remplissez une demande de rapprochement pour commencer à utiliser VERSD. Cette demande permettra à VERSD de sauvegarder votre travail afin que vous n'ayez pas à télécharger et à cartographier à nouveau votre ensemble de données chaque fois que vous souhaitez rapprocher :Term[entities]{#entity}.

Pour effectuer une demande de rapprochement, remplissez les champs de la page Demande de rapprochement :

1. Donnez un nom à votre demande de rapprochement.
2. Téléchargez votre jeu de données (JSON ou CSV). Si vous avez un petit jeu de données JSON, vous pouvez le coller directement dans la zone de texte pour le télécharger.
3. Choisissez le(s) type(s) de données que vous avez dans votre jeu de données (bibliographique, prosopographique ou géospatial). Le type de données que vous choisissez modifiera les :Term[authorities]{#authority-file} recommandés par le VERSD.
4. Choisissez les autorités avec lesquelles vous souhaitez effectuer le rapprochement. Vous pouvez sélectionner plusieurs autorités et les classer par ordre de priorité.
5. Cliquez sur **Configurer les correspondances des rubriques**.

:::note

Le VERSD prend principalement en charge les données bibliographiques. Bien que :Term[Wikidata]{#wikidata} donne le plus grand nombre de résultats, les résultats de :Term[VIAF]{#virtual-international-authority-file} font davantage autorité. Pour plus d'informations sur les autorités et sur la manière de les choisir, consultez le [LINCS entity reconciliation guide](/docs/create-lod/match-entities/entity-matching-guide).

:::

## Complétez votre mappage

### Cartographier les champs

Après avoir téléchargé votre jeu de données, vous devez :Term[map]{#mapping} les champs de votre jeu de données aux champs de l'autorité.

Cliquez sur la flèche vers le bas à côté de chaque champ pour afficher une liste déroulante des champs de votre jeu de données. Notez que vous pouvez mapper plusieurs champs de vos données à chaque champ de l'autorité. Par exemple, si votre jeu de données comporte un champ "Prénom" et un champ "Nom", vous pouvez les ajouter tous les deux au champ "Nom(s)" dans le VERSD.

:::warning

Il est important de saisir tout ce que vous pouvez pendant la phase de mise en correspondance. Si vous ne mappez pas un champ de votre jeu de données, il ne sera pas utilisé par le service de réconciliation pour trouver des entités candidates. Le champ sera toujours disponible en tant que contexte supplémentaire pour vérifier les correspondances.

:::

Cliquez sur **Fait** une fois que vous êtes satisfait de votre mappage.

### Ajuster le seuil

Utilisez le premier curseur pour déterminer le nombre de candidats que vous souhaitez voir suggérés pour chaque enregistrement de votre ensemble de données. Plus il y a de candidats, plus le service de rapprochement mettra de temps à traiter votre ensemble de données.

Utilisez le deuxième curseur pour déterminer à quel point les entités candidates doivent correspondre aux enregistrements de votre ensemble de données. Plus le seuil est élevé, plus la correspondance est étroite. LINCS suggère d'utiliser un seuil de 70-99% pour obtenir les meilleurs résultats.

## Soumettre votre demande

Cliquez sur **Submit** pour commencer à traiter votre jeu de données à l'aide du service de rapprochement. Sur la page de traitement, vous pourrez voir tous vos travaux antérieurs et savoir s'ils sont _en cours de traitement_ ou _prêts_. Comme vous pouvez revenir à cette page à tout moment, vous n'avez pas besoin de garder le VERSD ouvert localement pendant le traitement de votre ensemble de données.

Une fois que votre travail est _prêt_, cliquez sur **match rapide** pour commencer le rapprochement.

## Réconcilier les entités avec l'appariement rapide

Le VERSD vous présentera un enregistrement de votre jeu de données et les correspondances candidates des autorités que vous avez choisies au cours du processus de mise en correspondance.

Comparez les entités candidates à l'enregistrement de votre base de données. Vous avez les options suivantes lors de l'appariement :

- **Match complet:** Une entité candidate correspond à l'enregistrement de votre base de données. Une fenêtre contextuelle s'affiche pour vous permettre de vérifier les informations et d'accepter ou de rejeter l'entité candidate.
- **Correspondance partielle:** Une entité candidate correspond partiellement à l'enregistrement de votre base de données. Une fenêtre contextuelle s'affiche pour vous permettre d'accepter ou de rejeter certaines parties des informations de l'autorité et d'accepter ou de rejeter l'entité candidate.
- **Correspondance manuelle:** Vous disposez déjà d'un :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} pour l'entité. Une fenêtre contextuelle s'affiche pour vous permettre de saisir manuellement l'URI de l'enregistrement.
- **Pas de correspondance:** Aucune des entités candidates ne correspond à l'enregistrement dans votre ensemble de données.

Le VERSD dispose de quelques raccourcis clavier qui vous permettent d'accélérer le processus de mise en correspondance.

| Touche |              Action             |
| :-: | :--------------------------------: |
|  ←  | Sélectionner le candidat précédent |
|  →  | Sélectionner le candidat suivant   |
|  ↑  |       Faire un match complet       |
|  ↓  | Faire une correspondance partielle |
|  M  | Faire une correspondance manuelle  |
|  X  |      Ne pas faire d'amalgame       |

Une barre de progression en haut de la page vous indique le degré d'avancement de la mise en correspondance des entités de votre ensemble de données. Le VERSD enregistre automatiquement votre progression. Si vous actualisez votre page, le logiciel VERSD vous ramènera à l'endroit où vous vous êtes arrêté dans le processus d'appariement.

Une fois l'appariement terminé, vous obtiendrez une fenêtre contextuelle récapitulative qui vous donnera des statistiques sur vos appariements. Dans cette fenêtre, vous pouvez cliquer sur **Exporter les résultats** ou **Nouvelle demande**. L'option **Exporter les résultats** vous permet d'accéder à une page où vous pouvez exporter l'ensemble des données réconciliées au format JSON ou CSV. Sur la page d'exportation, vous pouvez choisir les champs que vous souhaitez ajouter à votre ensemble de données. **Nouvelle demande** vous conduira à la page Demande de rapprochement.

## Gérer les travaux

Pour accéder à vos travaux antérieurs, cliquez sur **Mes demandes** dans la barre d'outils de navigation supérieure. Vous pouvez supprimer des travaux en cliquant sur **Match rapide** puis sur **Delete** dans la liste déroulante.

## Voir vos statistiques

Chaque utilisateur du VERSD a un profil. Cliquez sur l'icône de profil dans le coin supérieur droit pour afficher votre page de profil. Sur votre page de profil, vous pouvez consulter vos statistiques de rapprochement et voir comment votre travail de rapprochement se compare à celui des autres utilisateurs de VERSD.
