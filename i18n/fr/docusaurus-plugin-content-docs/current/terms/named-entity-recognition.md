---
id: named-entity-recognition
title: Reconnaissance d’entité nommée (NER)
definition: Processus d’identification et de catégorisation d’entités (un mot ou un ensemble de mots faisant référence à la même chose) dans un texte.
---

La reconnaissance d’entité nommée (NER) consiste à identifier et à catégoriser :Term[entités]{#entity}—un mot ou un ensemble de mots qui fait référence à la même chose—dans le texte. Le NER implique donc deux étapes : (1) identifier l’entité et (2) la catégoriser. Des exemples de catégories d’entités peuvent être des personnes, des lieux, des heures, des valeurs monétaires, etc. Le processus d’attribution d’un identifiant unique aux entités est appelé :Term[Désambiguisation d’entité nommée (NED)]{#named-entity-disambiguation}. NER est une application de :Term[Traitement du langage naturel (TLN)]{#natural-language-processing}.

## Exemples

- [NERVE](https://github.com/cwrc/NERVE)
- [Stanford Named Entity Recognizer](https://nlp.stanford.edu/software/CRF-NER.html)

## Autres ressources

- Hooland et al. (2015) [“Exploring Entity Recognition and Disambiguation for Cultural Heritage Collections”](https://freeyourmetadata.org/publications/named-entity-recognition.pdf)
- [Named Entity Recognition (Wikipedia)](https://en.wikipedia.org/wiki/Named-entity_recognition)
- Selig (2021) [“Entity Extraction: How Does It Work?”](https://expertsystem.com/entity-extraction-work/)
