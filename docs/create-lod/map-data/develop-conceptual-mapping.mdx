---
sidebar_position: 6
title: "Develop Conceptual Mapping"
description: "LINCS Transformation Workflows — Develop Conceptual Data Mapping"
toc_min_heading_level: 2
toc_max_heading_level: 3
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Introduction

Every incoming dataset starts with a unique structure and use of terms. To get all of this unique data to connect as :Term[linked open data (LOD)]{#linked-open-data}, each dataset needs to use the same :Term[ontology]{#ontology}. In this step, LINCS develops a mapping that basically gives us instructions on how each relationship in the original data should look as :Term[Resource Description Framework (RDF)]{#resource-description-framework} :Term[triples]{#triple}.

LINCS has adopted :Term[CIDOC CRM]{#cidoc-crm} as its base ontology. CIDOC CRM can be tailored to a specific dataset using existing CIDOC CRM extensions or domain specific :Term[vocabularies]{#vocabulary}. LINCS’s use of CIDOC CRM is documented in the [LINCS application profile](/docs/explore-lod/understand-lincs-data/application-profiles-main), with project specific transformation details available in the [project application profiles](/docs/explore-lod/project-datasets/).

For more information, see our [ontology documentation](/docs/learn-lod/linked-open-data-basics/concepts-ontologies).

## Resources Needed

This step can be a challenging one as research teams are often new to CIDOC CRM and many of the ontology concepts introduced. LINCS is here to help make the step as simple as possible.

For :Term[natural language]{#natural-language-data} and :Term[TEI data]{#tei-data}, we have tools that let you skip this step and extract RDF directly from your data using templates for common relationships.

For datasets that need custom mappings, we have :Term[application profiles]{#application-profile} that you can follow. These application profiles show the patterns we have used in mappings for previously transformed LINCS data. When this is not enough, the Ontology Team can—with your consultation—develop custom mappings for you. This process typically takes 2-4 weeks from the time that LINCS receives a copy of your source data and enough documentation to understand the relationships it contains.

:::info
We encourage your team to take an active role through the mapping process, even if LINCS is developing a custom mapping for you. By interrogating the ways in which your data is mapped to CIDOC CRM and the vocabulary terms introduced during this step, research teams often learn new things about their data and reevaluate the best ways to express concepts.

The mapping process has inspired past LINCS projects to go back and enhance their source data once they understood how expressive CIDOC CRM and RDF data can be.
:::

Regardless of the workflow you are following, your team will benefit from reviewing our [ontology documentation](/docs/learn-lod/linked-open-data-basics/concepts-ontologies) to understand the goals of this step and the basics of CIDOC CRM. Once your data is transformed, understanding some CIDOC CRM will also help you navigate your data and take advantage of its new structure. 

|                     | Research Team | Ontology Team | Transformation Team | Storage Team |
| ------------------- | ------------- | ------------- | --------------- | ------------ |
| [Develop a Mapping](#develop-a-mapping)     | ✓              |  ✓            |                 |              |
| [Consult and Approve Mapping](#consult-and-approve-mapping) | ✓             | ✓             |                 |              |

## Develop a Mapping

<Tabs groupId="transformation-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>

Most incoming datasets express some relationships that are common to already transformed LINCS datasets. Basic biographic information about people or bibliographic details of written works are common examples. In these cases, your team can, with the support of the Ontology Team, use the [LINCS application profile](/docs/explore-lod/understand-lincs-data/application-profiles-main) to start mapping those components. As more datasets are added to the LINCS :Term[triplestore]{#triplestore}, more mappings will be available to draw upon.

When there are no existing datasets in LINCS that have similar structure and content, The Ontology Team either drafts a new conceptual mapping or adapts an existing mapping. The Ontology Team iterates the mapping process until a conceptual model has been created that accurately captures and perhaps even enhances the meaning of the original data. The mapping is approved by the Research Team.

</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">

Data in this category is unique to each project and, compared to :Term[structured data]{#structured-data}, there is not necessarily a clear structure of entities and the important relationships between them.

The basics of this step typically look like this:

- Your team will identify what from the existing data should be expressed by the new mappings. An important consideration here is how that information can be extracted from the data. Is the data standardized and annotated enough that a script could be written to extract it or will it need to be manually extracted by a human?
- Your team should review our [ontologies documentation](/docs/learn-lod/linked-open-data-basics/concepts-ontologies) to gain the relevant background knowledge. Next, review the [LINCS application profile](/docs/explore-lod/understand-lincs-data/application-profiles-main) to understand how basic information from your data can be mapped to CIDOC CRM. If your data is similar to existing LINCS data, then try to follow the application profile to map your data.
- If you need support or you have data that does not fit within existing LINCS mapping in the application profile, the Ontology Team can help with creating custom mappings for your project. Note that the amount of support we can provide varies depending on the other projects we are supporting at that time.

</TabItem>
<TabItem value="tei" label="TEI Data">

LINCS has developed tools that include pre-set templates for you to choose from that will extract information from your TEI documents and output CIDOC CRM RDF data. For details on these tools, continue to the [Implement Conceptual Mapping](/docs/create-lod/map-data/implement-conceptual-mapping/) step.

If you need additional custom mappings for TEI fields that do not fit in the templates but that fall within our definition of structured data, then you should follow the structured data workflow for those fields or use the transformation XSLTs provided in the [XTriples documentation](/docs/tools/xtriples/xtriples-documentation).

If you need additional mappings for TEI fields that do not fit in the templates, refer to the [LINCS application profile](/docs/explore-lod/understand-lincs-data/application-profiles-main) to map them into CIDOC CRM or consult with LINCS if custom mappings need to be created. Refer to the structured data workflow for details on the process of creating custom mappings.

If you want to handle natural language text fields embedded in the TEI documents then refer to the natural language workflow for that part of the data.

</TabItem>
<TabItem value="natural" label="Natural Language Data">

There are two ways LINCS extracts facts from natural language texts.

The preferred method is to use LINCS natural language processing tools which use a preset list of relationships that they can extract. For these extracted facts, you can use LINCS tools to transform the facts into CIDOC CRM. You do not need to create any custom mappings. For details on these tools, continue to the [Implement Conceptual Mapping](/docs/create-lod/map-data/implement-conceptual-mapping/) step. Check back soon for the application profile for these tools, which shows what relationships are covered and how they are expressed in CIDOC CRM.

If you would like to extract additional facts from your text that were either missed by our tools or that fall into relationships not covered by our tools, you can manually extract additional facts. This extraction can be by hand or using other :Term[relation extraction (RE)]{#relation-extraction} systems. For these facts, refer to the [LINCS application profile](/docs/explore-lod/understand-lincs-data/application-profiles-main) to map them into CIDOC CRM or consult with LINCS if custom mappings need to be created. Refer to the [Structured Data tab](/docs/create-lod/map-data/develop-conceptual-mapping?workflow=structured#develop-a-mapping) for details on the process of creating custom mappings.

</TabItem>
</Tabs>

## Consult and Approve Mapping

The mapping process is iterative; once the research team and Ontology Team are satisfied with the mapping, you can move to the next step. If you are using LINCS natural language or TEI transformation tools, then you will not have output from this step and will move on to the [Implement Conceptual Mapping](/docs/create-lod/map-data/implement-conceptual-mapping/) step to generate your transformed data.

Otherwise, at the end of this step you should have mappings defined that you will implement in the [next step](/docs/create-lod/map-data/implement-conceptual-mapping/).
